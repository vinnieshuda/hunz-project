﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;

public partial class api_skill : WOApiWebPage
{
    protected override void Execute()
    {
        int OpCode = web.GetInt("OpCode");

        switch(OpCode)
        {
            case 1:
                {
                    int CharID = web.GetInt("CharID");


                    SqlCommand sqcmd = new SqlCommand();
                    sqcmd.CommandType = CommandType.StoredProcedure;
                    sqcmd.Parameters.AddWithValue("@in_CharID", CharID);
                    sqcmd.CommandText = "WZ_Skill";

                    if (!CallWOApi(sqcmd))
                        return;

                    StringBuilder xml = new StringBuilder();
                    xml.Append("<?xml version=\"1.0\"?>\n");
                    xml.Append("<skills>");
                    while (reader.Read())
                    {
                        xml.Append("<s ");
                        xml.Append(xml_attr("SkillID", reader["SkillID"]));
                        xml.Append(xml_attr("level", reader["level"]));
                        xml.Append("/>\n");
                    }
                    xml.Append("</skills>");

                    GResponse.Write(xml.ToString());
                    break;
                }
            case 2:
                {
                    int CharID = web.GetInt("CharID");
                    int SkillID = web.GetInt("SkillID");
                    int level = web.GetInt("level");

                    SqlCommand sqcmd = new SqlCommand();
                    sqcmd.CommandType = CommandType.StoredProcedure;
                    sqcmd.Parameters.AddWithValue("@in_CharID", CharID);
                    sqcmd.Parameters.AddWithValue("@in_SkillID", SkillID);
                    sqcmd.Parameters.AddWithValue("@in_level", level);
                    sqcmd.CommandText = "WZ_AddSkill";

                    if (!CallWOApi(sqcmd))
                        return;

                    Response.Write("WO_0");

                    break;
                }
        }
    }
}
