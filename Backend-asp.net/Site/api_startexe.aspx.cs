﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class api_startexe : WOApiWebPage
{
    protected override void Execute()
    {
        string CustomerID = web.CustomerID();
        string logged = web.logged();

        SqlCommand sqcmd = new SqlCommand();
        sqcmd.CommandType = CommandType.StoredProcedure;
        sqcmd.CommandText = "WZ_startexe";
        sqcmd.Parameters.AddWithValue("@in_CustomerID", CustomerID);
        sqcmd.Parameters.AddWithValue("@in_logged", logged);

        if (!CallWOApi(sqcmd))
            return;

        Response.Write("WO_0");
        return;
    }
}
