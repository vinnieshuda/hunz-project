﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class api_vipitem : WOApiWebPage
{
    protected override void Execute()
    {
        string CustomerID = web.CustomerID();
        //string CharID = web.Param("CharID");
        string ItemID = web.Param("ItemID");

        SqlCommand sqcmd = new SqlCommand();
        sqcmd.CommandType = CommandType.StoredProcedure;
        sqcmd.CommandText = "WZ_VeteranItemToInv";
        sqcmd.Parameters.AddWithValue("@in_CustomerID", CustomerID);
        //sqcmd.Parameters.AddWithValue("@in_CharID", CharID);
        sqcmd.Parameters.AddWithValue("@in_ItemID", ItemID);

        if (!CallWOApi(sqcmd))
            return;

        Response.Write("WO_0");
        return;
    }
}
