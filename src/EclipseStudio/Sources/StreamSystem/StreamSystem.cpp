#include "r3dPCH.h"
#include "r3d.h"

#include "StreamSystem.h"

#include "../GameLevel.h"

const float load_radius = 350.0f;

StreamSystem::StreamSystem()
{
	levelObjectsNum = 0;
	range = 0.0f;
}

StreamSystem::~StreamSystem()
{
}

void StreamSystem::reset()
{
	for (int i = 0; i <= levelObjectsNum; i++)
	{
		levelObjects[i].reset();
	}

	levelObjectsNum = 0;
	range = 0.0f;
}

void StreamSystem::setPlayerPosition(r3dPoint3D pos)
{
	plrPosition = pos;

	checkObjects();
}

void StreamSystem::setObjectData(pugi::xml_node & curNode)
{
	int count = 0;
	pugi::xml_node xmlObject = curNode.child("object");

	for (;!xmlObject.empty();)
	{
		count++;
		
		pugi::xml_node posNode = curNode.child("position");
		
		const char* kocsog = curNode.attribute("className").value();
		levelObjects[count].class_name = curNode.attribute("className").value();
		levelObjects[count].load_name = curNode.attribute("fileName").value();
		r3dOutToLog("HunZ - StreamSystem Test fileName: %s\n", kocsog);

		levelObjects[count].pos.x = posNode.attribute("x").as_float();
		levelObjects[count].pos.y = posNode.attribute("y").as_float();
		levelObjects[count].pos.z = posNode.attribute("z").as_float();

		r3dOutToLog("HunZ - StreamSystem Test pos x: %f, y: %f, z: %f\n", levelObjects[count].pos.x, levelObjects[count].pos.y, levelObjects[count].pos.z);

		xmlObject = xmlObject.next_sibling();
	}

	levelObjectsNum = count;

	r3dOutToLog("HunZ - StreamSystem levelObjectsNum: %i, count: %i\n", levelObjectsNum, count);
}

void StreamSystem::checkObjects()
{
	// Na itt lesz az erdekes dolog :D
	// Le kell checkolni a player poziciojat, hogy az adott sugaru koron belul
	// be vannak e toltve az objectek! Ha igen akkor nem tortenik semmi ha nincs
	// akkor betolti. Ha mar nem szukseges az object akkor torolni kell a memoriabol,
	// valamint innen lesz elinditva a szukseges objectek betoltesenek megkezdese is!
	for (int i = 0; i <= levelObjectsNum; i++)
	{
		if (isInLoadRadius(levelObjects[i].pos))
		{
			if (!levelObjects[i].loaded)
			{
				levelObjects[i].setNeedLoad(true);
				levelObjects[i].loaded = true;
			}

			r3dOutToLog("HunZ - StreamSystem Object Name: %s, Object Class: %s\n", levelObjects[i].load_name, levelObjects[i].class_name);
		}
		else
			levelObjects[i].setNeedLoad(false);
	}
}

bool StreamSystem::isInLoadRadius(r3dPoint3D pos)
{
	// az object poziciojanak kiszamitasa a kor-hoz merten
	D3DXVECTOR3 vecDist(plrPosition - pos);
	float fDistSq( D3DXVec3Dot( &vecDist, &vecDist) );

	// ha kisebb vagy egyenlo akkor az object a kor-on belul talalhato
	if (fDistSq <= (load_radius * load_radius))
	{
		return true;
	}

	// ha nem
	return false;
}

GameObject* StreamSystem::LoadObject(ObjectData* object)
{
	GameObject* obj = NULL;
	
	/*obj = srv_CreateGameObject(class_name, load_name, pos);
	if(!obj)
	{
		r3dOutToLog("!!!Failed to create object! class: %s, name: %s\n", class_name, load_name);
		return NULL;
	}
	r3d_assert ( obj );
	obj->ReadSerializedData(curNode);*/

	return obj;
}