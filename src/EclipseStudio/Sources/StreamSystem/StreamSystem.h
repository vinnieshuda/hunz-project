#pragma once
#include "r3dPCH.h"
#include "r3d.h"

#include "../../../GameEngine/gameobjects/ObjManag.h"

class StreamSystem
{
public:
	StreamSystem();
	~StreamSystem();

	void setPlayerPosition(r3dPoint3D pos);
	void setObjectData(pugi::xml_node & curNode);

	void reset();

	void checkObjects();
private:
	bool isInLoadRadius(r3dPoint3D pos);

	struct ObjectData
	{
		const char* class_name;
		const char* load_name;
		r3dPoint3D pos;
		
		bool needLoad;
		bool loaded;
	
		ObjectData(): class_name(""), load_name(""), pos(0,0,0), needLoad(false), loaded(false) {}

		/*~ObjectData()
		{
			delete[] class_name;
			delete[] load_name;
		}*/

		void reset()
		{
			class_name = "";
			load_name = "";

			pos.x = 0;
			pos.y = 0;
			pos.z = 0;

			needLoad = false;
			loaded = false;
		}

		void setNeedLoad(bool need)
		{
			needLoad = need;
			//itt meg hivsz amit akarsz

		}
	};
	ObjectData levelObjects[OBJECTMANAGER_MAXOBJECTS];
	int levelObjectsNum;
	float range;

	r3dPoint3D plrPosition;

public:
	GameObject* LoadObject(ObjectData* object);
};