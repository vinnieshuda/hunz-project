#pragma once

#include "r3dPCH.h"
#include "r3d.h"
#include <stdio.h>
#include <Windows.h>

#include "teamspeak.h"

#include <public_definitions.h>
#include <public_errors.h>
#include <clientlib_publicdefinitions.h>
#include <clientlib.h>

#include "../UI/HUDDisplay.h"
extern HUDDisplay*	hudMain;


Teamspeak::Teamspeak()
{
}

Teamspeak::~Teamspeak()
{
}

/*
 * Callback for connection status change.
 * Connection status switches through the states STATUS_DISCONNECTED, STATUS_CONNECTING, STATUS_CONNECTED and STATUS_CONNECTION_ESTABLISHED.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   newStatus                 - New connection status, see the enum ConnectStatus in clientlib_publicdefinitions.h
 *   errorNumber               - Error code. Should be zero when connecting or actively disconnection.
 *                               Contains error state when losing connection.
 */
void onConnectStatusChangeEvent(uint64 serverConnectionHandlerID, int newStatus, unsigned int errorNumber) {
	r3dOutToLog("Connect status changed: %llu %d %u\n", (unsigned long long)serverConnectionHandlerID, newStatus, errorNumber);
	/* Failed to connect ? */
	if(newStatus == STATUS_DISCONNECTED && errorNumber == ERROR_failed_connection_initialisation) {
		r3dOutToLog("Looks like there is no server running, terminate!\n");
		//exit(-1);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", "No Teamspeak Server running", 0);
	}
}

/*
 * Callback for current channels being announced to the client after connecting to a server.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   channelID                 - ID of the announced channel
 *   channelParentID           - ID of the parent channel
 */
void onNewChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID) {
    /* Query channel name from channel ID */
    char* name;
    unsigned int error;

    r3dOutToLog("onNewChannelEvent: %llu %llu %llu\n", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)channelParentID);
    if((error = ts3client_getChannelVariableAsString(serverConnectionHandlerID, channelID, CHANNEL_NAME, &name)) == ERROR_ok) {
        r3dOutToLog("New channel: %llu %s \n", (unsigned long long)channelID, name);
        ts3client_freeMemory(name);  /* Release dynamically allocated memory only if function succeeded */
    } else {
        char* errormsg;
        if(ts3client_getErrorMessage(error, &errormsg) == ERROR_ok) {
            r3dOutToLog("Error getting channel name in onNewChannelEvent: %s\n", errormsg);
			char msg[128];
			sprintf(msg, "Error gettig channel name:%s", errormsg);
			hudMain->showChat(true);
			hudMain->addChatMessage(1, "<system>", msg, 0);
            ts3client_freeMemory(errormsg);
        }
    }
}

/*
 * Callback for just created channels.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   channelID                 - ID of the announced channel
 *   channelParentID           - ID of the parent channel
 *   invokerID                 - ID of the client who created the channel
 *   invokerName               - Name of the client who created the channel
 */
void onNewChannelCreatedEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
    char* name;

    /* Query channel name from channel ID */
    if(ts3client_getChannelVariableAsString(serverConnectionHandlerID, channelID, CHANNEL_NAME, &name) != ERROR_ok)
        return;
    r3dOutToLog("New channel created: %s\n", name);
    ts3client_freeMemory(name);  /* Release dynamically allocated memory only if function succeeded */
}

/*
 * Callback when a channel was deleted.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   channelID                 - ID of the deleted channel
 *   invokerID                 - ID of the client who deleted the channel
 *   invokerName               - Name of the client who deleted the channel
 */
void onDelChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	r3dOutToLog("Channel ID %llu deleted by %s (%u)\n", (unsigned long long)channelID, invokerName, invokerID);
}

/*
 * Called when a client joins, leaves or moves to another channel.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   clientID                  - ID of the moved client
 *   oldChannelID              - ID of the old channel left by the client
 *   newChannelID              - ID of the new channel joined by the client
 *   visibility                - Visibility of the moved client. See the enum Visibility in clientlib_publicdefinitions.h
 *                               Values: ENTER_VISIBILITY, RETAIN_VISIBILITY, LEAVE_VISIBILITY
 */
void onClientMoveEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* moveMessage) {
	r3dOutToLog("ClientID %u moves from channel %llu to %llu with message %s\n", clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, moveMessage);
}

/*
 * Callback for other clients in current and subscribed channels being announced to the client.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   clientID                  - ID of the announced client
 *   oldChannelID              - ID of the subscribed channel where the client left visibility
 *   newChannelID              - ID of the subscribed channel where the client entered visibility
 *   visibility                - Visibility of the announced client. See the enum Visibility in clientlib_publicdefinitions.h
 *                               Values: ENTER_VISIBILITY, RETAIN_VISIBILITY, LEAVE_VISIBILITY
 */
void onClientMoveSubscriptionEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility) {
    char* name;

    /* Query client nickname from ID */
    if(ts3client_getClientVariableAsString(serverConnectionHandlerID, clientID, CLIENT_NICKNAME, &name) != ERROR_ok)
        return;
    r3dOutToLog("New client: %s\n", name);
    ts3client_freeMemory(name);  /* Release dynamically allocated memory only if function succeeded */
}

/*
 * Called when a client drops his connection.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   clientID                  - ID of the moved client
 *   oldChannelID              - ID of the channel the leaving client was previously member of
 *   newChannelID              - 0, as client is leaving
 *   visibility                - Always LEAVE_VISIBILITY
 *   timeoutMessage            - Optional message giving the reason for the timeout
 */
void onClientMoveTimeoutEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* timeoutMessage) {
	r3dOutToLog("ClientID %u timeouts with message %s\n",clientID, timeoutMessage);
}

/*
 * This event is called when a client starts or stops talking.
 *
 * Parameters:
 *   serverConnectionHandlerID - Server connection handler ID
 *   status                    - 1 if client starts talking, 0 if client stops talking
 *   isReceivedWhisper         - 1 if this event was caused by whispering, 0 if caused by normal talking
 *   clientID                  - ID of the client who announced the talk status change
 */
void onTalkStatusChangeEvent(uint64 serverConnectionHandlerID, int status, int isReceivedWhisper, anyID clientID) {
    char* name;

    /* Query client nickname from ID */
    if(ts3client_getClientVariableAsString(serverConnectionHandlerID, clientID, CLIENT_NICKNAME, &name) != ERROR_ok)
        return;
    if(status == STATUS_TALKING) {
        r3dOutToLog("Client \"%s\" starts talking.\n", name);
		if (hudMain->isActivated)
		{
			/*hudMain->setVoipIcon(true);*/
		}		
    } else {
        r3dOutToLog("Client \"%s\" stops talking.\n", name);
		if (hudMain->isActivated)
		{
			/*hudMain->setVoipIcon(false);*/
		}		
    }
    ts3client_freeMemory(name);  /* Release dynamically allocated memory only if function succeeded */
}

void onServerErrorEvent(uint64 serverConnectionHandlerID, const char* errorMessage, unsigned int error, const char* returnCode, const char* extraMessage) {
	r3dOutToLog("Error for server %llu: %s %s\n", (unsigned long long)serverConnectionHandlerID, errorMessage, extraMessage);
	char msg[128];
	sprintf(msg, "Error for server %llu: %s %s", (unsigned long long)serverConnectionHandlerID, errorMessage, extraMessage);
	hudMain->showChat(true);
	hudMain->addChatMessage(1, "<system>", msg, 0);
}

void Teamspeak::Init()
{
	unsigned int error;

	/* Create struct for callback function pointers */
	struct ClientUIFunctions funcs;

	/* Initialize all callbacks with NULL */
	memset(&funcs, 0, sizeof(struct ClientUIFunctions));

	/* Callback function pointers */
	/* It is sufficient to only assign those callback functions you are using. When adding more callbacks, add those function pointers here. */
	funcs.onConnectStatusChangeEvent    = onConnectStatusChangeEvent;
	funcs.onNewChannelEvent             = onNewChannelEvent;
	funcs.onNewChannelCreatedEvent      = onNewChannelCreatedEvent;
	funcs.onDelChannelEvent             = onDelChannelEvent;
	funcs.onClientMoveEvent             = onClientMoveEvent;
	funcs.onClientMoveSubscriptionEvent = onClientMoveSubscriptionEvent;
	funcs.onClientMoveTimeoutEvent      = onClientMoveTimeoutEvent;
	funcs.onTalkStatusChangeEvent       = onTalkStatusChangeEvent;
	funcs.onServerErrorEvent            = onServerErrorEvent;

	/* Initialize client lib with callbacks */
	/* Resource path points to the SDK\bin directory to locate the soundbackends folder when running from Visual Studio. */
	/* If you want to run directly from the SDK\bin directory, use an empty string instead to locate the soundbackends folder in the current directory. */
	if((error = ts3client_initClientLib(&funcs, NULL, LogType_FILE | LogType_CONSOLE | LogType_USERLOGGING, NULL, "")) != ERROR_ok) {
		char* errormsg;
		if(ts3client_getErrorMessage(error, &errormsg) == ERROR_ok) {
			r3dOutToLog("HunZ - error on init clientlib: %s\n", errormsg);
			ts3client_freeMemory(errormsg);
		}
		//return 1;
	}else r3dOutToLog("HunZ - clientlib initialized!\n");

	/* Spawn a new server connection handler using the default port and store the server ID */
	if((error = ts3client_spawnNewServerConnectionHandler(0, &scHandlerID)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error spawning server connection handler: %d\n", error);
		//return 1;
	}else r3dOutToLog("HunZ - Spawning server connection handler!\n");

	/* Open default capture device */
	/* Passing empty string for mode and NULL or empty string for device will open the default device */
	if((error = ts3client_openCaptureDevice(scHandlerID, "", NULL)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error opening capture device: %d\n", error);
		char msg[128];
		sprintf(msg, "Error opening capture device: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Opening capture device!\n");

	/* Open default playback device */
	/* Passing empty string for mode and NULL or empty string for device will open the default device */
	if((error = ts3client_openPlaybackDevice(scHandlerID, "", NULL)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error opening playback device: %d\n", error);
		char msg[128];
		sprintf(msg, "Error opening playback device: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Opening playback device!\n");
}

void Teamspeak::Connect(const char* clientName)
{
	unsigned int error;
	char *version;

	/* Create a new client identity */
	/* In your real application you should do this only once, store the assigned identity locally and then reuse it. */
	if((error = ts3client_createIdentity(&identity)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error creating identity: %d\n", error);
		//return 1;
	}else r3dOutToLog("HunZ - Creating identity!\n");

	/* Connect to server on localhost:9987 with nickname "client", no default channel, no default channel password and server password "secret" */
	if((error = ts3client_startConnection(scHandlerID, identity, "localhost", 9987, clientName, NULL, "", "secret")) != ERROR_ok) {
		r3dOutToLog("HunZ - Error connecting to server: %d\n", error);
		//return 1;
		char msg[128];
		sprintf(msg, "Error connecting to server: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Connecting to server!\n");

	ts3client_freeMemory(identity);  /* Release dynamically allocated memory */
	identity = NULL;

	r3dOutToLog("HunZ - Client lib initialized and running\n");

	/* Query and print client lib version */
	if((error = ts3client_getClientLibVersion(&version)) != ERROR_ok) {
		r3dOutToLog("HunZ - Failed to get clientlib version: %d\n", error);
		//return 1;
		char msg[128];
		sprintf(msg, "Failed to get clientlib version: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Get clientlib version!\n");

	r3dOutToLog("HunZ - Client lib version: %s\n", version);
	ts3client_freeMemory(version);  /* Release dynamically allocated memory */
	version = NULL;
	//::Sleep(500);
}

void Teamspeak::Disconnect()
{
	unsigned int error;

	/* Disconnect from server */
	if((error = ts3client_stopConnection(scHandlerID, "leaving")) != ERROR_ok) {
		r3dOutToLog("HunZ - Error stopping connection: %d\n", error);
		//return 1;
	}else r3dOutToLog("HunZ - Stopping connection!\n");
	
	/* Destroy server connection handler */
	if((error = ts3client_destroyServerConnectionHandler(scHandlerID)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error destroying clientlib: %d\n", error);
		//return 1;
	}else r3dOutToLog("HunZ - Destroying clientlib!\n");

	/* Shutdown client lib */
	if((error = ts3client_destroyClientLib()) != ERROR_ok) {
		r3dOutToLog("HunZ - Failed to destroy clientlib: %d\n", error);
		//return 1;
	}else r3dOutToLog("HunZ - Destroy clientlib!\n");
}

void Teamspeak::muteClients(int clientID)
{
	unsigned int error;
	anyID clientIDArray[2];
	clientIDArray[0] = clientID;
	clientIDArray[1] = 0;

	if((error = ts3client_requestMuteClients(scHandlerID, clientIDArray, NULL)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error muting clients: %d\n", error);
		char msg[128];
		sprintf(msg, "Error muting clients: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Muting clients!\n");
}

void Teamspeak::unmuteClients(int clientID)
{
	unsigned int error;
	anyID clientIDArray[2];
	clientIDArray[0] = clientID;
	clientIDArray[1] = 0;

	if((error = ts3client_requestUnmuteClients(scHandlerID, clientIDArray, NULL)) != ERROR_ok) {
		r3dOutToLog("HunZ - Error unmuting clients: %d\n", error);
		char msg[128];
		sprintf(msg, "Error unmuting clients: %d", error);
		hudMain->showChat(true);
		hudMain->addChatMessage(1, "<system>", msg, 0);
	}else r3dOutToLog("HunZ - Muting clients!\n");
}