#pragma once

#include "public_definitions.h"
#include <stdio.h>

class Teamspeak
{
public:
	Teamspeak();
	~Teamspeak();

public:

	uint64 serverID;

	void Init();
	void Connect(const char* clientName);
	void Disconnect();
	void muteClients(int clientID);
	void unmuteClients(int clientID);

private:
	uint64 scHandlerID;
	char *identity;
};