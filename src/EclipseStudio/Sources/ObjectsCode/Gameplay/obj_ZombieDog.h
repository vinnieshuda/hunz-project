//=========================================================================
//	HunZi	
//=========================================================================

#pragma once

#include "multiplayer/P2PMessages.h"
#include "multiplayer/NetCellMover.h"
#include "ZombieStates.h"

class r3dPhysDogSkeleton;

class obj_ZombieDog: public GameObject
{
	DECLARE_CLASS(obj_ZombieDog, GameObject)
	
private:
	int	physXObstacleIndex_;

	
public:
	/**	Zombie animation system. */
	r3dAnimation	anim_;
	r3dPhysDogSkeleton* physSke;

	r3dMesh*	zombieParts[1]; //head/body/legs/special
	bool		gotLocalBbox;
	float		walkAnimCoef;
	void		UpdateAnimations();
	int		AddAnimation(const char* anim);

	void		StartWalkAnim(bool run);

	void		ProcessMovement();

	int		ZombieState;
	float		StateTimer;

	bool	bDead;

	int UpdateWarmUp;
	int PhysicsOn;

	void		SetZombieState(int state, bool atStart);
	
	void		DoDeath();

public:
	obj_ZombieDog();
	~obj_ZombieDog();

	virtual	BOOL OnCreate();
	virtual BOOL Update();
	virtual BOOL OnDestroy();
	virtual void OnPreRender();

	virtual void AppendShadowRenderables(RenderArray &rarr, const r3dCamera& cam);
	virtual void AppendRenderables(RenderArray ( & render_arrays  )[ rsCount ], const r3dCamera& cam);
};

