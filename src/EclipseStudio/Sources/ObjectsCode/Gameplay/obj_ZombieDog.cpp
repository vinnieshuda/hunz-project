//=========================================================================
//	HunZi
//	
//=========================================================================

#include "r3dPCH.h"
#include "r3d.h"

#include "obj_ZombieDog.h"
#include "../AI/r3dPhysSkeleton.h"

#include "ObjectsCode/Weapons/WeaponArmory.h"
#include "ObjectsCode/Weapons/HeroConfig.h"
#include "../../multiplayer/ClientGameLogic.h"
#include "../AI/AI_Player.H"

//////////////////////////////////////////////////////////////////////////

extern r3dCamera gCam;

IMPLEMENT_CLASS(obj_ZombieDog, "obj_ZombieDog", "Object");
AUTOREGISTER_CLASS(obj_ZombieDog);
r3dSkeleton* g_zombieDogBindSkeleton = NULL;
r3dAnimPool* g_zombieDogAnimPool = NULL;

/////////////////////////////////////////////////////////////////////////

namespace
{
	struct ZombieDogMeshesDeferredRenderable: public MeshDeferredRenderable
	{
		ZombieDogMeshesDeferredRenderable()
			: Parent(0)
		{
		}

		void Init()
		{
			DrawFunc = Draw;
		}

		static void Draw(Renderable* RThis, const r3dCamera& Cam)
		{
			R3DPROFILE_FUNCTION("ZombieDogMeshesDeferredRenderable");

			ZombieDogMeshesDeferredRenderable* This = static_cast<ZombieDogMeshesDeferredRenderable*>(RThis);

			r3dApplyPreparedMeshVSConsts(This->Parent->preparedVSConsts);
			This->Parent->OnPreRender();

			MeshDeferredRenderable::Draw(RThis, Cam);
		}

		obj_ZombieDog *Parent;
	};

	//////////////////////////////////////////////////////////////////////////

	struct ZombieDogMeshesShadowRenderable: public MeshShadowRenderable
	{
		void Init()
		{
			DrawFunc = Draw;
		}

		static void Draw( Renderable* RThis, const r3dCamera& Cam )
		{
			R3DPROFILE_FUNCTION("ZombieDogMeshesShadowRenderable");
			ZombieDogMeshesShadowRenderable* This = static_cast< ZombieDogMeshesShadowRenderable* >( RThis );

			r3dApplyPreparedMeshVSConsts(This->Parent->preparedVSConsts);

			This->Parent->OnPreRender();

			This->SubDrawFunc( RThis, Cam );
		}

		obj_ZombieDog *Parent;
	};
} // unnamed namespace

//////////////////////////////////////////////////////////////////////////

void obj_ZombieDog::OnPreRender()
{
	anim_.GetCurrentSkeleton()->SetShaderConstants();
}

void obj_ZombieDog::AppendRenderables(RenderArray ( & render_arrays  )[ rsCount ], const r3dCamera& cam)
{

		r3dMesh *mesh = zombieParts[0];

		uint32_t prevCount = render_arrays[ rsFillGBuffer ].Count();
		mesh->AppendRenderablesDeferred( render_arrays[ rsFillGBuffer ], r3dColor::white );

		for( uint32_t i = prevCount, e = render_arrays[ rsFillGBuffer ].Count(); i < e; i ++ )
		{
			ZombieDogMeshesDeferredRenderable& rend = static_cast<ZombieDogMeshesDeferredRenderable&>( render_arrays[ rsFillGBuffer ][ i ] ) ;

			rend.Init();
			rend.Parent = this;
		}

}

//////////////////////////////////////////////////////////////////////////

void obj_ZombieDog::AppendShadowRenderables(RenderArray &rarr, const r3dCamera& cam)
{
	float distSq = (gCam - GetPosition()).LengthSq();
	float dist = sqrtf( distSq );
	UINT32 idist = (UINT32) R3D_MIN( dist * 64.f, (float)0x3fffffff );

	r3dMesh *mesh = zombieParts[0];

	uint32_t prevCount = rarr.Count();

	mesh->AppendShadowRenderables( rarr );

	for( uint32_t i = prevCount, e = rarr.Count(); i < e; i ++ )
	{
		ZombieDogMeshesShadowRenderable& rend = static_cast<ZombieDogMeshesShadowRenderable&>( rarr[ i ] );

		rend.Init() ;
		rend.SortValue |= idist;
		rend.Parent = this ;
	}
}

obj_ZombieDog::obj_ZombieDog() : 
  physXObstacleIndex_(-1)
{
	UpdateWarmUp = 0;
	PhysicsOn = 1;

	ObjTypeFlags |= OBJTYPE_ZombieDog;
	physSke = NULL;
	m_bEnablePhysics = false;
	m_isSerializable = false;

	if(g_zombieDogBindSkeleton == NULL) 
	{
		g_zombieDogBindSkeleton = new r3dSkeleton();
		g_zombieDogBindSkeleton->LoadBinary("Data\\ObjectsDepot\\teszt2\\default.skl");
		
		g_zombieDogAnimPool = new r3dAnimPool();
	}

	bDead = false;
}

//////////////////////////////////////////////////////////////////////////

obj_ZombieDog::~obj_ZombieDog()
{

}

BOOL obj_ZombieDog::OnCreate()
{
	
	anim_.Init(g_zombieDogBindSkeleton, g_zombieDogAnimPool, NULL, (DWORD)this);

	//Sco betoltes es mesh.
	char tmpBuf[512];
	sprintf(tmpBuf, "Data/ObjectsDepot/teszt2/default.sco");

	zombieParts[0] =  r3dGOBAddMesh(tmpBuf, true, false, true, false );
	//BBox baszas
	r3dBoundBox bbox;
	bbox.Org	= r3dPoint3D(-0.2f, 0.0f, -0.2f);
	bbox.Size	= r3dPoint3D(+0.2f, 0.2f, +0.2f);
	SetBBoxLocal(bbox);
	gotLocalBbox = false;
	// set zombie to starting state.
	ZombieState = -1;
	StateTimer  = -1;
	// special case if zombie is dead already, just spawn dead pose anim
	// there is 1-7 dead poses
/*
	char aname[128];
	sprintf(aname, "111asd");
	int aid = AddAnimation(aname);
	anim_.StartAnimation(aid, ANIMFLAG_RemoveOtherNow | ANIMFLAG_Looped, 1.0f, 1.0f, 0.0f);
	ZombieState = EZombieStates::ZState_Walk;
	physXObstacleIndex_ = AcquirePlayerObstacle(GetPosition());
	UpdateAnimations();
	physSke = new r3dPhysDogSkeleton( "data/ObjectsDepot/Characters/hunzi.RepX" ) ;
	physSke->linkParent(g_zombieDogBindSkeleton, GetTransformMatrix(), this, PHYSCOLL_NETWORKPLAYER) ;
	SetBBoxLocal(physSke->getWorldBBox());*/
/*
	physSkeleton = AquireCacheSkeleton();
	physSkeleton->linkParent(anim_.GetCurrentSkeleton(), GetTransformMatrix(), this, PHYSCOLL_NETWORKPLAYER) ;*/
	parent::OnCreate();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////

BOOL obj_ZombieDog::Update()
{
	parent::Update();

	UpdateAnimations();
/*	physSke->syncAnimation(anim_.GetCurrentSkeleton(), GetTransformMatrix(), anim_);*/
	UpdatePlayerObstacle(physXObstacleIndex_, GetPosition());
	if(!gotLocalBbox)
	{
		r3dBoundBox bbox;
		bbox.InitForExpansion();
		if(zombieParts[0] && zombieParts[0]->IsLoaded()) {
			bbox.ExpandTo(zombieParts[0]->localBBox);
			gotLocalBbox = true;
		} else {
			gotLocalBbox = false;
		}
		if(gotLocalBbox) SetBBoxLocal(bbox);
	}

	if(ZombieState == EZombieStates::ZState_Walk || ZombieState == EZombieStates::ZState_Pursue)
	{
		float spd = GetVelocity().Length();

		//@HACK. because of huge delay on network game creation, there will be BIG zombie teleportation on first actual game frame when all movement packets will be proceeded
		//and animation will be fucked up, so limit speed here
		if(spd > 10) spd = 10;
	}
	//ProcessMovement();

/*
	if(!bDead)
	{
		UpdatePlayerObstacle(PhysXObstacleIndex, GetPosition());
	}*/

	return TRUE;
}

void obj_ZombieDog::ProcessMovement()
{
	r3d_assert(!NetworkLocal);
	
/*
	float timePassed = r3dGetFrameTime();
	
	if(GetVelocity().LengthSq() > 0.0001f)
	{
		R3DPROFILE_FUNCTION("NetZombie_Move");
		r3dPoint3D nextPos = GetPosition() + GetVelocity() * timePassed;
		
		// check if we overmoved to target position
		r3dPoint3D v = netMover.GetNetPos() - nextPos;
		float d = GetVelocity().Dot(v);
		if(d < 0) {
			nextPos = netMover.GetNetPos();
			SetVelocity(r3dPoint3D(0, 0, 0));
		}

		SetPosition(nextPos);
	}*/
}

int obj_ZombieDog::AddAnimation(const char* anim)
{
	char buf[MAX_PATH];
	sprintf(buf, "Data\\Animations5\\Zombie\\%s.anm", anim);
	int aid = g_zombieDogAnimPool->Add(anim, buf);
	
	return aid;
}	

void obj_ZombieDog::UpdateAnimations()
{
	const float TimePassed = r3dGetFrameTime();

	float zombRadiusSqr = g_zombie_update_radius->GetFloat();
	zombRadiusSqr *= zombRadiusSqr;

	D3DXMATRIX CharDrawMatrix;
	D3DXMatrixIdentity(&CharDrawMatrix);
	anim_.Update(TimePassed, r3dPoint3D(0,0,0), CharDrawMatrix);
	anim_.Recalc();
}

//////////////////////////////////////////////////////////////////////////

BOOL obj_ZombieDog::OnDestroy()
{
	parent::OnDestroy();

	return TRUE;
}

void obj_ZombieDog::DoDeath()
{
	bDead = true;
	// clear rotation so it won't affect ragdoll bbox
	SetRotationVector(r3dPoint3D(0, 0, 0));
	
	anim_.StopAll();
}

void obj_ZombieDog::StartWalkAnim(bool run) //HunZ - zombi animaciok
{
	int aid = 0;
	float wsk = 1.0f;	// walk animtion speed coefficient

	aid = AddAnimation("111asd");
	wsk = .8f; //2.2f;
	
	walkAnimCoef = wsk;
	int tr = anim_.StartAnimation(aid, ANIMFLAG_RemoveOtherFade | ANIMFLAG_Looped, 0.0f, 1.0f, 0.1f);

	// start with randomized frame
	r3dAnimation::r3dAnimInfo* ai = anim_.GetTrack(tr);
	ai->fCurFrame = u_GetRandom(0, (float)ai->pAnim->NumFrames - 1);
}

void obj_ZombieDog::SetZombieState(int state, bool atStart)
{
	r3d_assert(ZombieState != state);
	ZombieState = state;
	
	switch(ZombieState)
	{
		default:
			r3dError("unknown zombie state %d", ZombieState);
			return;

		case EZombieStates::ZState_Sleep:
		{
			int aid = 0;
			switch(u_random(2)) {
				default:
				case 0:	aid = AddAnimation("Zombie_Dead_Stand_B_01"); break;
				case 1:	aid = AddAnimation("Zombie_Dead_Stand_F_01"); break;
			}
			anim_.StartAnimation(aid, ANIMFLAG_RemoveOtherNow | ANIMFLAG_PauseOnEnd | ANIMSTATUS_Paused, 1.0f, 1.0f, 1.0f);
			break;
		}
		
		// wake up zombie, unpause animation from Sleep
		case EZombieStates::ZState_Waking:
			if(!atStart)
			{
				r3d_assert(anim_.AnimTracks.size() == 1);
				if(anim_.AnimTracks.size() > 0)
					anim_.AnimTracks[0].dwStatus &= ~ANIMSTATUS_Paused;
				break;
			}
			// note - waking zombies created at object creation phase will fallthru to idle phase.

		case EZombieStates::ZState_Idle:
		{
			// select new idle animation
			int aid = 0;

			switch(u_random(2)) {
				default:
				case 0:	aid = AddAnimation("Zombie_Idle_01"); break;
				case 1:	aid = AddAnimation("Zombie_Idle_02"); break;
			}
			if(u_random(10) == 0)
				aid = AddAnimation("Zombie_Eat_Crouched_01");

			int tr = anim_.StartAnimation(aid, ANIMFLAG_RemoveOtherFade | ANIMFLAG_Looped, 0.0f, 1.0f, 0.2f);

			// start with randomized frame
			r3dAnimation::r3dAnimInfo* ai = anim_.GetTrack(tr);
			ai->fCurFrame = u_GetRandom(0, (float)ai->pAnim->NumFrames - 1);
			break;
		}

		case EZombieStates::ZState_Walk:
			StartWalkAnim(false);
			break;
		case EZombieStates::ZState_MoveAwayFromEnemy:
			StartWalkAnim(false);
			break;
		case EZombieStates::ZState_Pursue:
			StartWalkAnim(true);
			break;

		case EZombieStates::ZState_Dead:
			DoDeath();
			return;
			
		case EZombieStates::ZState_Attack:
		case EZombieStates::ZState_BarricadeAttack:
			// select new attack animation
			int aid = 0;
			switch(u_random(2)) {
				default:
				case 0:	aid = AddAnimation("Zombie_Swing_Attack01"); break;
				case 1:	aid = AddAnimation("Zombie_Swing_Attack02"); break;
			}

			SetVelocity(r3dPoint3D(0, 0, 0));
			break;
	}
}