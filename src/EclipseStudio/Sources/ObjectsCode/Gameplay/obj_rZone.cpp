#include "r3dPCH.h"
#include "r3d.h"

#include "GameCommon.h"
#include "obj_rZone.h"
#include "XMLHelpers.h"

extern bool g_bEditMode;

IMPLEMENT_CLASS(obj_rZone, "obj_rZone", "Object");
AUTOREGISTER_CLASS(obj_rZone);

std::vector<obj_rZone*> obj_rZone::LoadedrZonees;

namespace
{
	struct obj_rZoneCompositeRenderable: public Renderable
	{
		void Init()
		{
			DrawFunc = Draw;
		}

		static void Draw( Renderable* RThis, const r3dCamera& Cam )
		{
			obj_rZoneCompositeRenderable *This = static_cast<obj_rZoneCompositeRenderable*>(RThis);

			r3dRenderer->SetTex(NULL);
			r3dRenderer->SetMaterial(NULL);
			r3dRenderer->SetRenderingMode(R3D_BLEND_NZ | R3D_BLEND_ZC);

			r3dDrawLine3D(This->Parent->GetPosition(), This->Parent->GetPosition() + r3dPoint3D(0, 20.0f, 0), Cam, 0.4f, r3dColor24::grey);
			r3dDrawCircle3D(This->Parent->GetPosition(), This->Parent->useRadius, Cam, 0.1f, r3dColor(255, 0, 0));

			r3dRenderer->Flush();
			r3dRenderer->SetRenderingMode(R3D_BLEND_POP);
		}

		obj_rZone *Parent;	
	};
}

obj_rZone::obj_rZone()
{
	useRadius = 10.0f;
}

obj_rZone::~obj_rZone()
{
}

#define RENDERABLE_OBJ_USER_SORT_VALUE (3*RENDERABLE_USER_SORT_VALUE)
void obj_rZone::AppendRenderables(RenderArray (& render_arrays  )[ rsCount ], const r3dCamera& Cam)
{
	parent::AppendRenderables(render_arrays, Cam);
#ifdef FINAL_BUILD
	return;
#else
	if(g_bEditMode)
	{
		obj_rZoneCompositeRenderable rend;
		rend.Init();
		rend.Parent		= this;
		rend.SortValue	= RENDERABLE_OBJ_USER_SORT_VALUE;
		render_arrays[ rsDrawDebugData ].PushBack( rend );
	}
#endif
}

void obj_rZone::ReadSerializedData(pugi::xml_node& node)
{
	GameObject::ReadSerializedData(node);

	pugi::xml_node objNode = node.child("rZone");
	GetXMLVal("useRadius", objNode, &useRadius);
}

void obj_rZone::WriteSerializedData(pugi::xml_node& node)
{
	GameObject::WriteSerializedData(node);

	pugi::xml_node objNode = node.append_child();
	objNode.set_name("rZone");
	SetXMLVal("useRadius", objNode, &useRadius);
}

BOOL obj_rZone::Load(const char *fname)
{
	const char* cpMeshName = "Data\\ObjectsDepot\\Capture_Points\\cr_control_point_flag_red_01.sco";

	if(!parent::Load(cpMeshName)) 
		return FALSE;

	return TRUE;
}

BOOL obj_rZone::OnCreate()
{
	parent::OnCreate();

	LoadedrZonees.push_back(this);
	return 1;
}


BOOL obj_rZone::OnDestroy()
{
	LoadedrZonees.erase(std::find(LoadedrZonees.begin(), LoadedrZonees.end(), this));
	return parent::OnDestroy();
}

BOOL obj_rZone::Update()
{
	return parent::Update();
}

//------------------------------------------------------------------------
#ifndef FINAL_BUILD
float obj_rZone::DrawPropertyEditor(float scrx, float scry, float scrw, float scrh, const AClass* startClass, const GameObjects& selected)
{
	float starty = scry;

	starty += parent::DrawPropertyEditor(scrx, scry, scrw,scrh, startClass, selected );

	if( IsParentOrEqual( &ClassData, startClass ) )
	{		
		starty += imgui_Static ( scrx, starty, "rZone Parameters" );
		starty += imgui_Value_Slider(scrx, starty, "rZone Radius", &useRadius, 0, 500.0f, "%.0f");
	}

	return starty-scry;
}
#endif
