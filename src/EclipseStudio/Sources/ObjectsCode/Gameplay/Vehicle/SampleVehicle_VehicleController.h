// This code contains NVIDIA Confidential Information and is disclosed to you
// under a form of NVIDIA software license agreement provided separately to you.
//
// Notice
// NVIDIA Corporation and its licensors retain all intellectual property and
// proprietary rights in and to this software and related documentation and
// any modifications thereto. Any use, reproduction, disclosure, or
// distribution of this software and related documentation without an express
// license agreement from NVIDIA Corporation is strictly prohibited.
//
// ALL NVIDIA DESIGN SPECIFICATIONS, CODE ARE PROVIDED "AS IS.". NVIDIA MAKES
// NO WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ALL IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Information and code furnished is believed to be accurate and reliable.
// However, NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright (c) 2008-2013 NVIDIA Corporation. All rights reserved.
// Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
// Copyright (c) 2001-2004 NovodeX AG. All rights reserved.  


#ifndef SAMPLE_VEHICLE_VEHICLE_CONTROLLER_H
#define SAMPLE_VEHICLE_VEHICLE_CONTROLLER_H

#include "common/PxPhysXCommon.h"
#include "foundation/PxVec3.h"
#include "vehicle/PxVehicleSDK.h"
#include "vehicle/PxVehicleUtilControl.h"

using namespace physx;

class SampleVehicle_VehicleController
{
public:

	SampleVehicle_VehicleController();
	~SampleVehicle_VehicleController();

	void toggleAutoGearFlag() 
	{
		mToggleAutoGears = true;
	}

	void update(/*const PxF32 dtime, */PxVehicleWheels& focusVehicle);

	void clear();

private:

	// Raw data taken from the correct stream (live input stream or replay stream)
	bool			mUseKeyInputs;

	// Toggle autogears flag on focus vehicle
	bool			mToggleAutoGears;

	//Auto-reverse mode.
	bool			mIsMovingForwardSlowly;
	bool			mInReverseMode;

	//Update 
	void processRawInputs(/*const PxF32 timestep, */const bool useAutoGears, PxVehicleDrive4WRawInputData& rawInputData);
	void processAutoReverse(
		const PxVehicleWheels& focusVehicle, const PxVehicleDriveDynData& driveDynData, const PxVehicleDrive4WRawInputData& rawInputData, 
		bool& toggleAutoReverse, bool& newIsMovingForwardSlowly) const;

};

#endif //SAMPLE_VEHICLE_VEHICLE_CONTROLLER_H