#pragma once

#include "multiplayer/P2PMessages.h"
#include "multiplayer/NetCellMover.h"
#include "../../../../GameEngine/gameobjects/GameObj.h"
#include "vehicle/PxVehicleDrive.h"

struct VehicleDescriptor;
class obj_VehicleSpawn;
class obj_Player;

class obj_Vehicle: public MeshGameObject
{
	DECLARE_CLASS(obj_Vehicle, MeshGameObject)
	
public:
	PKT_S2C_CreateVehicle_s CreateParams;

	CNetCellMover	netMover;

	VehicleDescriptor *vd;
	obj_Player* driver;
	const VehicleDescriptor* getVehicleDescriptor() { return vd; }

	r3dPoint3D	netVelocity;
	float		v_rotation;
	PxTransform pxpos;

	bool		controlled;
	float		rpm;

	void*	m_vehicleSound;

	void			UpdatePositionFromPhysx();
	void			UpdateNotMovingPose();
	void			SwitchToDrivable(bool doDrive);
	void			SwitchToNonDriveable();
	void			SyncPhysicsPoseWithObjectPose();
	void			SetBoneMatrices();
	const bool		getExitSpace( r3dVector& outVector, int exitIndex );
	void			UpdatePositionFromRemote();
	void			SetPositionForPhysx(const r3dPoint3D& pos);
	void			SetRotationVectorForPhysx(const r3dVector& Angles);
	r3dVector GetRotation();

	void setVehicleSpawner( obj_VehicleSpawn* targetSpawner) { spawner = targetSpawner;}
	obj_VehicleSpawn* spawner;

	virtual	BOOL	OnNetReceive(DWORD EventID, const void* packetData, int packetSize);
	void			OnNetPacket(const PKT_C2C_MoveSetCell_s& n);
	void			OnNetPacket(const PKT_C2C_MoveRel_s& n);
	void			OnNetPacket(const PKT_C2C_AutoUpdate_s& n);

public:
	obj_Vehicle();
	~obj_Vehicle();

	virtual	BOOL OnCreate();
	virtual BOOL Update();
	virtual BOOL OnDestroy();
	virtual void OnPreRender() { SetBoneMatrices(); }	
};

