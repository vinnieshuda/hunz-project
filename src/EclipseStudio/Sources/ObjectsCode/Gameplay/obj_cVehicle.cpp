#include "r3dPCH.h"
#include "r3d.h"

#include "obj_cVehicle.h"
#include "../../multiplayer/ClientGameLogic.h"
#include "../AI/AI_Player.H"
#include "../../../../GameEngine/gameobjects/PhysXWorld.h"
#include "../../../../GameEngine/gameobjects/VehicleDescriptor.h"
#include "../../../../GameEngine/gameobjects/VehicleManager.h"
#include "../../../../GameEngine/gameobjects/ObjManag.h"
#include "../../GameCommon.h"

//////////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(obj_Vehicle, "obj_Vehicle", "Object");
AUTOREGISTER_CLASS(obj_Vehicle);

/////////////////////////////////////////////////////////////////////////

namespace
{
	void QuaternionToEulerAngles(PxQuat &q, float &xRot, float &yRot, float &zRot)
	{
		//q.normalize();

		void MatrixGetYawPitchRoll ( const D3DXMATRIX & mat, float & fYaw, float & fPitch, float & fRoll );
		PxMat33 mat(q);
		D3DXMATRIX res;
		D3DXMatrixIdentity(&res);
		res._11 = mat.column0.x;
		res._12 = mat.column0.y;
		res._13 = mat.column0.z;

		res._21 = mat.column1.x;
		res._22 = mat.column1.y;
		res._23 = mat.column1.z;

		res._31 = mat.column2.x;
		res._32 = mat.column2.y;
		res._33 = mat.column2.z;

		MatrixGetYawPitchRoll(res, xRot, yRot, zRot);
	}

	//////////////////////////////////////////////////////////////////////////

	/** Helper constant transformation factors */
	struct UsefulTransforms 
	{
		PxQuat rotY_quat;
		D3DXMATRIX rotY_mat;

		UsefulTransforms()
		{
			D3DXQUATERNION rotY_D3D;
			D3DXQuaternionRotationYawPitchRoll(&rotY_D3D, /*D3DX_PI / 2*/0, 0, 0); // HunZ - ujabb titov bullshit
			rotY_quat = PxQuat(rotY_D3D.x, rotY_D3D.y, rotY_D3D.z, rotY_D3D.w);
			D3DXMatrixRotationQuaternion(&rotY_mat, &rotY_D3D);
		}
	};
}

/////////////////////////////////////////

obj_Vehicle::obj_Vehicle() : 
  netMover(this, 0.1f, (float)PKT_C2C_MoveSetCell_s::PLAYER_CELL_RADIUS)
{
	ObjTypeFlags |= OBJTYPE_Vehicle;
	ObjFlags |= OBJFLAG_DisableShadows;
	m_bEnablePhysics = false;
	m_isSerializable = false;
	controlled = false;
	
}

void obj_Vehicle::UpdateNotMovingPose()
{
	if (!vd)
		return;

	PxRigidDynamicFlags f = vd->vehicle->getRigidDynamicActor()->getRigidDynamicFlags();
	if ( !(f & PxRigidDynamicFlag::eKINEMATIC) && NetworkLocal)
	{
		PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();
		SetPosition(r3dVector(t.p.x, t.p.y, t.p.z));
		r3dVector angles;
		QuaternionToEulerAngles(t.q, angles.x, angles.y, angles.z);
		//	To degrees
		angles = D3DXToDegree(angles);

		SetRotationVector(angles);
		SyncPhysicsPoseWithObjectPose();
	}
}

void obj_Vehicle::UpdatePositionFromPhysx()
  {
	R3DPROFILE_FUNCTION("obj_Vehicle::UpdatePositionFromPhysx");

	if (!vd)
		return;
	if(!NetworkLocal)
		return;

	PxRigidDynamicFlags f = vd->vehicle->getRigidDynamicActor()->getRigidDynamicFlags();
	if ( !(f & PxRigidDynamicFlag::eKINEMATIC) && NetworkLocal)
	  {
		  PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();
		  SetPosition(r3dVector(t.p.x, t.p.y, t.p.z));
		  r3dVector angles;
		  QuaternionToEulerAngles(t.q, angles.x, angles.y, angles.z);
		  //	To degrees
		  angles = D3DXToDegree(angles);

		  SetRotationVector(angles);
		  SyncPhysicsPoseWithObjectPose();
	  }
  }

//////////////////////////////////////////////////////////////////////////

obj_Vehicle::~obj_Vehicle()
{
	g_pPhysicsWorld->m_VehicleManager->DeleteCar(vd);
	m_vehicleSound = NULL;
}

BOOL obj_Vehicle::OnCreate()
{
	R3DPROFILE_FUNCTION("obj_Vehicle::OnCreate");
	
	if (!parent::OnCreate())
		return FALSE;

	r3dMesh *m = MeshLOD[0];
	m_vehicleSound = 0;

	if (!m)
		return FALSE;

	vd = g_pPhysicsWorld->m_VehicleManager->CreateVehicle(m, CreateParams.spawnPos, CreateParams.rotM);

	if (vd)
	{
		vd->owner = this;

		SyncPhysicsPoseWithObjectPose();

		PxRigidDynamicFlags f = vd->vehicle->getRigidDynamicActor()->getRigidDynamicFlags();

		if ( !(f & PxRigidDynamicFlag::eKINEMATIC))
		{

			PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();

			SetPosition(r3dVector(t.p.x, t.p.y, t.p.z));
			r3dVector angles;
			QuaternionToEulerAngles(t.q, angles.x, angles.y, angles.z);
			//	To degrees
			angles = D3DXToDegree(angles);
			pxpos = t;
			PKT_C2C_AutoUpdate_s n;
			n.pxtrans = t;
			p2pSendToHost(this, &n, sizeof(n));

			SetRotationVector(r3dVector(-90,0,0));
			SyncPhysicsPoseWithObjectPose();
		}
	}
	r3dBoundBox bbox;
	bbox.Org	= r3dPoint3D(-2.2f, 0.0f, -2.2f);
	bbox.Size	= r3dPoint3D(+2.2f, 2.0f, +2.2f);
	SetBBoxLocal(bbox);
	return TRUE;
	//return vd != 0;	
}

r3dVector obj_Vehicle::GetRotation()
{
	PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();
	r3dVector angles;
	QuaternionToEulerAngles(t.q, angles.x, angles.y, angles.z);
	//	To degrees
	angles = D3DXToDegree(angles);
	return angles;
}

void obj_Vehicle::UpdatePositionFromRemote()
{
	//r3d_assert(!NetworkLocal);

	const float fTimePassed = r3dGetFrameTime();

	r3dVector currentRotation = GetRotationVector();
	float rotX      = currentRotation.x;
	float turnAngle = netMover.NetData().turnAngle;
	if(fabs(rotX - turnAngle) > 0.01f) 
	{
		extern float getMinimumAngleDistance(float from, float to);
		float f1 = getMinimumAngleDistance(rotX, turnAngle);
		rotX += ((f1 < 0) ? -1 : 1) * fTimePassed * 360;
		float f2 = getMinimumAngleDistance(rotX, turnAngle);
		if((f1 > 0 && f2 <= 0) || (f1 < 0 && f2 >= 0))
			rotX = turnAngle;

		currentRotation.x = rotX;
		SetRotationVector( currentRotation );
	}

	if(netVelocity.LengthSq() > 0.0001f)
	{
		SetPosition(GetPosition() + netVelocity * fTimePassed);

		// check if we overmoved to target position
		r3dPoint3D v = netMover.GetNetPos() - GetPosition();
		float d = netVelocity.Dot(v);
		if(d < 0) {
			SetPosition(netMover.GetNetPos());
			netVelocity = r3dPoint3D(0, 0, 0);
		}
	}
}

void obj_Vehicle::SwitchToDrivable(bool doDrive)
{
	if (vd && vd->vehicle->getRigidDynamicActor())
	{
		vd->vehicle->getRigidDynamicActor()->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, !doDrive);
		if (doDrive)
			g_pPhysicsWorld->m_VehicleManager->DriveCar(vd);
	}
}
void obj_Vehicle::SwitchToNonDriveable()
{
	if (vd && vd->vehicle->getRigidDynamicActor())
	{
		vd->vehicle->getRigidDynamicActor()->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, false);
		g_pPhysicsWorld->m_VehicleManager->UnDriveCar(vd);
	}
}

const bool obj_Vehicle::getExitSpace( r3dVector& outVector, int exitIndex )
{
	return vd->GetExitIndex( outVector,exitIndex);
}

void obj_Vehicle::SyncPhysicsPoseWithObjectPose()
{
	if (!vd)
		return;

	/*PxRigidDynamicFlags f = vd->vehicle->getRigidDynamicActor()->getRigidDynamicFlags();
	if (!(f & PxRigidDynamicFlag::eKINEMATIC))
	{
		r3dOutToLog("HunZ - !dynamicflag\n");
		return;
	}*/

	r3dPoint3D pos(GetPosition());
	D3DXMATRIX rotM(GetRotationMatrix());
	D3DXQUATERNION q;
	D3DXQuaternionRotationMatrix(&q, &rotM);
	PxQuat quat(q.x, q.y, q.z, q.w);

	PxTransform carPose(PxVec3(pos.x, pos.y, pos.z), quat);
	vd->vehicle->getRigidDynamicActor()->setGlobalPose(carPose);
}

void obj_Vehicle::SetPositionForPhysx(const r3dPoint3D& pos)
{

}

void obj_Vehicle::SetRotationVectorForPhysx(const r3dVector& Angles)
{

}

void obj_Vehicle::SetBoneMatrices()
{
	if (!vd)
		return;

	PxRigidDynamic *a = vd->vehicle->getRigidDynamicActor();
	D3DXMATRIX boneTransform;
	
	static const UsefulTransforms T;

	//	Init with identities
	for (int i = 0; i < vd->skl->NumBones; ++i)
	{
		r3dBone &b = vd->skl->Bones[i];
		b.CurrentTM = T.rotY_mat;
	}

	//	Retrieve vehicle wheels positions
	// HunZ - TODO: kerek poz kipofozasa
	PxShape *shapes[VEHICLE_PARTS_COUNT] = {0};
	PxU32 sn = a->getShapes(shapes, VEHICLE_PARTS_COUNT);
	for (PxU32 i = 0; i < sn; ++i)
	{
		PxShape *s = shapes[i];
		PxU32 boneIndex = reinterpret_cast<PxU32>(s->userData);
		r3dBone &b = vd->skl->Bones[boneIndex];
		PxTransform pose = s->getLocalPose();
		
		PxVec3 bonePos = PxVec3(b.vRelPlacement.x, b.vRelPlacement.y, b.vRelPlacement.z);
		D3DXMATRIX toOrigin, fromOrigin, suspensionOffset;
		D3DXMatrixTranslation(&toOrigin, -bonePos.x, -bonePos.y, -bonePos.z);

		PxVec3 bonePosNew = T.rotY_quat.rotate(bonePos);
		D3DXMatrixTranslation(&fromOrigin, pose.p.x, pose.p.y, pose.p.z);
		pose.q = pose.q * T.rotY_quat;
		D3DXQUATERNION q(pose.q.x, pose.q.y, pose.q.z, pose.q.w);
		D3DXMatrixRotationQuaternion(&boneTransform, &q);

		D3DXMatrixMultiply(&boneTransform, &toOrigin, &boneTransform);
		D3DXMatrixMultiply(&boneTransform, &boneTransform, &fromOrigin); 
		
		bool isRootBone = strstr(vd->skl->Bones[boneIndex].Name, "Bone_Root") != NULL ? true : false;
		if (isRootBone)
			b.CurrentTM = boneTransform;
		else
		{
			D3DXQUATERNION test;
			D3DXQuaternionRotationYawPitchRoll(&test, D3DXToRadian(13), 0, 0);

			D3DXMatrixRotationQuaternion(&boneTransform, &test);

			b.CurrentTM = boneTransform;
		}
	}
	vd->skl->SetShaderConstants();
}

BOOL obj_Vehicle::Update()
{
	parent::Update();

	if(!vd)
		return TRUE;

	R3DPROFILE_FUNCTION("obj_Vehicle::Update");

	r3dMesh *m = MeshLOD[0];

	r3dBoundBox bbox;
	bbox.InitForExpansion();
	bbox.ExpandTo(m->localBBox);
	SetBBoxLocal(bbox);

	if(NetworkLocal) 
	{

		
		float maxRevs = vd->vehicle->mDriveDynData.getEngineRotationSpeed();//Convert from radians per second to rpm
		if(!m_vehicleSound)
		{
			m_vehicleSound = SoundSys.Play(SoundSys.GetEventIDByPath("Sounds/vehicle/vehicle_sound"),GetPosition(), true);
		}else
		{
			SoundSys.SetSoundPos(m_vehicleSound, GetPosition());
			SoundSys.SetParamValue(m_vehicleSound, "rpm", maxRevs);
		}
		PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();
		pxpos = t;
		PKT_C2C_AutoUpdate_s n3;
		n3.pxtrans = t;
		n3.rpm = maxRevs;
		p2pSendToHost(this, &n3, sizeof(n3));

		CNetCellMover::moveData_s md;
		md.pos       = r3dPoint3D(t.p.x, t.p.y, t.p.z) /*GetPosition()*/;
		md.turnAngle = GetRotationVector().x;
		md.bendAngle = 0;
		md.state     = 0;

		PKT_C2C_MoveSetCell_s n1;
		PKT_C2C_MoveRel_s     n2;
		DWORD pktFlags = netMover.SendPosUpdate(md, &n1, &n2);
		if(pktFlags & 0x1)
		{
			p2pSendToHost(this, &n1, sizeof(n1), true);
		}
		if(pktFlags & 0x2)
		{

			p2pSendToHost(this, &n2, sizeof(n2), true);
		}
	}else
	{
		PxTransform t = pxpos;
		SetPosition(r3dVector(t.p.x, t.p.y, t.p.z));
		r3dVector angles;
		QuaternionToEulerAngles(t.q, angles.x, angles.y, angles.z);
		//	To degrees
		angles = D3DXToDegree(angles);

		SetRotationVector(angles);
		SyncPhysicsPoseWithObjectPose();

		if(controlled)
		{
			if(!m_vehicleSound)
			{
				m_vehicleSound = SoundSys.Play(SoundSys.GetEventIDByPath("Sounds/vehicle/vehicle_sound"),GetPosition(), true);
			}else
			{
				SoundSys.SetSoundPos(m_vehicleSound, r3dPoint3D(t.p.x, t.p.y, t.p.z));
				SoundSys.SetParamValue(m_vehicleSound, "rpm", rpm);
			}
		}
	}
	if(!controlled && SoundSys.isPlaying(m_vehicleSound))
	{
		SoundSys.Stop(m_vehicleSound);
		m_vehicleSound = 0;
	}
	//UpdatePositionFromRemote();
	
	return TRUE;
}

BOOL obj_Vehicle::OnDestroy()
{
	parent::OnDestroy();
	m_vehicleSound = NULL;

	return TRUE;
}

BOOL obj_Vehicle::OnNetReceive(DWORD EventID, const void* packetData, int packetSize)
{
	//r3d_assert(!(ObjFlags & OBJFLAG_JustCreated)); // make sure that object was actually created before processing net commands

#undef DEFINE_GAMEOBJ_PACKET_HANDLER
#define DEFINE_GAMEOBJ_PACKET_HANDLER(xxx) \
	case xxx: { \
		const xxx##_s&n = *(xxx##_s*)packetData; \
		r3d_assert(packetSize == sizeof(n)); \
		OnNetPacket(n); \
		return TRUE; \
	}

	switch(EventID)
	{
		default: return FALSE;
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_MoveSetCell);
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_MoveRel);
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_AutoUpdate);
	}

	return FALSE;
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_AutoUpdate_s& n)
{
	pxpos = n.pxtrans;
	rpm = n.rpm;
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_MoveSetCell_s& n)
{
	netMover.SetCell(n);
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_MoveRel_s& n)
{
/*
	const CNetCellMover::moveData_s& md = netMover.DecodeMove(n);

	v_rotation				= md.turnAngle;
	PxTransform t = vd->vehicle->getRigidDynamicActor()->getGlobalPose();
	r3dPoint3D asd;
	asd.x = t.p.x;
	asd.y = t.p.y;
	asd.z = t.p.z;

	// calc velocity to reach position on time for next update
	netVelocity = netMover.GetVelocityToNetTarget(
		asd,
		GPP->AI_SPRINT_SPEED * 1.5f,
		1.0f);

	SetVelocity(netVelocity);*/
}
