#include "r3dpch.h"
#include "r3d.h"

#include "UI/UIimEdit.h"

#include "Editors/LevelEditor_Collections.h"

#include "GameObjects\gameobj.h"
#include "GameObjects\objmanag.h"
#include "ProceduralGrass.h"
#include "..\..\..\..\bin\Data\Shaders\DX9_P1\system\LibSM\shadow_config.h" // shader config file
#include "..\..\..\..\..\GameEngine\TrueNature\Terrain.h"
#include "../../../Editors/CollectionsManager.h"

IMPLEMENT_CLASS(obj_ProceduralGrass, "obj_ProceduralGrass", "Object");
AUTOREGISTER_CLASS(obj_ProceduralGrass);

obj_ProceduralGrass::obj_ProceduralGrass()
{
}

obj_ProceduralGrass::~obj_ProceduralGrass()
{
}