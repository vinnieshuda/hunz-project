#ifndef __PWAR_OBJECT_PROCEDURALGRASS_H
#define __PWAR_OBJECT_PROCEDURALGRASS_H

class obj_ProceduralGrass : public GameObject
{
	DECLARE_CLASS(obj_ProceduralGrass, GameObject)
public:
	obj_ProceduralGrass();
	virtual ~obj_ProceduralGrass();
};

#endif