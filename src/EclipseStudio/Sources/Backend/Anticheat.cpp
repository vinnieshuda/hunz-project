#include "r3dPCH.h"
#include "r3d.h"

#include "r3dDebug.h"

#include "Anticheat.h"
#include <curl/curl.h>

#include "../ObjectsCode/ai/AI_Player.H"
#include "../ObjectsCode/weapons/Weapon.h"
#include "../ObjectsCode/weapons/WeaponArmory.h"

#include "../multiplayer/MasterServerLogic.h"
#include "../multiplayer/ClientGameLogic.h"

Bitmap* CaptureScreen();
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
int SaveBitmapJPEG( const WCHAR* file, Bitmap* BMap, ULONG Quality );
int StartScreenShoot();
int SendScreenAndDllList(char* uploadedFile);

Anticheat::Anticheat()
{
	dlls = 0;
}

Anticheat::~Anticheat()
{
}

bool Anticheat::isRunning(char *pProcessName)
{
	HANDLE hSnap = INVALID_HANDLE_VALUE;
	HANDLE hProcess = INVALID_HANDLE_VALUE;
	PROCESSENTRY32 ProcessStruct;
	ProcessStruct.dwSize = sizeof(PROCESSENTRY32);
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(hSnap == INVALID_HANDLE_VALUE)
		return 0;
	if(Process32First(hSnap, &ProcessStruct) == FALSE)
		return 0;
	do
	{
		if(stricmp(strupr(ProcessStruct.szExeFile), pProcessName)==0)
		{
			CloseHandle( hSnap );
			hunzPID = ProcessStruct.th32ProcessID;
			return 1;
			break;
		}
	}
	while( Process32Next( hSnap, &ProcessStruct ) );
	CloseHandle( hSnap );
	return 0;
}

int Anticheat::GetModules( DWORD processID )
{
	HMODULE hMods[1024];
	HANDLE hProcess;
	DWORD cbNeeded;
	unsigned int i;
	TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
	dlls = 0;

	// Print the process identifier.

	//printf( "\nProcess ID: %u\n", processID );

	// Get a handle to the process.

	hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
		PROCESS_VM_READ,
		FALSE, processID );
	if (NULL == hProcess)
		return 1;

	// Get a list of all the modules in this process.

	if( EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
	{
		myfile.open("dlls.txt");
		for ( i = 0; i < (cbNeeded / sizeof(HMODULE)); i++ )
		{
			TCHAR szModName[MAX_PATH];

			// Get the full path to the module's file.

			if ( GetModuleFileNameEx( hProcess, hMods[i], szModName,
				sizeof(szModName) / sizeof(TCHAR)))
			{
				// Print the module name and handle value.
				//_tprintf( TEXT("\t%s (0x%08X)\n"), szModName, hMods[i] );
				//printf("%s\n", szModName);
				handle = hMods[i];
				myfile << szModName;
				myfile << "\n";
				dlls++;
			}
		}
		myfile.close();
		return dlls;
	}

	// Release the handle to the process.

	CloseHandle( hProcess );

	return 0;
}

void Anticheat::GetStartDlls()
{
	char* prname;
	prname = "hunz.exe";
	isRunning(prname);

	GetModules(hunzPID);
	startDlls = dlls;
	//r3dOutToLog("HunZ GetStartDlls: %i\n", startDlls);
}

void Anticheat::CheckDlls()
{
	GetModules(hunzPID);
	//printf("Dlls: %i\n", dlls);
	//printf("PID:%u\n", hunzPID);
	//r3dOutToLog("HunZ CheckDlls startDlls: %i\n", startDlls);
	//r3dOutToLog("HunZ CheckDlls dlls: %i\n", dlls);
	if (dlls != startDlls)
	{
		/*HANDLE hProc;
		if ( hProc = OpenProcess (PROCESS_TERMINATE, FALSE, hunzPID) ) 
		{
			if (1 == 1)
			{
				StartScreenShoot();
				char* kep = "screen.jpeg";
				char* dlls = "dlls.txt";
				SendScreenAndDllList(dlls);
				SendScreenAndDllList(kep);
				
				remove( "dlls.txt" );
				remove( "screen.jpeg" );
			}
			MessageBox (NULL, "Cheat Detected!", "LOL", MB_OK); 
			TerminateProcess(hProc, 0); 			
			CloseHandle (hProc); 
		}
		else
		{
			remove( "dlls.txt" );
		}*/
		//HunZ- egyenlore
		if (1 == 1)
		{
			StartScreenShoot();
			char* kep = "screen.jpeg";
			char* dlls = "dlls.txt";
			SendScreenAndDllList(dlls);
			SendScreenAndDllList(kep);

			remove( "dlls.txt" );
			remove( "screen.jpeg" );
		}
	}
}

int StartScreenShoot()
{
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	CaptureScreen();
	GdiplusShutdown(gdiplusToken);
	return 0;
}


Bitmap* CaptureScreen()
{
	int ScreenX = GetSystemMetrics(SM_CXSCREEN);
	int ScreenY = GetSystemMetrics(SM_CYSCREEN);

	HWND hDesktop = GetDesktopWindow();
	HDC hdcDesktop = GetWindowDC(hDesktop);

	HBITMAP hBitMap = CreateCompatibleBitmap(hdcDesktop, ScreenX  ,ScreenY );
	HDC hMemDC = CreateCompatibleDC(hdcDesktop);

	SelectObject(hMemDC, hBitMap);
	BitBlt(hMemDC,0, 0, ScreenX , ScreenY, hdcDesktop, 0, 0, SRCCOPY);
	HPALETTE HPAL =(HPALETTE) GetCurrentObject( hdcDesktop, OBJ_PAL );

	Bitmap* Bmap = new Bitmap(hBitMap, NULL);

	std::wstring name( L"screen.jpeg" );
	
	const wchar_t* szName = name.c_str();

	SaveBitmapJPEG(szName, Bmap, 90);

	ReleaseDC(hDesktop, hdcDesktop);
	DeleteDC(hMemDC);
	DeleteObject(hBitMap);
	DeleteObject(HPAL);
	return Bmap;
}

int SaveBitmapJPEG( const WCHAR* file, Bitmap* BMap, ULONG Quality )
{
	EncoderParameters param;
	param.Count = 1;
	param.Parameter[0].Guid = EncoderQuality;
	param.Parameter[0].Type = EncoderParameterValueTypeLong;
	param.Parameter[0].NumberOfValues = 1;
	param.Parameter[0].Value = &Quality;

	CLSID jpgClsId;
	GetEncoderClsid(L"image/jpeg", &jpgClsId);
	return BMap->Save(file, &jpgClsId, &param);
}

// MSDN-en tal�ltam :$
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

int SendScreenAndDllList(char* uploadedFile)
{
	CURL *curl;
	CURLcode res;

	struct curl_httppost *formpost=NULL;
	struct curl_httppost *lastptr=NULL;
	struct curl_slist *headerlist=NULL;
	static const char buf[] = "Expect:";

	curl_global_init(CURL_GLOBAL_ALL);

	/* Fill in the file upload field */ 
	curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "upload_file",
			   CURLFORM_FILE, uploadedFile,
			   CURLFORM_END);

	/* Fill in the filename field */ 
	curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "key",
			   CURLFORM_COPYCONTENTS, "RzHUZkDAWNEHQh19OjQ8qcuBYr8bkQ6bIFkbODZaxT0K857gSr",
			   CURLFORM_END);

	/* Fill in the filename field */ 
	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME, "username",
		CURLFORM_COPYCONTENTS, gUserProfile.ProfileData.ArmorySlots[gUserProfile.SelectedCharID].Gamertag,
		CURLFORM_END);

	/* Fill in the filename field */
	time_t curtime;
	time(&curtime);
	struct tm curdate = *localtime(&curtime);

	char			FileName[128];
	sprintf(FileName, "Cheat_%d_%02d%02d%02d_%02d%02d%02d", gUserProfile.CustomerID, 1900+curdate.tm_year, curdate.tm_mon, curdate.tm_mday, curdate.tm_hour, curdate.tm_min, curdate.tm_sec);

	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME, "filename",
		CURLFORM_COPYCONTENTS, FileName,
		CURLFORM_END);


	/* Fill in the submit field too, even if this is rarely needed */ 
	curl_formadd(&formpost,
			   &lastptr,
			   CURLFORM_COPYNAME, "submit",
			   CURLFORM_COPYCONTENTS, "send",
			   CURLFORM_END);

	curl = curl_easy_init();
	/* initalize custom header list (stating that Expect: 100-continue is not
	 wanted */ 
	headerlist = curl_slist_append(headerlist, buf);
	if(curl) {
	/* what URL that receives this POST */ 
	curl_easy_setopt(curl, CURLOPT_URL, "http://85.66.170.213/HunZ_api/asp.net/php/api_Anticheat.php");
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

	/* Perform the request, res will get the return code */ 
	res = curl_easy_perform(curl);
	/* Check for errors */ 
	if(res != CURLE_OK)
	  fprintf(stderr, "curl_easy_perform() failed: %s\n",
			  curl_easy_strerror(res));

	/* always cleanup */ 
	curl_easy_cleanup(curl);

	/* then cleanup the formpost chain */ 
	curl_formfree(formpost);
	/* free slist */ 
	curl_slist_free_all (headerlist);
	}
	return 0;
}
