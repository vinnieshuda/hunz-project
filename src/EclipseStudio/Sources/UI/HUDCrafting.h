#pragma once

#include "APIScaleformGfx.h"
#include "../GameCode/UserProfile.h"
#include "UIItemInventory.h"
#include "UIMarket.h"

class HUDCrafting
{
public:
	HUDCrafting();
	~HUDCrafting();

	bool Init();
	bool Unload();

	bool IsInited() const { return isInit_; }

	void Update();
	void Draw();

	bool isActive() const { return isActive_; }
	void Activate();
	void Deactivate();

	void eventReturnToGame(r3dScaleformMovie* pMovie, const Scaleform::GFx::Value* args, unsigned argCount);
	void eventCraftItem(r3dScaleformMovie* pMovie, const Scaleform::GFx::Value* args, unsigned argCount);

	//void addTabType(int tabID, const char* name, bool inStore, bool inInventory);
	//void addCategory(int catID, const char* name, int tabID, int slotDB); // HunZ - WTF???? a slotDB es az arg5???? WTF???? TITOV!!!!!
	//void addItem(int itemID, int catID, const char* name, const char* desc, const char* icon);

private:
	r3dScaleformMovie gfxMovie;
	r3dScaleformMovie* prevKeyboardCaptureMovie_;

	bool isActive_;
	bool isInit_;

	/*void addCategories();
	void addTabTypes();
	void addItems();*/

	void addClientSurvivor();
	void addBackpackItem();
	void updateSurvivorTotalWeight();
	void loadMaterialsFromRecipe(int recipeID);
	bool searchMaterialsFromBagpack(int itemID);

	void addRecipe();
	void addRecipeComponent();

	UIItemInventory itemInventory_;
};