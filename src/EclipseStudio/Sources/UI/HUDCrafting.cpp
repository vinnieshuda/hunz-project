#include "r3dPCH.h"
#include "r3d.h"

#include "HUDCrafting.h"

#include "FrontendShared.h"

#include "../ObjectsCode/ai/AI_Player.H"
#include "../ObjectsCode/weapons/Weapon.h"
#include "../ObjectsCode/weapons/WeaponArmory.h"

#include "../multiplayer/MasterServerLogic.h"
#include "../multiplayer/ClientGameLogic.h"

#include "HUDDisplay.h"

extern const wchar_t* getReputationString(int Reputation);

extern HUDDisplay* hudMain;

HUDCrafting::HUDCrafting() :
isActive_(false),
isInit_(false),
prevKeyboardCaptureMovie_(NULL)
{
}

HUDCrafting::~HUDCrafting()
{
}

void HUDCrafting::addClientSurvivor()
{
	Scaleform::GFx::Value var[22];

	obj_Player* plr = gClientLogic().localPlayer_;
	r3d_assert(plr);
	wiCharDataFull& slot = plr->CurLoadout;
	char tmpGamertag[128];
	if(plr->ClanID != 0)
		sprintf(tmpGamertag, "[%s] %s", plr->ClanTag, slot.Gamertag);
	else
		r3dscpy(tmpGamertag, slot.Gamertag);
	var[0].SetString(tmpGamertag);
	var[1].SetNumber(slot.Health);
	var[2].SetNumber(slot.Stats.XP);
	var[3].SetNumber(slot.Stats.TimePlayed);
	var[4].SetNumber(slot.Hardcore);
	var[5].SetNumber(slot.HeroItemID);
	var[6].SetNumber(slot.HeadIdx);
	var[7].SetNumber(slot.BodyIdx);
	var[8].SetNumber(slot.LegsIdx);
	var[9].SetNumber(slot.Alive);
	var[10].SetNumber(slot.Hunger);
	var[11].SetNumber(slot.Thirst);
	var[12].SetNumber(slot.Toxic);
	var[13].SetNumber(slot.BackpackID);
	var[14].SetNumber(slot.BackpackSize);

	var[15].SetNumber(0);		// weight
	var[16].SetNumber(0);		// zombies Killed
	var[17].SetNumber(0);		// bandits killed
	var[18].SetNumber(0);		// civilians killed
	var[19].SetString("");	// alignment
	var[20].SetString("");	// last Map
	var[21].SetBoolean(false); // globalInventory. For now false. Separate UI for it

	gfxMovie.Invoke("_root.api.addClientSurvivor", var, 22);

	gfxMovie.Invoke("_root.api.clearBackpack", NULL, 0);

	addBackpackItem();
}
bool HUDCrafting::searchMaterialsFromBagpack(int itemID)
{
/*
	std::vector<uint32_t> uniqueBackpacks; // to filter identical backpack
	int backpackSlotIDInc = 0;*/

	Scaleform::GFx::Value var[7];
	obj_Player* plr = gClientLogic().localPlayer_;
	wiCharDataFull& slot = plr->CurLoadout;
	for (int i = 0; i < slot.BackpackSize; i++)
	{
		if(slot.Items[i].itemID == itemID)
		{
			var[0].SetInt(i);
			var[1].SetUInt(0); // not used for game
			var[2].SetUInt(slot.Items[i].itemID);
			var[3].SetInt(slot.Items[i].quantity);
			var[4].SetInt(slot.Items[i].Var1);
			var[5].SetInt(slot.Items[i].Var2);
			char tmpStr[128] = {0};
			getAdditionalDescForItem(slot.Items[i].itemID, slot.Items[i].Var1, slot.Items[i].Var2, tmpStr);
			var[6].SetString(tmpStr);
			gfxMovie.Invoke("_root.api.addBackpackItem", var, 7);
		}
	}
	return false;
}

void HUDCrafting::loadMaterialsFromRecipe(int recipeID)
{
	Scaleform::GFx::Value var[3];
	const CraftRecipeConfig* craftRecipe = g_pWeaponArmory->getCraftRecipeConfig(recipeID);
	for(int i=0; i<craftRecipe->NeededItemNumber; i++)
	{
		var[0].SetUInt(craftRecipe->m_itemID);
		var[1].SetUInt(craftRecipe->itemid[i]);
		var[2].SetUInt(1);
		gfxMovie.Invoke("_root.api.addRecipeComponent", var, 4);
	}
}


void HUDCrafting::addBackpackItem()
{
	// reset backpack
/*
	{
		gfxMovie.Invoke("_root.api.clearBackpack", "");
		gfxMovie.Invoke("_root.api.clearBackpacks", "");
	}*/
/*

	std::vector<uint32_t> uniqueBackpacks; // to filter identical backpack
	int backpackSlotIDInc = 0;*/
	// add backpack content info
	{
		obj_Player* plr = gClientLogic().localPlayer_;
		r3d_assert(plr);
		wiCharDataFull& slot = plr->CurLoadout;

		Scaleform::GFx::Value var[4];
/*		gfxMovie.Invoke("_root.api.clearRecipes","");*/

		// HunZ - crafting fos szepen, rendezetten
		for(int i=0; i<slot.BackpackSize; i++)
		{
			if (slot.Items[i].itemID != 0)
			{
				const BaseItemConfig* itm = g_pWeaponArmory->getConfig(slot.Items[i].itemID);
				if (itm->category == storecat_CraftRecipe)
				{
					const CraftRecipeConfig* craftRecipe = g_pWeaponArmory->getCraftRecipeConfig(itm->m_itemID);
					var[0].SetUInt(craftRecipe->m_itemID);
					var[1].SetStringW(craftRecipe->m_StoreNameW);
					var[2].SetStringW(craftRecipe->m_DescriptionW);
					var[3].SetString(craftRecipe->m_StoreIcon);
					gfxMovie.Invoke("_root.api.addRecipe", var, 4);
					loadMaterialsFromRecipe(craftRecipe->m_itemID);
					for(int z=0;z<craftRecipe->NeededItemNumber;z++)
					{
						searchMaterialsFromBagpack(craftRecipe->itemid[z]);
					}
				}
			}
		}
/*
		for (int a = 0; a < slot.BackpackSize; a++)
		{
			const BackpackConfig* bpc = g_pWeaponArmory->getBackpackConfig(slot.Items[i].itemID);
			if(bpc)
			{
				if(std::find<std::vector<uint32_t>::iterator, uint32_t>(uniqueBackpacks.begin(), uniqueBackpacks.end(), slot.Items[i].itemID) != uniqueBackpacks.end())
				continue;

				// add backpack info
				var[0].SetInt(backpackSlotIDInc++);
				var[1].SetUInt(slot.Items[i].itemID);
				gfxMovie.Invoke("_root.api.addBackpack", var, 2);

				uniqueBackpacks.push_back(slot.Items[i].itemID);
			}
		}*/
	/*	for (int a = 0; a < slot.BackpackSize; a++)
		{
			if (slot.Items[a].itemID != 0)
			{
				const BaseItemConfig* itm = g_pWeaponArmory->getConfig(slot.Items[a].itemID);
				if (itm->category == storecat_CraftRecipe)
				{
					r3dOutToLog("HunZ - CraftRecipeConfig\n");
					const CraftRecipeConfig* craftRecipe = g_pWeaponArmory->getCraftRecipeConfig(itm->m_itemID);
					var[0].SetUInt(craftRecipe->m_itemID);
					var[1].SetStringW(craftRecipe->m_StoreNameW);
					var[2].SetStringW(craftRecipe->m_DescriptionW);
					var[3].SetString(craftRecipe->m_StoreIcon);
					gfxMovie.Invoke("_root.api.addRecipe", var, 4);
					for(int y=0; y<craftRecipe->NeededItemNumber; y++)
					{
						if(checkCraftingMaterialbyID(craftRecipe->itemid[y]))
						{
							var[0].SetUInt(craftRecipe->m_itemID);
							var[1].SetUInt(craftRecipe->itemid[y]);
							var[2].SetUInt(1);
							gfxMovie.Invoke("_root.api.addRecipeComponent", var, 4);
						}
					}
/ *
					for (int y = 0; y < slot.BackpackSize; y++)
					{
						if (slot.Items[y].itemID != 0)
						{
							if (slot.Items[y].itemID == craftRecipe->itemid[y])
							{
								var[0].SetUInt(craftRecipe->m_itemID);
								var[1].SetUInt(craftRecipe->itemid[y]);
								var[2].SetUInt(1);
								gfxMovie.Invoke("_root.api.addRecipeComponent", var, 4);
							}
						}
					}* /
				}
				/ *if (itm->category == storecat_CraftComponent)
				{
					r3dOutToLog("HunZ - CraftComponentsConfig\n");
					const CraftComponentsConfig* craftComp = g_pWeaponArmory->getCraftComponentsConfig(itm->m_itemID);
					var[0].SetUInt(craftComp->RecipeID);
					var[1].SetUInt(craftComp->m_itemID);
					var[2].SetUInt(1);
					gfxMovie.Invoke("_root.api.addRecipeComponent", var, 4);
				}* /
				var[0].SetInt(a);
				var[1].SetUInt(0); // not used for game
				var[2].SetUInt(slot.Items[a].itemID);
				var[3].SetInt(slot.Items[a].quantity);
				var[4].SetInt(slot.Items[a].Var1);
				var[5].SetInt(slot.Items[a].Var2);
				char tmpStr[128] = {0};
				getAdditionalDescForItem(slot.Items[a].itemID, slot.Items[a].Var1, slot.Items[a].Var2, tmpStr);
				var[6].SetString(tmpStr);
				gfxMovie.Invoke("_root.api.addBackpackItem", var, 7);

				const BackpackConfig* bpc = g_pWeaponArmory->getBackpackConfig(slot.Items[a].itemID);
				if(bpc)
				{
					if(std::find<std::vector<uint32_t>::iterator, uint32_t>(uniqueBackpacks.begin(), uniqueBackpacks.end(), slot.Items[a].itemID) != uniqueBackpacks.end())
						continue;

					// add backpack info
					var[0].SetInt(backpackSlotIDInc++);
					var[1].SetUInt(slot.Items[a].itemID);
					gfxMovie.Invoke("_root.api.addBackpack", var, 2);

					uniqueBackpacks.push_back(slot.Items[a].itemID);
				}
			}
		}*/
	}
}

void HUDCrafting::updateSurvivorTotalWeight()
{
	obj_Player* plr = gClientLogic().localPlayer_;
	r3d_assert(plr);

	wiCharDataFull& slot = plr->CurLoadout;
	char tmpGamertag[128];
	if(plr->ClanID != 0)
		sprintf(tmpGamertag, "[%s] %s", plr->ClanTag, slot.Gamertag);
	else
		r3dscpy(tmpGamertag, slot.Gamertag);

	float totalWeight = plr->CurLoadout.getTotalWeight();

	Scaleform::GFx::Value var[2];
	var[0].SetString(tmpGamertag);
	var[1].SetNumber(totalWeight);
	gfxMovie.Invoke("_root.api.updateClientSurvivorWeight", var, 2);
}

void HUDCrafting::eventReturnToGame(r3dScaleformMovie* pMovie, const Scaleform::GFx::Value* args, unsigned argCount)
{
	Deactivate();
}

void HUDCrafting::eventCraftItem(r3dScaleformMovie* pMovie, const Scaleform::GFx::Value* args, unsigned argCount)
{
	int recipeID = args[0].GetInt();
	if(gClientLogic().localPlayer_->CraftItem(recipeID))
	{
		gfxMovie.Invoke("_root.api.hideInfoMsg", NULL, 0);
		hudMain->showMessage(L"Crafting successful!");
	}
	else hudMain->showMessage(L"Crafting failed!");
	Deactivate();
}

bool HUDCrafting::Init()
{
 	if(!gfxMovie.Load("Data\\Menu\\WarZ_HUD_CraftingMenu.swf", false)) 
	{
 		return false;
	}

#define MAKE_CALLBACK(FUNC) new r3dScaleformMovie::TGFxEICallback<HUDCrafting>(this, &HUDCrafting::FUNC)
	gfxMovie.RegisterEventHandler("eventCraftItem", MAKE_CALLBACK(eventCraftItem));
	gfxMovie.RegisterEventHandler("eventReturnToGame", MAKE_CALLBACK(eventReturnToGame));

	gfxMovie.SetCurentRTViewport(Scaleform::GFx::Movie::SM_ExactFit);

	/*addTabTypes();
	addItems();
	addCategories();*/
	itemInventory_.initialize(&gfxMovie);

	isActive_ = false;
	isInit_ = true;
	return true;
}

bool HUDCrafting::Unload()
{
 	gfxMovie.Unload();
	isActive_ = false;
	isInit_ = false;
	return true;
}

void HUDCrafting::Update()
{

}

void HUDCrafting::Draw()
{
 	gfxMovie.UpdateAndDraw();
}

void HUDCrafting::Deactivate()
{
	Scaleform::GFx::Value var[1];
	var[0].SetString("menu_close");
	gfxMovie.OnCommandCallback("eventSoundPlay", var, 1);

	if(prevKeyboardCaptureMovie_)
	{
		prevKeyboardCaptureMovie_->SetKeyboardCapture();
		prevKeyboardCaptureMovie_ = NULL;
	}

	if(!g_cursor_mode->GetInt())
	{
		r3dMouse::Hide();
	}

	isActive_ = false;
}

void HUDCrafting::Activate()
{
	prevKeyboardCaptureMovie_ = gfxMovie.SetKeyboardCapture(); // for mouse scroll events
	gfxMovie.Invoke("_root.api.clearRecipes","");
	r3d_assert(!isActive_);
	r3dMouse::Show();
	isActive_ = true;

	addClientSurvivor();
	updateSurvivorTotalWeight();

	gfxMovie.Invoke("_root.api.showCraftScreen", 0);

	Scaleform::GFx::Value var[1];
	var[0].SetString("menu_open");
	gfxMovie.OnCommandCallback("eventSoundPlay", var, 1);
}