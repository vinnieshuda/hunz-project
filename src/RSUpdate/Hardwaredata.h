#ifndef HARDWAREDATA_H
#define HARDWAREDATA_H

//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#pragma comment(lib, "iphlpapi.lib")
#include "MACAddress.h"


class Hardwaredata{
public:
	static std::string getFormattedMacAddress();
	static std::string getHashedString(std::string*);
	static MACAddress* getMACaddress();
	static size_t read_callback(void *ptr, size_t size, size_t nmemb, void* userp);
	static string checkBanState(std::string);
	static bool fexists(const char *filename);
	static std::string BSTRToString(BSTR bs);
	static std::string getData(LPCWSTR query, LPCWSTR field, bool initSec);
	static std::string getGIDfromPNP(std::string);

	struct WriteThis {
		const char *readptr;
		long sizeleft;
	};
}; 
#endif //HARDWAREDATA_H