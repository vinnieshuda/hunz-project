#include "r3dPCH.h"
#include "r3d.h"

bool	UPDATER_UPDATER_ENABLED  = 1;
char	UPDATER_VERSION[512]     = "HunZ.v0.6.5.Beta";
char	UPDATER_VERSION_SUFFIX[512] = "";
char	UPDATER_BUILD[512]	 = __DATE__ " " __TIME__;

char	BASE_RESOURSE_NAME[512]  = "HunZ";
char	GAME_EXE_NAME[512]       = "HunZ.exe";
char	GAME_TITLE[512]          = "HunZ Beta";

// updater (xml and exe) and game info on our server.
char	UPDATE_DATA_URL[512]     = "http://your ip/wz.xml";	// url for data update
char	UPDATE_UPDATER_URL[512]  = "http://your ip/launcher_ver/woupd.xml";

// HIGHWIND CDN
char	UPDATE_UPDATER_HOST[512] = "http://your ip/launcher/";

// LOCAL TESTING
//http://arktos.pandonetworks.com/Arktos
//char	UPDATE_DATA_URL[512]     = "http://localhost/wo/wo.xml";	// url for data update
//char	UPDATE_UPDATER_HOST[512] = "http://localhost/wo/updater/";	// url for updater .xml

char	EULA_URL[512]            = "http://your ip/GYIK.rtf";
char	TOS_URL[512]             = "http://your ip/GYIK.rtf";
char	GETSERVERINFO_URL[512]   = "http://your ip/api_getserverinfo.xml";



bool	UPDATER_STEAM_ENABLED	 = false;
