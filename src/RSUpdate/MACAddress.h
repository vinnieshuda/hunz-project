#ifndef MACADDRESS_H
#define MACADDRESS_H

#include <string>
#include <stdio.h>

using namespace std;

class MACAddress{
public:
	MACAddress(unsigned char raw[])
	{
		char buffer[50];
		sprintf( buffer, "%02X-%02X-%02X-%02X-%02X-%02X", 
			raw[0], raw[1], raw[2], raw[3], raw[4], raw[5]);
		mac = new string(buffer);
	}

	std::string* getMac()
	{
		return mac;
	}

	~MACAddress()
	{
		delete mac;
	}
private:
	std::string* mac;
};



#endif //MACADDRESS_H