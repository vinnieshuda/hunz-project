#include "r3dPCH.h"
#include "r3d.h"

#include "WOCreateAccHelper.h"
#include "WOBackendAPI.h"

int CCreateAccHelper::DoCreateAcc()
{
	r3d_assert(createAccCode == CA_Unactive);
	r3d_assert(*username);
	r3d_assert(*passwd1);
	r3d_assert(*passwd2);
	
	
	//int		CheckCode;
	const char*	CheckMsg;
	
	CWOBackendReq req1("api_AccPreorder.aspx");
	req1.AddParam("email", username);
	createAccCode = CA_Processing;
	req1.Issue();
	createAccCode = CCreateAccHelper::CA_Unactive;
	CheckMsg = req1.bodyStr_;
	//sscanf(req1.bodyStr_, "%d", CheckMsg);

	//MessageBox(NULL, CheckMsg, "some title", 0);

	CWOBackendReq req("api_AccRegister.aspx");
	req.AddParam("username", username);
	req.AddParam("password", passwd1);
	req.AddParam("serial",   req1.bodyStr_);
	req.AddParam("email",    username); // previously that was order email.

	createAccCode = CA_Processing;
	req.Issue();
        createAccCode = CCreateAccHelper::CA_Unactive;

	return req.resultCode_;
}
