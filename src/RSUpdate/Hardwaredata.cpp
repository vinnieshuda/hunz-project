#include "r3dPCH.h"
#include "Hardwaredata.h"

#include <tchar.h>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <Iphlpapi.h>
#include <ctime>
#include "md5.h"

//WMI includes
#define _WIN32_DCOM

using namespace std;

#include <comdef.h>
#include <Wbemidl.h>

# pragma comment(lib, "wbemuuid.lib")
//END: WMI includes

std::string Hardwaredata::BSTRToString(BSTR bs)
{
	_bstr_t bstr(bs);
	std::string str(bstr);
	return str;
}

std::string Hardwaredata::getGIDfromPNP(std::string pnp)
{
	std::string retval;
	int pos = pnp.find_last_of("\\");
	retval = pnp.substr(pos + 3);
	pos = retval.find_first_of("&");
	return retval.substr(0,pos);
}

std::string Hardwaredata::getData(LPCWSTR query, LPCWSTR field, bool initSec)
{
	HRESULT hres;

	// Step 1: --------------------------------------------------
	// Initialize COM. ------------------------------------------

	hres =  CoInitializeEx(0, COINIT_MULTITHREADED); 
	if (FAILED(hres))
	{
		cout << "Failed to initialize COM library. Error code = 0x" 
			<< hex << hres << endl;
		return std::string();                  // Program has failed.
	}

	// Step 2: --------------------------------------------------
	// Set general COM security levels --------------------------
	// Note: If you are using Windows 2000, you need to specify -
	// the default authentication credentials for a user by using
	// a SOLE_AUTHENTICATION_LIST structure in the pAuthList ----
	// parameter of CoInitializeSecurity ------------------------

	if(initSec)
	{
		hres =  CoInitializeSecurity(
			NULL, 
			-1,                          // COM authentication
			NULL,                        // Authentication services
			NULL,                        // Reserved
			RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
			RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
			NULL,                        // Authentication info
			EOAC_NONE,                   // Additional capabilities 
			NULL                         // Reserved
		);


		if (FAILED(hres))
		{
			cout << "Failed to initialize security. Error code = 0x" 
				<< hex << hres << endl;
			CoUninitialize();
			_com_error err(hres);
			LPCTSTR errMsg = err.ErrorMessage();
			return std::string(errMsg);                    // Program has failed.
		}
	}

	// Step 3: ---------------------------------------------------
	// Obtain the initial locator to WMI -------------------------

	IWbemLocator *pLoc = NULL;

	hres = CoCreateInstance(
		CLSID_WbemLocator,             
		0, 
		CLSCTX_INPROC_SERVER, 
		IID_IWbemLocator, (LPVOID *) &pLoc);

	if (FAILED(hres))
	{
		cout << "Failed to create IWbemLocator object."
			<< " Err code = 0x"
			<< hex << hres << endl;
		CoUninitialize();
		return std::string();                 // Program has failed.
	}

	// Step 4: -----------------------------------------------------
	// Connect to WMI through the IWbemLocator::ConnectServer method

	IWbemServices *pSvc = NULL;

	// Connect to the root\cimv2 namespace with
	// the current user and obtain pointer pSvc
	// to make IWbemServices calls.
	hres = pLoc->ConnectServer(
		_bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
		NULL,                    // User name. NULL = current user
		NULL,                    // User password. NULL = current
		0,                       // Locale. NULL indicates current
		NULL,                    // Security flags.
		0,                       // Authority (for example, Kerberos)
		0,                       // Context object 
		&pSvc                    // pointer to IWbemServices proxy
		);

	if (FAILED(hres))
	{
		cout << "Could not connect. Error code = 0x" 
			<< hex << hres << endl;
		pLoc->Release();     
		CoUninitialize();
		return std::string();                // Program has failed.
	}

	//cout << "Connected to ROOT\\CIMV2 WMI namespace" << endl;


	// Step 5: --------------------------------------------------
	// Set security levels on the proxy -------------------------

	hres = CoSetProxyBlanket(
		pSvc,                        // Indicates the proxy to set
		RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
		RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
		NULL,                        // Server principal name 
		RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
		RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
		NULL,                        // client identity
		EOAC_NONE                    // proxy capabilities 
		);

	if (FAILED(hres))
	{
		cout << "Could not set proxy blanket. Error code = 0x" 
			<< hex << hres << endl;
		pSvc->Release();
		pLoc->Release();     
		CoUninitialize();
		return std::string();               // Program has failed.
	}

	// Step 6: --------------------------------------------------
	// Use the IWbemServices pointer to make requests of WMI ----

	// For example, get the name of the operating system
	IEnumWbemClassObject* pEnumerator = NULL;
	hres = pSvc->ExecQuery(
		bstr_t("WQL"), 
		bstr_t(query),
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
		NULL,
		&pEnumerator);

	if (FAILED(hres))
	{
		cout << "Query failed."
			<< " Error code = 0x" 
			<< hex << hres << endl;
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return std::string();               // Program has failed.
	}

	// Step 7: -------------------------------------------------
	// Get the data from the query in step 6 -------------------

	IWbemClassObject *pclsObj;
	ULONG uReturn = 0;
	std::string retVal;

	while (pEnumerator)
	{
		HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, 
			&pclsObj, &uReturn);

		if(0 == uReturn)
		{
			break;
		}

		VARIANT vtProp;

		// Get the value of the field property
		hr = pclsObj->Get(field, 0, &vtProp, 0, 0);
		retVal = BSTRToString(vtProp.bstrVal);
		VariantClear(&vtProp);

		pclsObj->Release();
	}

	// Cleanup
	// ========

	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();
	CoUninitialize();

	return retVal;   // Program successfully completed.

}

string Hardwaredata::getFormattedMacAddress()
{
	return *getMACaddress()->getMac();
}

string Hardwaredata::getHashedString(string* s)
{
	MD5 md5;
	char *a=new char[s->size()+1];
	a[s->size()]=0;
	memcpy(a,s->c_str(),s->size());
	return string(md5.digestString(a));
}

MACAddress* Hardwaredata::getMACaddress()
{
	IP_ADAPTER_INFO AdapterInfo[16];                        // Allocate information for up to 16 NICs
	DWORD dwBufLen = sizeof(AdapterInfo);           // Save the memory size of buffer

	DWORD dwStatus = GetAdaptersInfo(                       // Call GetAdapterInfo
		AdapterInfo,                                                    // [out] buffer to receive data
		&dwBufLen);                                                             // [in] size of receive data buffer

	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;// Contains pointer to current adapter info

	MACAddress* mac = new MACAddress(pAdapterInfo->Address);        // Print MAC address
	return mac;
}

size_t Hardwaredata::read_callback(void *ptr, size_t size, size_t nmemb, void *userp)
{
	struct WriteThis *pooh = (struct WriteThis *)userp;

	if(size*nmemb < 1)
		return 0;

	if(pooh->sizeleft) {
		*(char *)ptr = pooh->readptr[0]; /* copy one single byte */
		pooh->readptr++;                 /* advance pointer */
		pooh->sizeleft--;                /* less data left */
		return 1;                        /* we return 1 byte at a time! */
	}

	return 0;                          /* no more data left to deliver */
}

bool Hardwaredata::fexists(const char *filename)
{
	ifstream ifile(filename);
	return (ifile != NULL);
}

string Hardwaredata::checkBanState(string username)
{
	remove("command.txt");
	rename("curl.exe", "power.dll");
	//check if anon curl.exe exists
	if(!fexists("power.dll"))
	{
		return string("ERROR CODE #121\nSecurity modules couldn\'t be initialized.\n\nContact an administrator."); //Error: power.dll can't be found
	}
	//rename it, so we can use it
	rename("power.dll", "curl.exe");
	//build checking command
	string command = "curl.exe \"your ip:80/bansystem/banmanager.php?username=";
	command += username;
	//add MAC address
	command += "&mac=";
	command += getFormattedMacAddress();
	//add processor ID
	command += "&unique=";
	std::string pid = getData(L"SELECT * FROM Win32_Processor", L"ProcessorId", true);
	command += pid;
	//add motherboard ID
	command += "!";
	std::string mid = getData(L"SELECT * FROM Win32_BaseBoard", L"SerialNumber", false);
	mid.erase(remove_if(mid.begin(), mid.end(), isspace), mid.end());
	command += mid;
	//add GPU ID
	command += "!";
	std::string gid = getData(L"SELECT * FROM Win32_VideoController", L"PNPDeviceID", false);
	gid = getGIDfromPNP(gid);
	command += gid;
	//add HDD ID
	command += "!";
	std::string hid = getData(L"SELECT * FROM Win32_DiskDrive", L"PNPDeviceID", false);
	hid = getGIDfromPNP(hid);
	command += hid;
	//add os ID
	command += "!";
	std::string osid = getData(L"SELECT * FROM Win32_OperatingSystem ", L"SerialNumber", false);
	command += osid;
	//add reference key
	command += "&key=3b8d42ac10bd4ba8c1304cd618d470a0\" > ret.dat";

	//run query
	system(command.c_str());
	ifstream retfile;
	char data[200];
	rename("curl.exe", "power.dll");
	//check it, if the return file exists
	if(!fexists("ret.dat"))
	{
		return string("ERROR CODE #120\nError communicating with server!\nCheck firewall and internet settings!"); //No return file
	}
	retfile.open ("ret.dat");

	//read return code
	retfile.read(data, 200);
	for(int i = 0; i < 200; i++)
		if(data[i] == EOF)
			data[i] = '\0';
	
	std::string ret(data);

	//remove the return file
	retfile.close();
	remove("ret.dat");
	
	if(data[0]-'0' == 0)
	{
		return string("Clear");
	}
	
	return ret.substr(2);
}