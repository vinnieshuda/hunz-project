#include "r3dPCH.h"
#include "r3d.h"
#include "shellapi.h"

#include "SkillSystem.h"

float baseXP = 200;
float XPFactor1 = 1.2f; //HunZ - alap gorbe
float XPFactor2 = 2.8f; //HunZ - gorbe veget emeli

SkillSystem::SkillSystem()
{

}

SkillSystem::~SkillSystem()
{

}

wiSkillData SkillSystem::GetSkillById(obj_ServerPlayer* plr, int skillid)
{
	SelectedSkill.Level = 0;
	SelectedSkill.SkillID = 0;
	for (int i = 0; i < SKILL_ID_END; i++)
	{
		if (skillid == plr->loadout_->Skills[i].SkillID)
		{
			SelectedSkill.SkillID = plr->loadout_->Skills[i].SkillID;
			SelectedSkill.Level = plr->loadout_->Skills[i].Level;
			r3dOutToLog("HunZ - player: %s used skill: %d at level: %d\n",plr->userName, plr->loadout_->Skills[i].SkillID, plr->loadout_->Skills[i].Level);
			return SelectedSkill;
		}
	}
		
	return SelectedSkill;
}

float SkillSystem::GetSkillEffectBySkill(const wiSkillData pSkill)
{
	float ret;
	switch(pSkill.SkillID)
	{
		default:
			{
				ret = 0;
				return ret;
			}
		case SKILL_PLUS_HP: //+ 1 hp
			{
				ret = pSkill.Level / 100.0f;
				return ret;
			}

		case SKILL_PLUS_DMG: // +1 dmg
			{
				ret = pSkill.Level / 100.0f;
				return ret;
			}

		case SKILL_PLUS_HEALING: // +1 healing effect
			{
				ret = (float)pSkill.Level;
				return ret;
			}

		case SKILL_MINUS_POISON: // -1 poison chance
			{
				ret = (float)pSkill.Level;
				return ret;
			}

		case SKILL_MINUS_BLEEDING: //-2 bleeding chance
			{
				ret =  (2 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_PLUS_LOOTCHANCE: // +5 loot chance
			{
				ret = (5 * pSkill.Level) / 100.0f;
				return ret;
			}
		case SKILL_PLUS_DEF: // +2 def
			{
				ret = (2 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_SURVIVOR: //+2 def, + 15 hp
			{
				break;
			}

		case SKILL_MINUS_BLEEDING_HIGH: // -6 bleeding chance
			{
				ret = 0.06f;
				return ret;
			}

		case SKILL_BERSERKING: // nem tudom meg
			{
				break;
			}

		case SKILL_PLUS_STAMINA: // +5 stamina
			{
				ret = (5 * pSkill.Level) / 100.0f;
				break;
			}

		case SKILL_WHO_NEED_FOOD: // nem tudom meg
			{
				break;
			}

		case SKILL_MINUS_MDMG: // -5 melee dmg
			{
				ret = (5 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_PLUS_HEALING_HIGH: // +4 healing 
			{
				ret = (4 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_PLUS_VACCINE: // +2 vaccine
			{
				ret = (2 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_PLUS_HEALING_BIGGER: // +2 healing
			{
				ret = (2 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_MINUS_NOISE: // -20 noise // HunZ - itt tartok
			{
				ret = 0.2f;
				return ret;
			}

		case SKILL_PLUS_MDMG: // +3 melee dmg
			{
				ret = (3 * pSkill.Level) / 100.0f;
				return ret;
			}

		case SKILL_CALL_ME_DOCTOR: // ???
			{
				break;
			}

		case SKILL_POISON_WHAT: // ???
			{
				break;
			}

		case SKILL_HARDER_HIT: // ???
			{
				break;
			}
	}
	return ret;
}

bool SkillSystem::SelectSkillbyID(obj_ServerPlayer* plr, int skillid)
{
	for (int i = 0; i < SKILL_ID_END; i++)
	{
		if (skillid == plr->loadout_->Skills[i].SkillID)
		{
			return true;
		}
	}

	return false;
}

void SkillSystem::LevelUpCheck(obj_ServerPlayer* plr)
{
	bool levelup = false;
	float nextLevelXp = 0;
	float nextLevel = (float)plr->loadout_->charLevel + 1;
	float currentLevel = (float)plr->loadout_->charLevel;
	
	nextLevelXp = ((pow(baseXP, XPFactor1)) * currentLevel) + (baseXP*(pow(currentLevel, XPFactor2)) + (nextLevel * XPFactor2));

	if(nextLevelXp >= (float)plr->loadout_->Stats.XP)
		levelup = true;

	if(levelup)
	{
		plr->loadout_->Stats.XP = plr->loadout_->Stats.XP - (int)nextLevelXp;
		plr->loadout_->charLevel += 1;
		plr->loadout_->skillPoints += 1;
		gServerLogic.ApiPlayerUpdateChar(plr,false);

		PKT_S2C_SkillSystem_s n;
		n.opCode = 1;
		n.level = plr->loadout_->charLevel;
		gServerLogic.p2pSendToPeer(plr->peerId_, plr, &n, sizeof(n));
	}
}


