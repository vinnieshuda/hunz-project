#include "r3dPCH.h"
#include "r3d.h"
#include "GameCommon.h"

#include "multiplayer/P2PMessages.h"
#include "ServerGameLogic.h"
#include "ObjectsCode/Weapons/WeaponArmory.h"
#include "ObjectsCode/Weapons/HeroConfig.h"

#include "sobj_Zombie.h"
#include "sobj_ZombieSpawn.h"
#include "ObjectsCode/obj_ServerPlayer.h"
#include "ObjectsCode/sobj_DroppedItem.h"
#include "ObjectsCode/obj_ServerBarricade.h"
#include "ServerWeapons/ServerWeapon.h"

#include "../../SkillSystem/SkillSystem.h"

#include "../../EclipseStudio/Sources/ObjectsCode/Gameplay/ZombieStates.h"

#include "../../GameEngine/ai/AutodeskNav/AutodeskNavMesh.h"

IMPLEMENT_CLASS(obj_Zombie, "obj_Zombie", "Object");
AUTOREGISTER_CLASS(obj_Zombie);

extern SkillSystem* gSkillSystem;

#if 0 //@ 
	//DEBUG VARIABLES
	int		_zai_DisableDetect       = 1;
	float		_zai_IdleStatePatrolPerc = 1.0f;
	float		_zai_NoPatrolPlayerDist  = -1.0f;
	float		_zai_PlayerSpawnProtect  = 0.0f;
	float		_zai_MaxSpawnDelay       = 0.1f;
	float		_zai_AttackDamage        = 0.0f;
	int		_zai_DebugAI             = 1;
#else
	int		_zai_DisableDetect       = 0;
	float		_zai_IdleStatePatrolPerc = 0.6f;
	float		_zai_NoPatrolPlayerDist  = 500.0;
	float		_zai_PlayerSpawnProtect  = 35.0f;	// radius where zombie won't be spawned because of player presense
	float		_zai_MaxSpawnDelay       = 9999.0f;
	float		_zai_AttackDamage        = 23.0f;
	int		_zai_DebugAI             = 0;
#endif
	float		_zai_MaxPatrolDistance   = 100.0f;	// distance to search for navpoints when switchint to patrol
	float		_zai_MaxPursueDistance   = 300.0f;
	float		_zai_MaxPursueDistanceS  = 100.0f;
	//float		_zai_AttackRadius        = 1.0f;
	float		_zai_AttackTimer         = 1.2f;
	float		_zai_DistToRecalcPath    = 0.8f; // _zai_AttackRadius / 2
	float		_zai_VisionConeCos       = cosf(90.0f); // have 100 degree vision
	float		_zai_maxVision			 = 40.f;
	float		_zai_maxNoiseDetection	 = 30.f;
	float		_zai_closestMoveDistance = 30.0f;
	bool		_zai_pursueEnemyNotVision = false;
	
	int		_zstat_NumZombies = 0;
	int		_zstat_NavFails   = 0;
	int		_zstat_Disabled   = 0;

obj_Zombie::obj_Zombie() : 
	netMover(this, 1.0f / 10.0f, (float)PKT_C2C_MoveSetCell_s::PLAYER_CELL_RADIUS)
{
	_zstat_NumZombies++;

	ZombieDisabled = 0;
	SuperDafiZombie = false;
	
	spawnObject    = NULL;
	RunSpeed       = -1;
	WalkSpeed      = -1;
	DetectRadius   = -1;

	ZombieState    = EZombieStates::ZState_Idle;
	StateStartTime = r3dGetTime();
	StateTimer     = -1;
	nextDetectTime = r3dGetTime() + 2.0f;
	
	navAgent       = NULL;
	patrolPntIdx   = -1;
	moveFrameCount = 0;
	
	staggerTime    = -1;
	animState      = 0;
	ZombieHealth   = 100;
	_zai_AttackRadius = 1.2f;
}

obj_Zombie::~obj_Zombie()
{
	_zstat_NumZombies--;
	if(ZombieDisabled)
		_zstat_Disabled--;

	gServerLogic.FreeNetId(this->GetNetworkID());
}

BOOL obj_Zombie::OnCreate()
{
	//r3dOutToLog("obj_Zombie %p %s created\n", this, Name.c_str()); CLOG_INDENT;
	_zai_is_gohome = false;
	ObjTypeFlags |= OBJTYPE_Zombie;
	
	r3d_assert(NetworkLocal);
	r3d_assert(GetNetworkID());
	r3d_assert(spawnObject);
	
	if(u_GetRandom() < spawnObject->sleepersRate)
		ZombieState = EZombieStates::ZState_Sleep;

	if(spawnObject->ZombieSpawnSelection.size() == 0)
		HeroItemID = u_GetRandom() >= 0.5f ? 20170 : 20183; // old behaviour. TODO: remove
	else
	{
		uint32_t idx1 = u_random(spawnObject->ZombieSpawnSelection.size());
		r3d_assert(idx1 < spawnObject->ZombieSpawnSelection.size());
		HeroItemID = spawnObject->ZombieSpawnSelection[idx1];
		if(HeroItemID == 20204)
		{
			_zai_AttackRadius = 2.5f;
			SuperDafiZombie = true;
			ZombieHealth = 300000;
		}

	}
	const HeroConfig* heroConfig = g_pWeaponArmory->getHeroConfig(HeroItemID);
	if(!heroConfig) { 
		r3dOutToLog("!!!! unable to spawn zombie - there is no hero config %d\n", HeroItemID);
		return FALSE;
	}
	
	FastZombie = u_GetRandom() > spawnObject->fastZombieChance;

	//HunZ - ha fast zombi akkor ne legyen mar kuszo is :D

	if(!FastZombie)
		CrawlZombie = u_GetRandom() > spawnObject->crawZombieChance;

	//HalloweenZombie = false; //NO more special zombies u_GetRandom() < (1.0f / 100) ? true : false; // every 100th zombie is special
	//if(HalloweenZombie) FastZombie = 1;

	HeadIdx = u_random(heroConfig->getNumHeads());
	BodyIdx = u_random(heroConfig->getNumBodys());
	LegsIdx = u_random(heroConfig->getNumLegs());

	//HunZ - maszo zombi de szarul van igy implementalva edes faszom
	if(!CrawlZombie)
	{
		WalkSpeed  = FastZombie ? 1.0f : 1.8f;
		RunSpeed   = FastZombie ? 2.9f : 3.2f;
	}else //HunZ - kuszo zombi speed
	{
		WalkSpeed  = CrawlZombie ? 1.0f : 1.8f;
		RunSpeed   = CrawlZombie ? 2.9f : 3.2f;
	}
	
	WalkSpeed += WalkSpeed * u_GetRandom(-spawnObject->speedVariation, +spawnObject->speedVariation);
	if(SuperDafiZombie)
	{

	RunSpeed  += 0.18f;
	WalkSpeed += 0.38f;

	}else 
	RunSpeed  += RunSpeed  * u_GetRandom(-spawnObject->speedVariation, +spawnObject->speedVariation);
	
	//r3d_assert(WalkSpeed > 0);
	//r3d_assert(RunSpeed > 0);
	//r3d_assert(DetectRadius >= 0);
	
	// need to create nav agent so it will be placed to navmesh position
	CreateNavAgent();
	spawnpos = GetPosition();

	gServerLogic.NetRegisterObjectToPeers(this);
	
	return parent::OnCreate();
}

BOOL obj_Zombie::OnDestroy()
{
	//r3dOutToLog("obj_Zombie %p destroyed\n", this);

	PKT_S2C_DestroyNetObject_s n;
	n.spawnID = toP2pNetId(GetNetworkID());
	gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));

	DeleteZombieNavAgent(navAgent);
	navAgent = NULL;
	
	return parent::OnDestroy();
}

void obj_Zombie::DisableZombie()
{
	AILog(0, "DisableZombie\n");
	StopNavAgent();

	ZombieDisabled = true;

	//DeleteZombieNavAgent(navAgent);
	//navAgent = NULL;

	_zstat_Disabled++;
	_zstat_NavFails++;
}

void obj_Zombie::AILog(int level, const char* fmt, ...)
{
	if(level > _zai_DebugAI)
		return;
		
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	StringCbVPrintfA(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	
	r3dOutToLog("AIZombie%p[%d] %s", this, navAgent->m_navBot->GetVisualDebugId(), buf);
}

bool obj_Zombie::CheckNavPos(r3dPoint3D& pos)
{
	Kaim::TriangleFromPosQuery q;
	q.Initialize(gAutodeskNavMesh.GetDB(), R3D_KY(pos));
	q.PerformQuery();
	switch(q.GetResult())
	{
		default:
			r3dOutToLog("!!!! TriangleFromPosQuery returned %d\n", q.GetResult());
			return false;
		case Kaim::TRIANGLEFROMPOS_DONE_NO_TRIANGLE_FOUND:
			return false;
		case Kaim::TRIANGLEFROMPOS_DONE_TRIANGLE_FOUND:
			pos.y = q.GetAltitudeOfProjectionInTriangle();
			return true;
	}
}

void obj_Zombie::CreateNavAgent()
{
	r3d_assert(!navAgent);

	// there is no checks here, they should be done in ZombieSpawn, so pos is navmesh position
	r3dPoint3D pos = GetPosition();
	navAgent = CreateZombieNavAgent(pos);

	//AILog(3, "created at %f %f %f\n", GetPosition().x, GetPosition().y, GetPosition().z);	
	
	return;
}

void obj_Zombie::StopNavAgent()
{
	if(!navAgent) return;
		
	if(patrolPntIdx >= 0)
	{
		spawnObject->ReleaseNavPoint(patrolPntIdx);
		patrolPntIdx = -1;
	}
	
	navAgent->StopMove();
}


void obj_Zombie::SetNavAgentSpeed(float speed)
{
	if(!navAgent) return;

	navAgent->SetTargetSpeed(speed);
}

bool obj_Zombie::MoveNavAgent(const r3dPoint3D& pos, float maxAstarRange)
{
	AILog(4, "navigating to %f %f %f from %f %f %f\n", pos.x, pos.y, pos.z, GetPosition().x, GetPosition().y, GetPosition().z);
	
	if(!navAgent->StartMove(pos, maxAstarRange))
		return false;
		
	moveFrameCount = 0;

	moveWatchTime  = 0;
	moveWatchPos   = GetPosition();
	moveStartPos   = GetPosition();
	moveTargetPos  = pos;
	moveStartTime  = r3dGetTime();
	moveAvoidTime  = 0;
	moveAvoidPos   = GetPosition();

	SendAIStateToNet();

	return true;
}

int obj_Zombie::CheckMoveWatchdog()
{
	if(navAgent->m_status == AutodeskNavAgent::ComputingPath)
		return 1;
	if(navAgent->m_status != AutodeskNavAgent::Moving)
		return 2;
	if(staggerTime > 0)
		return 1;
		
	// check every 5 sec that we moved somewhere
	moveWatchTime += r3dGetFrameTime();
	if(moveWatchTime < 5)
		return 1;
	
	if((GetPosition() - moveWatchPos).Length() < 0.5f)
	{
		AILog(1, "!!! move watchdog at %f %f %f\n", GetPosition().x, GetPosition().y, GetPosition().z);
		//DisableZombie();
		return 0;
	}

	moveWatchTime = 0;
	moveWatchPos  = GetPosition();
	return 1;
}

int obj_Zombie::CheckMoveStatus()
{
	float curTime = r3dGetTime();
	
	switch(navAgent->m_status)
	{
		case AutodeskNavAgent::Idle:
			return 2;
			
		case AutodeskNavAgent::ComputingPath:
			if(curTime > moveStartTime + 5.0f)
			{
				AILog(0, "!!! ComputingPath for %f\n", curTime - moveStartTime);
				_zstat_NavFails++;
				StopNavAgent();
				return 2;
			}
			return 1;
		
		case AutodeskNavAgent::PathNotFound:
			AILog(5, "PATH_NOT_FOUND %d to %f,%f,%f from %f,%f,%f\n", 
				navAgent->m_pathFinderQuery->GetResult(),
				moveTargetPos.x, moveTargetPos.z, moveTargetPos.y,
				moveStartPos.x, moveStartPos.z, moveStartPos.y);

			StopNavAgent();
			return 2;
			
		case AutodeskNavAgent::Moving:
			// break to next check for path status
			break;
			
		case AutodeskNavAgent::Arrived:
			return 0;

		case AutodeskNavAgent::Failed:
			AILog(5, "!!! Failed with %d\n", navAgent->m_navBot->GetTargetOnLivePathStatus());
			return 2;
	}
	
	Kaim::TargetOnPathStatus status = navAgent->m_navBot->GetTargetOnLivePathStatus();
	switch(status)
	{
		case Kaim::TargetOnPathNotInitialized:
		case Kaim::TargetOnPathUnknownReachability:
		case Kaim::TargetOnPathInInvalidNavData:
			if(curTime > moveStartTime + 5.0f)
			{
				AILog(0, "!!! GetPathStatus:%d for %f\n", status, curTime - moveStartTime); //@
				_zstat_NavFails++;
				StopNavAgent();
				return 2;
			}
			return 1;

		case Kaim::TargetOnPathNotReachable:
			return 2;
	}

	return 1;
}

GameObject* obj_Zombie::FindBarricade()
{
	for(std::vector<obj_ServerBarricade*>::iterator it = obj_ServerBarricade::allBarricades.begin(); it != obj_ServerBarricade::allBarricades.end(); ++it)
	{
		obj_ServerBarricade* shield = *it;
		if(SuperDafiZombie)
		{
			if((GetPosition() - shield->GetPosition()).Length() < 30.0f)
				continue;
			
		}else
		{
		// fast discard by radius
		if((GetPosition() - shield->GetPosition()).Length() > shield->m_Radius + _zai_AttackRadius)
			continue;
		}
		// get obstacle, TODO: rework to point vs OBB logic
		Kaim::WorldElement* e = gAutodeskNavMesh.obstacles[shield->m_ObstacleId];
		r3d_assert(e);
		if(e->GetType() != Kaim::TypeBoxObstacle)
			continue;
		Kaim::BoxObstacle* boxObstacle = static_cast<Kaim::BoxObstacle*>(e);
		
		// search for every spatial cylinder there
		for(KyUInt32 cidx = 0; cidx < boxObstacle->GetSpatializedCylinderCount(); cidx++)
		{
			const Kaim::SpatializedCylinder& cyl = boxObstacle->GetSpatializedCylinder(cidx);
			r3dPoint3D p1 = r3dPoint3D(GetPosition().x, 0, GetPosition().z);
			r3dPoint3D p2 = r3dPoint3D(cyl.GetPosition().x, 0, cyl.GetPosition().y); // KY_R3D
			float dist = (p1 - p2).Length() - cyl.GetRadius();
			if(SuperDafiZombie)
			{
				if(dist < 30.0f)
				{
					return shield;
				}
			}else
			{
				if(dist < _zai_AttackRadius * 0.7f)
				{
					return shield;
				}
			}
		}
	}
	return NULL;
}

bool obj_Zombie::CheckForBarricadeBlock()
{
	// we detect barricade by checking for them every sec if we're in avoidance mode.
	Kaim::IAvoidanceComputer::AvoidanceResult ares = navAgent->m_navBot->GetTrajectory()->GetAvoidanceResult();
	if(ares == Kaim::IAvoidanceComputer::NoAvoidance)
	{
		moveAvoidTime = 0;
		moveAvoidPos  = GetPosition();
		return false;
	}
	
	moveAvoidTime += r3dGetFrameTime();
	
	float avoidTimeCheck = 1.0f;
	float minDistToMove  = WalkSpeed * avoidTimeCheck * 0.8f;
	//r3dOutToLog("%f %f vs %f\n", moveAvoidTime, (GetPosition() - moveAvoidPos).Length(), minDistToMove);
	if(moveAvoidTime >= avoidTimeCheck)
	{
		float dist = (GetPosition() - moveAvoidPos).Length();
		moveAvoidTime = 0;
		moveAvoidPos  = GetPosition();

		if(dist >= minDistToMove)
			return false;
		
		GameObject* shield = FindBarricade();
		if(!shield)
			return false;
			
		StopNavAgent();
		
		// found barricade, wreck it!
		hardObjLock = shield->GetSafeID();
		attackTimer = _zai_AttackTimer / 2;
		SwitchToState(EZombieStates::ZState_BarricadeAttack);
		return true;
	}
	
	return false;
}


void obj_Zombie::FaceVector(const r3dPoint3D& v)
{
	float angle = atan2f(-v.z, v.x);
	angle = R3D_RAD2DEG(angle);
	SetRotationVector(r3dVector(angle - 90, 0, 0));
}
GameObject* obj_Zombie::canHearNoise()
{
	if(ZombieDisabled || _zai_DisableDetect)
		return NULL;

	GameObject* found   = NULL;

	for(int i=0; i<gServerLogic.MAX_PEERS_COUNT; i++)
	{
		obj_ServerPlayer* plr = gServerLogic.peers_[i].player;
		if(!plr)
			continue;
		if(plr->isincar)
			continue;
		if(gServerLogic.isModerator(plr))
			continue;

		float dist = (GetPosition() - plr->GetPosition()).Length();

		float tempDist = getPlayerByNoise(plr);
		if(dist < tempDist)
		{
			found = plr;
			return found;
		}
	}
	return found;

}
GameObject* obj_Zombie::canSeeAnything()
{	
	if(ZombieDisabled || _zai_DisableDetect)
		return NULL;

	GameObject* found   = NULL;
/*
	for(GameObject* obj = GameWorld().GetFirstObject(); obj; obj = GameWorld().GetNextObject(obj))
	{
		if(!obj->isObjType(OBJTYPE_Human))
			continue;
		obj_ServerPlayer* plr = (obj_ServerPlayer*)obj;
		if(!plr)
			continue;
		if(plr->isincar)
			continue;
		if(gServerLogic.isModerator(plr))
			continue;

		float dist = (GetPosition() - plr->GetPosition()).Length();

		if(dist < _zai_maxVision)
		{
			if(CheckViewToPlayer(found))
			{
				found   = obj;
				return found;
			}
		}
	}*/
	// scan for all player
	for(int i=0; i<gServerLogic.MAX_PEERS_COUNT; i++)
	{
		obj_ServerPlayer* plr = gServerLogic.peers_[i].player;
		if(!plr)
			continue;
		if(plr->isincar)
			continue;
		if(gServerLogic.isModerator(plr))
			continue;
		if(plr->loadout_->Alive == 0)
			continue;
		if(plr->virtualdeath)
			continue;
		float curtime = r3dGetTime();

		//HunZ - zombieprotect

		if(plr->FireHitCount == 0  && curtime < plr->startPlayTime_ + 60.0f)
			continue;

		float dist = (GetPosition() - plr->GetPosition()).Length();

		if(dist < _zai_maxVision - modifyVisionBy(plr))
		{
			if(CheckViewToPlayer(plr))
			{
				found   = plr;
				return found;
			}
		}
	}
	return found;
}

float obj_Zombie::modifyVisionBy(obj_ServerPlayer* plr)
{
	float ret = 0.0f;

	switch(plr->m_PlayerState)
	{
	case PLAYER_IDLE:
	case PLAYER_IDLEAIM:
		{
			ret = 0.0f;
			return ret;
		}
	case PLAYER_MOVE_CROUCH:
	case PLAYER_MOVE_CROUCH_AIM:
		{
			ret = 20.0f;
			return ret;
		}
	case PLAYER_MOVE_PRONE:
	case PLAYER_MOVE_PRONE_AIM:
		{
			ret = 30.0f;
			return ret;
		}
	case PLAYER_MOVE_WALK_AIM:
	case PLAYER_MOVE_RUN:
		{
			ret = -5.0f;
			return ret;
		}
	case PLAYER_MOVE_SPRINT:
		{
			ret = -15.0f;
			return ret;
		}
	}
	return ret;
}

float obj_Zombie::getPlayerByNoise(obj_ServerPlayer* plr)
{
	float ret = 0.0f;

	switch(plr->m_PlayerState)
	{
		case PLAYER_IDLE:
		case PLAYER_IDLEAIM:
			{
				ret = 3.0f;
				return ret;
			}
		case PLAYER_MOVE_CROUCH:
		case PLAYER_MOVE_CROUCH_AIM:
			{
				ret = 1.0f;
				return ret;
			}
		case PLAYER_MOVE_PRONE:
		case PLAYER_MOVE_PRONE_AIM:
			{
				ret = 1.0f;
				return ret;
			}
		case PLAYER_MOVE_RUN:
			{
				ret = 4.0f;
				return ret;
			}
		case PLAYER_MOVE_SPRINT:
			{
				ret = 6.0f;
				return ret;
			}
	}
	return ret;
}

GameObject* obj_Zombie::ScanForTarget(bool immidiate)
{
	if(ZombieDisabled || _zai_DisableDetect)
		return NULL;
		
	obj_ServerPlayer* found   = NULL;
	float             minDist = 10.0f;

	// scan for all player
	for(int i=0; i<gServerLogic.MAX_PEERS_COUNT; i++)
	{
		obj_ServerPlayer* plr = gServerLogic.peers_[i].player;
		if(!plr)
			continue;
		if(plr->isincar)
			continue;
		if(gServerLogic.isModerator(plr))
			continue;

		float dist = (GetPosition() - plr->GetPosition()).Length();

		if(dist < minDist)
		{
			found   = plr;
			return found;
		}
	}

	//if(found) r3dOutToLog("zombie%p GetClosestPlayerBySenses %s\n", this, found->userName);
	return found;
}

bool obj_Zombie::IsPlayerDetectable(const obj_ServerPlayer* plr, float dist)
{
	if(plr->loadout_->Alive == 0)
		return false;
	if(plr->isincar)
		return false;


	// vision is disabled in sleep mode
	if(ZombieState == EZombieStates::ZState_Sleep)
		return false;

/*
	// vision check: range
	const static float VisionDetectRangesByState[] = {
		 4.0f, //PLAYER_IDLE = 0,
		 4.0f, //PLAYER_IDLEAIM,
		 7.0f, //PLAYER_MOVE_CROUCH,
		 7.0f, //PLAYER_MOVE_CROUCH_AIM,
		10.0f, //PLAYER_MOVE_WALK_AIM,
		15.0f, //PLAYER_MOVE_RUN,
		30.0f, //PLAYER_MOVE_SPRINT,
		10.0f, //PLAYER_MOVE_swim1,
		 15.0f, //PLAYER_MOVE_swim2,
		 4.0f, //PLAYER_MOVE_PRONE,
		 4.0f, //PLAYER_MOVE_PRONE_AIM,
		 1.8f, //PLAYER_PRONE_UP,
		 1.8f, //PLAYER_PRONE_DOWN,
		 1.8f, //PLAYER_PRONE_IDLE,
		 0.0f, //PLAYER_DIE,
	};
	COMPILE_ASSERT( R3D_ARRAYSIZE(VisionDetectRangesByState) == PLAYER_NUM_STATES);
	
	if(plr->m_PlayerState < 0 || plr->m_PlayerState >= PLAYER_NUM_STATES) {
		r3dOutToLog("!!! bad state\n");
		return false;
	}
	if(dist > VisionDetectRangesByState[plr->m_PlayerState]) {
		return false;
	}*/
	
	// vision check: cone of view
	/*{
		r3dPoint3D v1(0, 0, -1);
		v1.RotateAroundY(GetRotationVector().x);
		r3dPoint3D v2 = (plr->GetPosition() - GetPosition()).NormalizeTo();
		
		float dot = v1.Dot(v2);
		//float deg = R3D_RAD2DEG(acosf(dot));
		if(dot < _zai_VisionConeCos)
			return false;
	}*/
	
	// vision check: obstacles
	if(CheckViewToPlayer(plr))
	{
		StartAttack(plr);
		return true;
	}
		
	return false;
}

GameObject* obj_Zombie::GetClosestPlayerBySenses()
{
	obj_ServerPlayer* found   = NULL;
	float             minDist = 9999999;
	
	// scan for all player
	for(int i=0; i<gServerLogic.MAX_PEERS_COUNT; i++)
	{
		obj_ServerPlayer* plr = gServerLogic.peers_[i].player;
		if(!plr)
			continue;
		if(plr->isincar)
			continue;
		if(gServerLogic.isModerator(plr))
			continue;

		float dist = (GetPosition() - plr->GetPosition()).Length();
		if(!IsPlayerDetectable(plr, dist))
			continue;
			
		if(dist < minDist)
		{
			minDist = dist;
			found   = plr;
		}
	}
	
	//if(found) r3dOutToLog("zombie%p GetClosestPlayerBySenses %s\n", this, found->userName);
	return found;
}

bool obj_Zombie::CheckViewToPlayer(const GameObject* obj)
{
	obj_ServerPlayer* plr = (obj_ServerPlayer*)obj;
	if(!plr)
		return false;
	if(plr->virtualdeath)
		return false;
	if(plr->loadout_->Alive == 0)
		return false;

	const float eyeHeight = 1.6f;
	
	// Issue raycast query to check visibility occluders
	float dist = (GetPosition() - obj->GetPosition()).Length();
	if(dist < 1.0f)
		return true;
	if(dist > _zai_maxVision)
		return false;
/*
	if(dist < 0)
		return true;*/
	r3dPoint3D v1(0, 0, -1);
	v1.RotateAroundY(GetRotationVector().x);
	r3dPoint3D v2 = (obj->GetPosition() - GetPosition()).NormalizeTo();

	float dot = v1.Dot(v2);
	//float deg = R3D_RAD2DEG(acosf(dot));
	if(dot > _zai_VisionConeCos)
	{
		r3dPoint3D dir = (obj->GetPosition() - GetPosition());
		dir.Normalize();

		PxVec3 porigin(GetPosition().x, GetPosition().y + eyeHeight, GetPosition().z);
		PxVec3 pdir(dir.x, dir.y, dir.z);
		PxSceneQueryFlags flags = PxSceneQueryFlag::eNORMAL;
		PxRaycastHit hit;
		PxSceneQueryFilterData filter(PxFilterData(/*COLLIDABLE_STATIC_MASK*/COLLIDABLE_PLAYER_COLLIDABLE_MASK, 0, 0, 0), PxSceneQueryFilterFlags(PxSceneQueryFilterFlag::eDYNAMIC | PxSceneQueryFilterFlag::eSTATIC));
		if(g_pPhysicsWorld->PhysXScene->raycastSingle(porigin, pdir, dist, flags, hit, filter))
		{
			PhysicsCallbackObject* target = NULL;
			if(hit.shape && (target = static_cast<PhysicsCallbackObject*>(hit.shape->getActor().userData)))
			{
				GameObject* obj = target->isGameObject();
				if(obj)
				{
					return false;
				}
			}
		}else return true;
	}else return false;

	return false;
}

bool obj_Zombie::SenseWeaponFire(const obj_ServerPlayer* plr, const ServerWeapon* wpn, r3dPoint3D* trgpos)
{
	if(ZombieState == EZombieStates::ZState_Dead)
		return false; // HunZ - halott ne halljon semmitsenemse.
	if(ZombieState == EZombieStates::ZState_Waking)
		return false; // HunZ - ez meg itt meg almos
	if(ZombieState == EZombieStates::ZState_Pursue)
		return false; // HunZ - ez meg uldoz
	if(ZombieState == EZombieStates::ZState_Attack)
		return false; // HunZ - ez nyavogva utlegel!
	if(ZombieState == EZombieStates::ZState_BarricadeAttack)
		return false; // HunZ - barrikad
	if(ZombieState == EZombieStates::ZState_MoveToBarricade)
		return false; // HunZ - barrikad
		
	if(!wpn)
		return false;

	float range = 0;
	float wpnsound = 0;
	switch(wpn->getCategory())
	{
		default:
		case storecat_SNP:
			{
				range = 250.0f;
				wpnsound = 75.0f;
			break;
			}

		case storecat_ASR:
		case storecat_SHTG:
		case storecat_MG:
			{
				range = 150.0f;
				wpnsound = 50.0f;
				break;
			}

		case storecat_HG:
		case storecat_SMG:
			{
				range = 75.0f;
				wpnsound = 25.0f;
				break;
			}
			
		case storecat_MELEE:
			{
				range = 5.0f; // sergey's design.
				break;
			}
	}

	// silencer halves range
	if(wpn->m_Attachments[WPN_ATTM_MUZZLE] && wpn->m_Attachments[WPN_ATTM_MUZZLE]->m_itemID == 400013)
	{
		range = 10.0f;
		wpnsound = 10.0f;
	}
	if(wpn->getConfig()->m_itemID == 101106 || wpn->getConfig()->m_itemID == 101322)
	{
		range = 10.0f;
		wpnsound = 10.0f;
	}
	
	// override for .50 cal - big range, no silencer
	if(wpn->getConfig()->m_itemID == 101088)
	{
		range = 250.0f;
		wpnsound = 75.0f;
	}

	float dist = (GetPosition() - plr->GetPosition()).Length();

	if(dist < wpnsound)
	{
		if(ZombieState == EZombieStates::ZState_Walk)
			StopNavAgent();
		FaceVector(plr->GetPosition() - GetPosition());
		if(CheckViewToPlayer(plr))
		{
			if(ZombieState == EZombieStates::ZState_Sleep)
			{
				SwitchToState(EZombieStates::ZState_Waking);
				return false;
			}
			StartAttack(plr);	
			return true;
		}else
		{
			r3dPoint3D temp = gServerLogic.getRandomCircleFitt(plr->GetPosition().x, plr->GetPosition().z, _zai_closestMoveDistance);
			temp.y = plr->GetPosition().y;
			trgpos->x = temp.x;
			trgpos->z = temp.z;
			trgpos->y = temp.y;
			if(gAutodeskNavMesh.GetClosestNavMeshPoint(temp, 1500.0f, _zai_closestMoveDistance)/*gAutodeskNavMesh.IsNavPointValid(temp)*/)
			{
				SetNavAgentSpeed(WalkSpeed);
				MoveNavAgent(temp, 1000.0f);
				if(ZombieState != EZombieStates::ZState_Walk)
					SwitchToState(EZombieStates::ZState_Walk);
				return true;
			}
		}
	}else if(dist < range)
	{
		if(ZombieState == EZombieStates::ZState_Walk)
			StopNavAgent();

		if(ZombieState == EZombieStates::ZState_Sleep)
		{
			SwitchToState(EZombieStates::ZState_Waking);
			return false;
		}

		r3dPoint3D temp = gServerLogic.getRandomCircleFitt(plr->GetPosition().x, plr->GetPosition().z, _zai_closestMoveDistance);
		temp.y = plr->GetPosition().y;
		trgpos->x = temp.x;
		trgpos->z = temp.z;
		trgpos->y = temp.y;
		if(gAutodeskNavMesh.GetClosestNavMeshPoint(temp, 1500.0f, _zai_closestMoveDistance))
		{
			SetNavAgentSpeed(WalkSpeed);
			MoveNavAgent(temp, 1000.0f);
			if(ZombieState != EZombieStates::ZState_Walk)
				SwitchToState(EZombieStates::ZState_Walk);
			return true;
		}
	}else return false;
/*
	if(dist > range)
	{	
		return false;
	}

	// if player can't be seen, check agains halved radius
	if(!CheckViewToPlayer(plr))
	{
		if(dist > range * 0.5f)
			return false;
	}

	//r3dOutToLog("zombie%p sensed weapon fire from %s\n", this, plr->userName); CLOG_INDENT;

	// check if new target is closer that current one
	const GameObject* trg = GameWorld().GetObject(hardObjLock);
	if((trg == NULL) || (trg && dist < (trg->GetPosition() - GetPosition()).Length()))
	{
		StartAttack(plr);
		return true;
	}
	
	// if this is same target, recalculate path if he was moved
	if(ZombieState == EZombieStates::ZState_Pursue && trg == plr && (trg->GetPosition() - lastTargetPos).Length() > _zai_DistToRecalcPath)
	{
		lastTargetPos = trg->GetPosition();
		if(SuperDafiZombie)
		{
			MoveNavAgent(trg->GetPosition(), _zai_MaxPursueDistanceS);
		}else MoveNavAgent(trg->GetPosition(), _zai_MaxPursueDistance);
	}*/
	
	return false;
}

bool obj_Zombie::StartAttack(const GameObject* trg)
{
	r3d_assert(ZombieState != EZombieStates::ZState_Dead);

	if(ZombieDisabled)
		return false;
	if(hardObjLock == trg->GetSafeID())
		return true;

	if(ZombieState == EZombieStates::ZState_Sleep)
	{
		// wake up sleeper
		SwitchToState(EZombieStates::ZState_Waking);
		return true;
	}

	StopNavAgent(); // to release current nav point from Walk state

	AILog(2, "attacking %s\n", trg->Name.c_str()); CLOG_INDENT;

	float dist = (GetPosition() - trg->GetPosition()).Length();
	if(SuperDafiZombie)
	{
		if(dist > _zai_MaxPursueDistanceS)
			return false;
	}else if(dist > _zai_MaxPursueDistance)
		return false;
		
	// check if we can switch to melee immidiately
	if(dist < _zai_AttackRadius)
	{
		hardObjLock = trg->GetSafeID();

		if(ZombieState != EZombieStates::ZState_Attack)
			SwitchToState(EZombieStates::ZState_Attack);
			
		attackTimer   = _zai_AttackTimer / 2;
		isFirstAttack = true;
		return true;
	}

	// check if zombie can get to the player within 2 radius of attack
	r3dPoint3D trgPos = trg->GetPosition();
	if(!gAutodeskNavMesh.AdjustNavPointHeight(trgPos, 1.0f))
	{
		if(!gAutodeskNavMesh.GetClosestNavMeshPoint(trgPos, 2.0f, _zai_AttackRadius * 2))
		{
			AILog(5, "player offmesh at %f %f %f\n", trgPos.x, trgPos.y, trgPos.z);
			return false;
		}
		
		if((trgPos - GetPosition()).Length() < 1.0f)
		{
			AILog(5, "player offmesh and we can't reach him\n");
			return false;
		}

		// ok, we'll reach him in end of our path
		AILog(5, "going to offmesh player to %f %f %f\n", trgPos.x, trgPos.y, trgPos.z);
	}

	// start pursuing immidiately
	SetNavAgentSpeed(staggerTime < 0 ? RunSpeed : 0.0f);
	if(SuperDafiZombie)
	{
		if(!MoveNavAgent(trgPos, _zai_MaxPursueDistanceS))
			return false;
	}else if(!MoveNavAgent(trgPos, _zai_MaxPursueDistance))
		return false;
	
	lastTargetPos = trg->GetPosition();
	hardObjLock   = trg->GetSafeID();

	// switch to pursue mode
	if(ZombieState != EZombieStates::ZState_Pursue) 
	{
		SwitchToState(EZombieStates::ZState_Pursue);
	}
	
	return true;
}

void obj_Zombie::StopAttack()
{
	r3d_assert(ZombieState == EZombieStates::ZState_Attack || ZombieState == EZombieStates::ZState_BarricadeAttack);
	
	hardObjLock = invalidGameObjectID;

	// scan for new target immiiately
	if(GameObject* trg = ScanForTarget(true))
	{
		if(StartAttack(trg))
			return;
	}
	
	// start attack failed or no target to attack, switch to idle
	SwitchToState(EZombieStates::ZState_Idle);
}

BOOL obj_Zombie::Update()
{
	parent::Update();

	if(!isActive())
		return TRUE;

	const float curTime = r3dGetTime();

	if(ZombieState == EZombieStates::ZState_Dead)
	{
		// deactivate zombie after some time
		if(curTime > StateStartTime + 60.0f)
		{

			setActiveFlag(0);
		}
		return TRUE;
	}

	DebugSingleZombie();

	// Propagate AI agent position to zombie position
	if(navAgent)
	{
		SetPosition(navAgent->GetPosition());

		if(navAgent->m_status == AutodeskNavAgent::Moving)
		{
			Kaim::Vec3f rot  = navAgent->m_velocity;
			if(rot.GetSquareLength2d() > 0.001f)
				FaceVector(r3dPoint3D(rot[0], rot[2], rot[1]));
		}
	}
	moveFrameCount++;

	// check for stagger
	if(staggerTime > 0)
	{
		animState = 1;
		staggerTime -= r3dGetFrameTime();
		if(staggerTime <= 0.001f)
		{
			animState = 0;
			staggerTime = -1;
		}
	}
	if(_zai_is_gohome)
	{
		float dist2 = (GetPosition() - spawnpos).Length();
		if(dist2 < 2.0f)
		{
			_zai_is_gohome = false;
		}
	}

	// send network position update
	{
		CNetCellMover::moveData_s md;
		md.pos       = GetPosition();
		md.turnAngle = GetRotationVector().x;
		md.bendAngle = 0;
		md.state     = (BYTE)animState;

		PKT_C2C_MoveSetCell_s n1;
		PKT_C2C_MoveRel_s     n2;
		DWORD pktFlags = netMover.SendPosUpdate(md, &n1, &n2);
		if(pktFlags & 0x1)
			gServerLogic.p2pBroadcastToActive(this, &n1, sizeof(n1));
		if(pktFlags & 0x2)
			gServerLogic.p2pBroadcastToActive(this, &n2, sizeof(n2));
	}

	switch(ZombieState)
	{
	default:
		break;

	case EZombieStates::ZState_Sleep:
		{
			if(GameObject* trg = ScanForTarget())
			{
				SwitchToState(EZombieStates::ZState_Waking);
			}
			break;
		}

	case EZombieStates::ZState_Waking:
		{
			if(curTime > StateStartTime + 3.0f)
			{
				SwitchToState(EZombieStates::ZState_Idle);
				break;
			}
		/*	// perform immidiate surrounding check if we don't have target yet
			if(hardObjLock == invalidGameObjectID)
			{
				if(GameObject* trg = canSeeAnything())
				{
					StartAttack(trg);
					break;
				}
			}

			GameObject* trg = GameWorld().GetObject(hardObjLock);
			if(!trg)
			{
				// no target, switch to idle
				SwitchToState(EZombieStates::ZState_Idle);
				break;
			}
			else
			{
				StartAttack(trg);
			}
			break;*/
			break;
		}

	case EZombieStates::ZState_Idle:
		{
			if(GameObject* trg = canSeeAnything())
			{
				StartAttack(trg);
				break;
			}

			if(curTime < StateTimer || staggerTime > 0) 
				break;

			if(GameObject* trg = canHearNoise())
			{			
				r3dPoint3D temp = gServerLogic.getRandomCircleFitt(trg->GetPosition().x, trg->GetPosition().z, 5.0f);
				temp.y = trg->GetPosition().y;
				if(gAutodeskNavMesh.GetClosestNavMeshPoint(temp, 1500.0f, 5.0f))
				{
					SetNavAgentSpeed(WalkSpeed);
					MoveNavAgent(temp, 250.0f);
					SwitchToState(EZombieStates::ZState_Walk);
					break;
				}
			}

			bool doPatrol = u_GetRandom() < _zai_IdleStatePatrolPerc && !ZombieDisabled;
			if(doPatrol && _zai_NoPatrolPlayerDist > 0 && gServerLogic.CheckForPlayersAround(GetPosition(), _zai_NoPatrolPlayerDist) == false)
			{
				StateTimer = curTime + u_GetRandom(3, 5);
				break;
			}

			if(doPatrol) // HunZ - zombi mozgas felderitese
			{
				r3dPoint3D out_pos;
				out_pos = gServerLogic.getRandomCircleFitt(GetPosition().x, GetPosition().z, _zai_MaxPatrolDistance);
				out_pos.y = GetPosition().y;
				if(gAutodeskNavMesh.GetClosestNavMeshPoint(out_pos, 100.0f, _zai_closestMoveDistance))
				{
					SetNavAgentSpeed(WalkSpeed);
					MoveNavAgent(out_pos, _zai_MaxPatrolDistance * 2);
					SwitchToState(EZombieStates::ZState_Walk);
					break;
				}else
				{
					StateTimer = curTime + u_GetRandom(5, 60);
					break;
				}
			}
			else
			{
				// continuing idle time for some more
				StateTimer = curTime + u_GetRandom(5, 60);
			}
			break;
		}
	case EZombieStates::ZState_MoveAwayFromEnemy:
		{
			switch(CheckMoveStatus())
			{
				default: r3d_assert(false);
				case 0: // completed
				case 2: // failed
					SwitchToState(EZombieStates::ZState_Idle);
					break;
				case 1: // in progress
					if(!CheckMoveWatchdog())
					{
						SwitchToState(EZombieStates::ZState_Idle);
					}
					break;
			}

			break;
		}
	case EZombieStates::ZState_Walk:
		{
			r3d_assert(!ZombieDisabled && navAgent);

			if(staggerTime < 0)
				SetNavAgentSpeed(WalkSpeed);

			if(CheckForBarricadeBlock())
			{
				break;
			}

			if(GameObject* trg = canSeeAnything())
			{
				StartAttack(trg);
				break;
			}
/*
			if(staggerTime < 0)
				SetNavAgentSpeed(WalkSpeed);
			if(isBarricadeInArea())
			{
				break;	
			}else if(GameObject* trg = canSeeAnything())
			{
				StartAttack(trg);
				break;
			}			
			if(CheckForBarricadeBlock() && !isBarricadeInArea())
			{
				break;
			}*/

			switch(CheckMoveStatus())
			{
			default: r3d_assert(false);
			case 0: // completed
			case 2: // failed
				SwitchToState(EZombieStates::ZState_Idle);
				break;
			case 1: // in progress
				if(!CheckMoveWatchdog())
				{
					SwitchToState(EZombieStates::ZState_Idle);
				}
				break;
			}

			break;
		}

	case EZombieStates::ZState_Pursue:
		{
			r3d_assert(!ZombieDisabled && navAgent);
			static r3dPoint3D trgPos;

			GameObject* trg = GameWorld().GetObject(hardObjLock);
			if(trg)
			{				
				// check if we're within melee range
				float dist = (trg->GetPosition() - GetPosition()).Length();
				trgPos = trg->GetPosition();
				if(dist < _zai_AttackRadius && staggerTime < 0)
				{
					FaceVector(trg->GetPosition() - GetPosition());
					StopNavAgent();
					SwitchToState(EZombieStates::ZState_Attack);
					attackTimer   = _zai_AttackTimer / 2;
					isFirstAttack = true;
					break;
				}
			}
			if(staggerTime < 0)
			{
				SetNavAgentSpeed(RunSpeed);
			}
			if(trg && CheckViewToPlayer(trg))
			{

				r3dPoint3D pos = trg->GetPosition();
				if((trg->GetPosition() - lastTargetPos).Length() > _zai_DistToRecalcPath)
				{
					lastTargetPos = trg->GetPosition();
					MoveNavAgent(trg->GetPosition(), _zai_MaxPursueDistance);
				}
				if(!gAutodeskNavMesh.GetClosestNavMeshPoint(pos, 2.1f, 1.1f))
				{
					// HunZ - todo, ha nincs navmesh
					/*trgPos = gServerLogic.getRandomCircleFitt(GetPosition().x, GetPosition().z, 100.f);
					if(gAutodeskNavMesh.GetClosestNavMeshPoint(trgPos, 1500.0f, 200.0f))
					{
						StopNavAgent();
						SetNavAgentSpeed(WalkSpeed);
						MoveNavAgent(trgPos, 250.0f);
						hardObjLock = invalidGameObjectID;
						SwitchToState(EZombieStates::ZState_MoveAwayFromEnemy);
						break;
					}*/
				}
			}else if(GameObject* trg = canSeeAnything())
			{
				if(trg->GetSafeID() != hardObjLock)
				{
					StartAttack(trg);
				}
			}else
			{
				trgPos = gServerLogic.getRandomCircleFitt(trgPos.x, trgPos.z, u_GetRandom(1.0f,_zai_closestMoveDistance));
				if(gAutodeskNavMesh.GetClosestNavMeshPoint(trgPos, 1500.0f, _zai_closestMoveDistance))
				{
					StopNavAgent();
					SetNavAgentSpeed(WalkSpeed);
					MoveNavAgent(trgPos, 250.0f);
					hardObjLock = invalidGameObjectID;
					_zai_pursueEnemyNotVision = true;
					if(ZombieState != EZombieStates::ZState_Walk)
						SwitchToState(EZombieStates::ZState_Walk);
					break;
				}

			}
			// check if we still have target in our visibility
/*
			if(isBarricadeInArea())
			{
				break;
			}else if(GameObject* trg = canSeeAnything())
			{
				if(!trg)
				{
					StopAttack();
				}
				if(trg->GetSafeID() != hardObjLock)
				{
					// new target, switch to him
					StartAttack(trg);
					break;
				}

				// if player went off mesh - do nothing, continue what we was doing

				// recalculate paths sometime
				if((trg->GetPosition() - lastTargetPos).Length() > _zai_DistToRecalcPath)
				{
					lastTargetPos = trg->GetPosition();
					if(SuperDafiZombie)
					{
						MoveNavAgent(trg->GetPosition(), _zai_MaxPursueDistanceS);
					}else MoveNavAgent(trg->GetPosition(), _zai_MaxPursueDistance);
				}
				r3dPoint3D pos = trg->GetPosition();
				if(SuperDafiZombie && !_zai_is_gohome)
				{
					float dist2 = (GetPosition() - spawnpos).Length();
					if(dist2 > 10.0f)
					{
						_zai_is_gohome = true;
						MoveNavAgent(spawnpos, 200.0f);
						break;
					}
				}else if(gAutodeskNavMesh.GetClosestNavMeshPoint(pos, 1.1f, 1.1f))
				{
					if(isBarricadeInArea() && SuperDafiZombie)
					{
						break;
					}else
					{
						StartAttack(trg);
						break;
					}
				}else if(!gAutodeskNavMesh.IsNavPointValid(trg->GetPosition()))
				{
					MoveNavAgent(spawnpos, 9999.0f);
					break;
				}
			}
			if(CheckForBarricadeBlock() && !isBarricadeInArea())
			{
				break;
			}*/

			switch(CheckMoveStatus())
			{
			default: r3d_assert(false);
			case 0: // completed
			case 2: // failed
				{
					hardObjLock = invalidGameObjectID;
					if(ZombieState != EZombieStates::ZState_Idle)
						SwitchToState(EZombieStates::ZState_Idle);
					break;
				}
			case 1: // in progress
				break;
			}

			break;
		}

	case EZombieStates::ZState_Attack:
		{
			obj_ServerPlayer* trg = IsServerPlayer(GameWorld().GetObject(hardObjLock));
			if(trg)
				FaceVector(trg->GetPosition() - GetPosition());
			if(!trg || trg->loadout_->Alive == 0 || trg->isincar)
			{
				StopAttack();
				break;
			}

			if(staggerTime > 0)
			{
				attackTimer   = 0;
				isFirstAttack = false;
				break;
			}

			attackTimer += r3dGetFrameTime();
			if(attackTimer >= _zai_AttackTimer)
			{
				attackTimer = 0;

				// first attack always land
				bool canAttack = true;
				if(!isFirstAttack)
				{
					float dist = (trg->GetPosition() - GetPosition()).Length();
					if(dist > _zai_AttackRadius * 1.1f || !CheckViewToPlayer(trg))
						canAttack = false;
				}

				if(!canAttack)
				{
					StopAttack();
					break;
				}
				else
				{
					isFirstAttack = false;

					PKT_S2C_ZombieAttack_s n;
					n.targetId = toP2pNetId(trg->GetNetworkID());
					gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));


					//-1% poison

					/*trg->CurrentlyUsedSkill = gSkillSystem->GetSkillById(trg, SKILL_MINUS_POISON);
					float modifier = gSkillSystem->GetSkillEffectBySkill(trg->CurrentlyUsedSkill);*/

					//-2% bleeding

					/*trg->CurrentlyUsedSkill = gSkillSystem->GetSkillById(trg, SKILL_MINUS_BLEEDING);
					float bleedingmodifier = gSkillSystem->GetSkillEffectBySkill(trg->CurrentlyUsedSkill);*/

					// HunZ - verzes 

					if(SuperDafiZombie)
					{
						if(u_GetRandom() < (0.6f /*- bleedingmodifier*/) && trg->loadout_->isbleeding != 1 && !trg->profile_.ProfileData.isDevAccount)
						{
							trg->loadout_->isbleeding = 1;
							trg->loadout_->bleedinglevel = 1;
							gServerLogic.ApiPlayerUpdateChar(trg);
							trg->DoCharBleeding(1, 1);
							trg->bleedingstarted = true;
						}
						trg->loadout_->Health -= _zai_AttackDamage + 20.0f;
					}
					else
					{ 
						if(u_GetRandom() < 0.1f /*- bleedingmodifier*/ && trg->loadout_->isbleeding != 1 && !trg->profile_.ProfileData.isDevAccount)
						{
							trg->loadout_->isbleeding = 1;
							trg->loadout_->bleedinglevel = 1;
							gServerLogic.ApiPlayerUpdateChar(trg);
							trg->DoCharBleeding(1, 1);
							trg->bleedingstarted = true;
						}					
						trg->loadout_->Health -= _zai_AttackDamage;
					}
					if(SuperDafiZombie)
					{

						trg->loadout_->Toxic += 10.0f /*- modifier*/;
					}
					else if(u_GetRandom(0.0f, 100.0f) < 7.0f /* - modifier*/) // chance of infecting //TODO poison implement better chance
					{
						trg->loadout_->Toxic += u_GetRandom(1.0f, 10.0f);
					}

					// HunZ - dev cant die from zombie
					if(trg->profile_.ProfileData.isDevAccount)
					{
						if(trg->loadout_->Health <= 97.0f)
							trg->loadout_->Health = 100;
						break;
					}

					if(trg->loadout_->Health <= 0)
					{
						trg->loadout_->Health = 0;
						gServerLogic.DoKillPlayer(this, trg, storecat_MELEE);

						StopAttack();
						break;
					}
				}
			}
			break;
		}
	case EZombieStates::ZState_MoveToBarricade:
		{
			obj_ServerBarricade* shield = (obj_ServerBarricade*)GameWorld().GetObject(hardObjLock);
			if(!shield)
			{
				if(GameObject* trg = ScanForTarget())
				{
					if((trg->GetPosition() - GetPosition()).Length() < _zai_AttackRadius)
					{
						StartAttack(trg);
						break;
					}
				}else SwitchToState(EZombieStates::ZState_Idle);
				break;
			}
			float dist = (shield->GetPosition() - GetPosition()).Length();
			if(dist <= 2.0f)
			{
				SwitchToState(EZombieStates::ZState_BarricadeAttack);
				break;
			}
			break;
		}

	case EZombieStates::ZState_BarricadeAttack:
		{
			obj_ServerBarricade* shield = (obj_ServerBarricade*)GameWorld().GetObject(hardObjLock);
			if(!shield)
			{
				StopAttack();
				break;
			}

			// check if we have player in melee range

			if(GameObject* trg = ScanForTarget())
			{
				if((trg->GetPosition() - GetPosition()).Length() < _zai_AttackRadius)
				{
					StartAttack(trg);
					break;
				}
			}


			FaceVector(shield->GetPosition() - GetPosition());

			if(staggerTime > 0)
			{
				attackTimer   = 0;
				break;
			}

			attackTimer += r3dGetFrameTime();
	
			StopNavAgent();
			if(attackTimer >= _zai_AttackTimer)
			{
				attackTimer = 0;
				if(SuperDafiZombie)
					shield->DoDamage(_zai_AttackDamage + 1000.0f);
				else shield->DoDamage(_zai_AttackDamage);
			}
			break;
		}

	case EZombieStates::ZState_Dead:
		break;
	}

	return TRUE;
}

DefaultPacket* obj_Zombie::NetGetCreatePacket(int* out_size)
{
	static PKT_S2C_CreateZombie_s n;
	n.spawnID    = toP2pNetId(GetNetworkID());
	n.spawnPos   = GetPosition();
	n.spawnDir   = GetRotationVector().x;
	n.moveCell   = netMover.SrvGetCell();
	n.HeroItemID = HeroItemID;
	n.HeadIdx    = (BYTE)HeadIdx;
	n.BodyIdx    = (BYTE)BodyIdx;
	n.LegsIdx    = (BYTE)LegsIdx;
	n.State      = (BYTE)ZombieState;
	n.FastZombie = (BYTE)FastZombie;
	n.CrawlZombie = (BYTE)CrawlZombie;
	n.WalkSpeed  = WalkSpeed;
	n.RunSpeed   = RunSpeed;
	//if(HalloweenZombie) n.HeroItemID += 1000000;
	
	*out_size = sizeof(n);
	return &n;
}

bool obj_Zombie::isBarricadeInArea()
{
	if(!SuperDafiZombie)
		return false;

	float maxDist = 30.0f;
	for(std::vector<obj_ServerBarricade*>::iterator it = obj_ServerBarricade::allBarricades.begin(); it != obj_ServerBarricade::allBarricades.end(); ++it)
	{
		GameObject* shield = *it;
		float dist = (shield->GetPosition() - GetPosition()).Length();
		if(dist < maxDist)
		{
			MoveNavAgent(shield->GetPosition(), 100.0f);

			// found barricade, wreck it!
			hardObjLock = shield->GetSafeID();
			attackTimer = _zai_AttackTimer / 2;
			SetNavAgentSpeed(staggerTime < 0 ? RunSpeed : 0.0f);
			SwitchToState(EZombieStates::ZState_MoveToBarricade);
			return true;
		}
	}
	return false;
}

void obj_Zombie::SendAIStateToNet()
{
	if(!_zai_DebugAI)
		return;
		
	if(navAgent->m_status == AutodeskNavAgent::Moving)
	{
		PKT_S2C_Zombie_DBG_AIInfo_s n;
		n.from = moveStartPos;
		n.to   = moveTargetPos;
		gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));
	}
}

void obj_Zombie::DebugSingleZombie()
{
	if(!_zai_DebugAI)
		return;

	static KyUInt32 debugVisualId = 0;
	FILE* f = fopen("zdebug.txt", "rt");
	if(!f) return;
	fscanf(f, "%d", &debugVisualId);
	fclose(f);
	
	if(navAgent->m_navBot->GetVisualDebugId() != debugVisualId)
		return;
		
	if(ZombieDisabled)
	{
		AILog(0, "zombie disabled\n");
		return;
	}

	Kaim::Bot* m_navBot = navAgent->m_navBot;
	Kaim::AStarQuery<Kaim::AStarCustomizer_Default>* m_pathFinderQuery = navAgent->m_pathFinderQuery;
	AILog(0, "state: %d, time: %f\n", ZombieState, r3dGetTime() - StateStartTime);
	AILog(0, "GetTargetOnLivePathStatus(): %d\n", m_navBot->GetTargetOnLivePathStatus());
	AILog(0, "GetPathValidityStatus(): %d\n", m_navBot->GetLivePath().GetPathValidityStatus());
	AILog(0, "GetPathFinderResult(): %d %d\n", m_pathFinderQuery->GetPathFinderResult(), m_pathFinderQuery->GetResult());
	if(m_navBot->GetPathFinderQuery())
		AILog(0, "m_processStatus: %d\n", m_navBot->GetPathFinderQuery()->m_processStatus);
}

BOOL obj_Zombie::OnNetReceive(DWORD EventID, const void* packetData, int packetSize)
{
	switch(EventID)
	{
		case PKT_C2S_Zombie_DBG_AIReq:
			SendAIStateToNet();
			break;
	}
	
	return TRUE;
}

void obj_Zombie::SwitchToState(int in_state)
{
	r3d_assert(ZombieState != EZombieStates::ZState_Dead); // death should be final
	if(in_state == ZombieState)
		r3dOutToLog("HunZ - in state:%d\n", in_state);
	r3d_assert(ZombieState != in_state);
		
	ZombieState    = in_state;
	StateStartTime = r3dGetTime();
	
	// duration of idle state
	if(in_state == EZombieStates::ZState_Idle)
	{
		StopNavAgent(); // to release current nav point from Walk state
		hardObjLock = invalidGameObjectID;
		StateTimer  = r3dGetTime() + u_GetRandom(3, 10);
	}
	
	//r3dOutToLog("zombie%p SwitchToState %d\n", this, ZombieState); CLOG_INDENT;

	PKT_S2C_ZombieSetState_s n;
	n.State    = (BYTE)ZombieState;
	gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));
}

void obj_Zombie::DoDeath(float modifier)
{
	extern wiInventoryItem RandomItem(const LootBoxConfig* lootCfg);
	
	// drop loot

	// HunZ - random chance
	if (SuperDafiZombie)
	{
		if(spawnObject->lootBoxCfg)
		{
			for(int i=0; i<4; i++)
			{
				wiInventoryItem wi = RandomItem(spawnObject->lootBoxCfg);

				if(wi.itemID > 0)
				{
					// create random position around zombie
					r3dPoint3D pos = GetPosition();
					pos.y += 0.4f;
					pos.x += u_GetRandom(-1, 1);
					pos.z += u_GetRandom(-1, 1);

					// create network object
					obj_DroppedItem* obj = (obj_DroppedItem*)srv_CreateGameObject("obj_DroppedItem", "obj_DroppedItem", pos);
					obj->SetNetworkID(gServerLogic.GetFreeNetId());
					obj->NetworkLocal = true;
					// vars
					obj->m_Item       = wi;
				}
			}
		}
	}

	if(u_GetRandom() < (0.8f /*+ modifier*/))
	{
		if(spawnObject->lootBoxCfg)
		{
			wiInventoryItem wi = RandomItem(spawnObject->lootBoxCfg);
			if(wi.itemID > 0)
			{
				// create random position around zombie
				r3dPoint3D pos = GetPosition();
				pos.y += 0.4f;
				pos.x += u_GetRandom(-1, 1);
				pos.z += u_GetRandom(-1, 1);

				// create network object
				obj_DroppedItem* obj = (obj_DroppedItem*)srv_CreateGameObject("obj_DroppedItem", "obj_DroppedItem", pos);
				obj->SetNetworkID(gServerLogic.GetFreeNetId());
				obj->NetworkLocal = true;
				// vars
				obj->m_Item       = wi;
			}
		}
	}
/*
	
	if( u_GetRandom() < 0.2f) // 30% to drop that helmet
	{
			// create random position around zombie
			r3dPoint3D pos = GetPosition();
			pos.y += 0.4f;
			pos.x += u_GetRandom(-1, 1);
			pos.z += u_GetRandom(-1, 1);

			// create network object
			obj_DroppedItem* obj = (obj_DroppedItem*)srv_CreateGameObject("obj_DroppedItem", "obj_DroppedItem", pos);
			obj->SetNetworkID(gServerLogic.GetFreeNetId());
			obj->NetworkLocal = true;
			// vars
			obj->m_Item.itemID   = 20192; // HGear_Pumpkin_01
			obj->m_Item.quantity = 1;
	}*/

	// remove from active zombies, but keep object - so it'll be visible for some time on all clients
	StopNavAgent();
	
	DeleteZombieNavAgent(navAgent);
	navAgent = NULL;

	spawnObject->OnZombieKilled(this);
		
	SwitchToState(EZombieStates::ZState_Dead);
}

bool obj_Zombie::ApplyDamage(GameObject* fromObj, float damage, int bodyPart, STORE_CATEGORIES damageSource)
{
	if(ZombieState == EZombieStates::ZState_Dead)
		return false;
	float modifier = 0;
	float dmg = damage;
	if(bodyPart!=1) // only hitting head will lower zombie's health
		dmg = 0;

	if(damageSource != storecat_MELEE && bodyPart == 1) // everything except for melee: one shot in head = kill
		dmg = 1000; 

	ZombieHealth -= dmg;

	if(ZombieHealth <= 0.0f)
	{


		if(fromObj->Class->Name == "obj_ServerPlayer")
		{
			
			obj_ServerPlayer* plr = (obj_ServerPlayer*)fromObj;

			//+5% lootchance

			/*plr->CurrentlyUsedSkill = gSkillSystem->GetSkillById(plr, SKILL_PLUS_LOOTCHANCE);
			modifier = gSkillSystem->GetSkillEffectBySkill(plr->CurrentlyUsedSkill);*/
			gServerLogic.AddPlayerReward(plr, RWD_ZombieKill);
		}
		DoDeath(modifier);
		return true;
	}

	// stagger code
	if(staggerTime < 0)
	{
		if(ZombieState != EZombieStates::ZState_Sleep && ZombieState != EZombieStates::ZState_Waking)
		{
			SetNavAgentSpeed(0.0);
			if(SuperDafiZombie)
			{
				if(u_GetRandom() < 0.01f)
					staggerTime = 1.0f;
				else staggerTime = -1.0f;
			}else staggerTime = 1.0f;
			
		}
	}

	// waking zombies can't be switch to attack
	if(ZombieState == EZombieStates::ZState_Waking)
		return false;
		
	// direct hit, switch to that player anyway if it is new or closer that current one
	float distSq = (fromObj->GetPosition() - GetPosition()).LengthSq();
	const GameObject* trg = GameWorld().GetObject(hardObjLock);
	if((trg == NULL) || (trg && distSq < (trg->GetPosition() - GetPosition()).LengthSq()))
	{
		StartAttack(fromObj);
	}
	
	return false; // false as zombie wasn't killed
}
