#include "r3dPCH.h"
#include "r3d.h"

#include "GameCommon.h"

#include "multiplayer/P2PMessages.h"
#include "ServerGameLogic.h"
#include "ObjectsCode/Weapons/WeaponArmory.h"
#include "ObjectsCode/Weapons/HeroConfig.h"

#include "ObjectsCode/obj_ServerPlayer.h"

#include "../../EclipseStudio/Sources/ObjectsCode/Gameplay/ZombieStates.h"

#include "../../GameEngine/ai/AutodeskNav/AutodeskNavMesh.h"

#include "sobj_ZombieNPC.h"

IMPLEMENT_CLASS(obj_ZombieNPC, "obj_ZombieNPC", "Object");
AUTOREGISTER_CLASS(obj_ZombieNPC);

float		_zai_MaxPursueDistanceNPC   = 300.0f;
float		_zai_AttackRadiusNPC		 = 1.0f;
float		_zai_AttackTimerNPC         = 1.2f;

obj_ZombieNPC::obj_ZombieNPC() :
	netMover(this, 1.0f / 10.0f, (float)PKT_C2C_MoveSetCell_s::PLAYER_CELL_RADIUS)
{
	RunSpeed       = 1.0f;
	WalkSpeed      = 1.0f;

	ZombieState    = EZombieStates::ZState_Idle;

	ZombieHealth   = 100;
	animState      = 0;
	staggerTime    = -1;
}

obj_ZombieNPC::~obj_ZombieNPC()
{
	//ServerGameLogic::FreeNetId(this->GetNetworkID()); Ez a zombi egyelore nem aktiv
}

BOOL obj_ZombieNPC::OnCreate()
{
	ObjTypeFlags |= OBJTYPE_ZombieNPC;
	HeroItemID = 20170;
	navAgent   = NULL;

	const HeroConfig* heroConfig = g_pWeaponArmory->getHeroConfig(HeroItemID);

	HeadIdx = u_random(heroConfig->getNumHeads());
	BodyIdx = u_random(heroConfig->getNumBodys());
	LegsIdx = u_random(heroConfig->getNumLegs());

	CreateNavAgent();
	gServerLogic.NetRegisterObjectToPeers(this);

	return parent::OnCreate();
}

BOOL obj_ZombieNPC::OnDestroy()
{
	return parent::OnDestroy();
}

BOOL obj_ZombieNPC::Update()
{
	parent::Update();

	const float curTime = r3dGetTime();

	{
		CNetCellMover::moveData_s md;
		md.pos       = GetPosition();
		md.turnAngle = GetRotationVector().x;
		md.bendAngle = 0;
		md.state     = (BYTE)animState;

		PKT_C2C_MoveSetCell_s n1;
		PKT_C2C_MoveRel_s     n2;
		DWORD pktFlags = netMover.SendPosUpdate(md, &n1, &n2);
		if(pktFlags & 0x1)
			gServerLogic.p2pBroadcastToActive(this, &n1, sizeof(n1));
		if(pktFlags & 0x2)
			gServerLogic.p2pBroadcastToActive(this, &n2, sizeof(n2));
	}

	return TRUE;
}

void obj_ZombieNPC::CreateNavAgent()
{
	r3d_assert(!navAgent);

	// there is no checks here, they should be done in ZombieSpawn, so pos is navmesh position
	r3dPoint3D pos = GetPosition();
	navAgent = CreateZombieNavAgent(pos);

	//AILog(3, "created at %f %f %f\n", GetPosition().x, GetPosition().y, GetPosition().z);	

	return;
}

void obj_ZombieNPC::StopNavAgent()
{
	navAgent->StopMove();
}

void obj_ZombieNPC::FaceVector(const r3dPoint3D& v)
{
	float angle = atan2f(-v.z, v.x);
	angle = R3D_RAD2DEG(angle);
	SetRotationVector(r3dVector(angle - 90, 0, 0));
}

void obj_ZombieNPC::SwitchToState(int in_state)
{
	ZombieState    = in_state;

	//r3dOutToLog("zombie%p SwitchToState %d\n", this, ZombieState); CLOG_INDENT;

	PKT_S2C_ZombieSetState_s n;
	n.State    = (BYTE)ZombieState;
	gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));
}

DefaultPacket* obj_ZombieNPC::NetGetCreatePacket(int* out_size)
{
	static PKT_S2C_CreateZombie_s n;
	n.spawnID    = toP2pNetId(GetNetworkID());
	n.spawnPos   = GetPosition();
	n.spawnDir   = GetRotationVector().x;
	n.moveCell   = netMover.SrvGetCell();
	n.HeroItemID = HeroItemID;
	n.HeadIdx    = (BYTE)HeadIdx;
	n.BodyIdx    = (BYTE)BodyIdx;
	n.LegsIdx    = (BYTE)LegsIdx;
	n.State      = (BYTE)ZombieState;
	n.WalkSpeed  = WalkSpeed;
	n.RunSpeed   = RunSpeed;

	*out_size = sizeof(n);
	return &n;
}

bool obj_ZombieNPC::ApplyDamagetoZNPC(GameObject* fromObj, float damage, int bodyPart, STORE_CATEGORIES damageSource)
{
	if(ZombieState == EZombieStates::ZState_Dead)
		return false;

	float dmg = damage;
	if(bodyPart!=1) // only hitting head will lower zombie's health
		dmg = 0;

	if(damageSource != storecat_MELEE && bodyPart == 1) // everything except for melee: one shot in head = kill
		dmg = 1000; 

	ZombieHealth -= dmg;

	if(ZombieHealth <= 0.0f)
	{
		DoDeathtoZombie();

		if(fromObj->Class->Name == "obj_ServerPlayer")
		{
			obj_ServerPlayer* plr = (obj_ServerPlayer*)fromObj;
			gServerLogic.AddPlayerReward(plr, RWD_ZombieKill);
		}

		return true;
	}

	// waking zombies can't be switch to attack
	if(ZombieState == EZombieStates::ZState_Waking)
		return false;

	// direct hit, switch to that player anyway if it is new or closer that current one
	float distSq = (fromObj->GetPosition() - GetPosition()).LengthSq();
	const GameObject* trg = GameWorld().GetObject(hardObjLock);
	if((trg == NULL) || (trg && distSq < (trg->GetPosition() - GetPosition()).LengthSq()))
	{
		StartAttack(fromObj);
	}

	MoveNavAgent(fromObj->GetPosition(), 300.0f);

	return false; // false as zombie wasn't killed
}

void obj_ZombieNPC::DoDeathtoZombie()
{
	StopNavAgent();

	DeleteZombieNavAgent(navAgent);
	navAgent = NULL;

	SwitchToState(EZombieStates::ZState_Dead);
}

bool obj_ZombieNPC::MoveNavAgent(const r3dPoint3D& pos, float maxAstarRange)
{

	if(!navAgent->StartMove(pos, maxAstarRange))
		return false;

	moveFrameCount = 0;

	moveWatchTime  = 0;
	moveWatchPos   = GetPosition();
	moveStartPos   = GetPosition();
	moveTargetPos  = pos;
	moveStartTime  = r3dGetTime();
	moveAvoidTime  = 0;
	moveAvoidPos   = GetPosition();

	SendAIStateToNet();

	return true;
}

void obj_ZombieNPC::SendAIStateToNet()
{

	if(navAgent->m_status == AutodeskNavAgent::Moving)
	{
		PKT_S2C_Zombie_DBG_AIInfo_s n;
		n.from = moveStartPos;
		n.to   = moveTargetPos;
		gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));
	}
}

bool obj_ZombieNPC::StartAttack(const GameObject* trg)
{
	r3d_assert(ZombieState != EZombieStates::ZState_Dead);

	if(hardObjLock == trg->GetSafeID())
		return true;

	if(ZombieState == EZombieStates::ZState_Sleep)
	{
		// wake up sleeper
		SwitchToState(EZombieStates::ZState_Waking);
		return true;
	}

	StopNavAgent(); // to release current nav point from Walk state


	float dist = (GetPosition() - trg->GetPosition()).Length();
	if(dist > _zai_MaxPursueDistanceNPC)
		return false;

	// check if we can switch to melee immidiately
	if(dist < _zai_AttackRadiusNPC)
	{
		hardObjLock = trg->GetSafeID();

		if(ZombieState != EZombieStates::ZState_Attack)
			SwitchToState(EZombieStates::ZState_Attack);

		attackTimer   = _zai_AttackTimerNPC / 2;
		isFirstAttack = true;
		return true;
	}

	// check if zombie can get to the player within 2 radius of attack
	r3dPoint3D trgPos = trg->GetPosition();
	if(!gAutodeskNavMesh.AdjustNavPointHeight(trgPos, 1.0f))
	{
		if(!gAutodeskNavMesh.GetClosestNavMeshPoint(trgPos, 2.0f, _zai_AttackRadiusNPC * 2))
		{
			return false;
		}

		if((trgPos - GetPosition()).Length() < 1.0f)
		{
			return false;
		}
	}

	// start pursuing immidiately
	SetNavAgentSpeed(staggerTime < 0 ? RunSpeed : 0.0f);

	lastTargetPos = trg->GetPosition();
	hardObjLock   = trg->GetSafeID();

	// switch to pursue mode
	if(ZombieState != EZombieStates::ZState_Pursue) 
	{
		SwitchToState(EZombieStates::ZState_Pursue);
	}

	return true;
}

void obj_ZombieNPC::SetNavAgentSpeed(float speed)
{
	navAgent->SetTargetSpeed(speed);
}