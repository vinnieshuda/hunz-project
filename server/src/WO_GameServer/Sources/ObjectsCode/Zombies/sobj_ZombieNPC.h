#pragma once

#include "GameCommon.h"
#include "multiplayer/NetCellMover.h"
#include "NetworkHelper.h"

class ZombieNavAgent;

#include "ZombieNavAgent.h"

class obj_ZombieNPC: public GameObject, INetworkHelper
{
	DECLARE_CLASS(obj_ZombieNPC, GameObject)

public:
	CNetCellMover	netMover;
	int				HeroItemID;
	int				HeadIdx;
	int				BodyIdx;
	int				LegsIdx;
	int				ZombieState;
	int				animState;

	float			WalkSpeed;
	float			RunSpeed;
	float			ZombieHealth;
	float			attackTimer;
	float			staggerTime;
	int				isFirstAttack;

	r3dPoint3D		lastTargetPos;

	gobjid_t		hardObjLock;

	//Autodeskes szutykok

	float		moveWatchTime;
	r3dPoint3D	moveWatchPos;
	float		moveStartTime;
	r3dPoint3D	moveStartPos;
	float		moveAvoidTime;
	r3dPoint3D	moveAvoidPos;
	r3dPoint3D	moveTargetPos;
	int			moveFrameCount;


	void			SwitchToState(int in_state);
	void			CreateNavAgent();
	void			StopNavAgent();
	void			FaceVector(const r3dPoint3D& v);

	void			DoDeathtoZombie();

	bool			MoveNavAgent(const r3dPoint3D& pos, float maxAstarRange);
	void			SetNavAgentSpeed(float speed);
	void			SendAIStateToNet();

	ZombieNavAgent* navAgent;

public:
	obj_ZombieNPC();
	~obj_ZombieNPC();

	virtual BOOL	OnCreate();
	virtual BOOL	OnDestroy();
	virtual BOOL	Update();

	bool			ApplyDamagetoZNPC(GameObject* fromObj, float damage, int bodyPart, STORE_CATEGORIES damageSource);
	bool			StartAttack(const GameObject* trg);
	INetworkHelper*	GetNetworkHelper() { return dynamic_cast<INetworkHelper*>(this); }
	DefaultPacket*	INetworkHelper::NetGetCreatePacket(int* out_size);
};
