#include "r3dPCH.h"
#include "r3d.h"

#include "GameCommon.h"
#include "obj_ServerRadioActiveZone.h"
#include "XMLHelpers.h"

extern bool g_bEditMode;

IMPLEMENT_CLASS(obj_rZone, "obj_rZone", "Object");
AUTOREGISTER_CLASS(obj_rZone);

std::vector<obj_rZone*> obj_rZone::LoadedrZonees;


obj_rZone::obj_rZone()
{
	useRadius = 10.0f;
}

obj_rZone::~obj_rZone()
{
}

void obj_rZone::ReadSerializedData(pugi::xml_node& node)
{
	GameObject::ReadSerializedData(node);

	pugi::xml_node objNode = node.child("rZone");
	GetXMLVal("useRadius", objNode, &useRadius);
}

void obj_rZone::WriteSerializedData(pugi::xml_node& node)
{
	GameObject::WriteSerializedData(node);

	pugi::xml_node objNode = node.append_child();
	objNode.set_name("rZone");
	SetXMLVal("useRadius", objNode, &useRadius);
}

BOOL obj_rZone::OnCreate()
{
	parent::OnCreate();

	LoadedrZonees.push_back(this);
	return 1;
}


BOOL obj_rZone::OnDestroy()
{
	LoadedrZonees.erase(std::find(LoadedrZonees.begin(), LoadedrZonees.end(), this));
	return parent::OnDestroy();
}

BOOL obj_rZone::Update()
{
	return parent::Update();
}
