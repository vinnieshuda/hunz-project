#pragma once

#include "GameCommon.h"
#include "multiplayer/NetCellMover.h"
#include "NetworkHelper.h"

class obj_VehicleSpawn;
class obj_ServerPlayer;


class obj_Vehicle : public GameObject, INetworkHelper
{
	DECLARE_CLASS(obj_Vehicle, GameObject)

public:
	obj_VehicleSpawn* spawnObject;
	obj_ServerPlayer* driver;
	CNetCellMover	netMover;
	char		vehicle_Model[64];
	DWORD		peerId_;
	DWORD		origNetID;

	bool		controlled;
	float netvistaupdate;

public:
	obj_Vehicle();
	~obj_Vehicle();
	
	virtual BOOL	OnCreate();
	virtual BOOL	OnDestroy();
	virtual BOOL	Update();

	void		OnNetPacket(const PKT_C2C_MoveSetCell_s& n);
	void		OnNetPacket(const PKT_C2C_MoveRel_s& n);
	void		OnNetPacket(const PKT_C2C_AutoUpdate_s& n);
	void		RelayPacket(const DefaultPacket* packetData, int packetSize, bool guaranteedAndOrdered);
	BOOL		OnNetReceive(DWORD EventID, const void* packetData, int packetSize);
	
	INetworkHelper*	GetNetworkHelper() { return dynamic_cast<INetworkHelper*>(this); }
	DefaultPacket*	INetworkHelper::NetGetCreatePacket(int* out_size);
};
