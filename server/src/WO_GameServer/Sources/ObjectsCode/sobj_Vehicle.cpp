#include "r3dPCH.h"
#include "r3d.h"
#include "GameCommon.h"

#include "multiplayer/P2PMessages.h"
#include "ServerGameLogic.h"

#include "sobj_Vehicle.h"
#include "obj_VehicleSpawn.h"
#include "ObjectsCode/obj_ServerPlayer.h"

IMPLEMENT_CLASS(obj_Vehicle, "obj_Vehicle", "Object");
AUTOREGISTER_CLASS(obj_Vehicle);

obj_Vehicle::obj_Vehicle() : 
	netMover(this, 0.1f, (float)PKT_C2C_MoveSetCell_s::PLAYER_CELL_RADIUS)
{
	controlled = false;
	driver = NULL;
}

obj_Vehicle::~obj_Vehicle()
{
	gServerLogic.FreeNetId(this->GetNetworkID());
}

BOOL obj_Vehicle::OnCreate()
{
	SetPosition(GetPosition());
	netMover.SrvSetCell(GetPosition());
	ObjTypeFlags |= OBJTYPE_Vehicle;
	gServerLogic.NetRegisterObjectToPeers(this);
	netvistaupdate = r3dGetTime();
	
	return parent::OnCreate();
}

BOOL obj_Vehicle::OnDestroy()
{
	PKT_S2C_DestroyNetObject_s n;
	n.spawnID = toP2pNetId(GetNetworkID());

	gServerLogic.p2pBroadcastToActive(this, &n, sizeof(n));
	
	return parent::OnDestroy();
}

BOOL obj_Vehicle::Update()
{
	parent::Update();
	const float curTime = r3dGetTime();
	if(driver == 0)
	{

	}else
	{
		if(curTime > netvistaupdate + 0.5f && controlled)
		{
			driver->TeleportPlayerWithOutUpdate(GetPosition());
/*
			netvistaupdate = r3dGetTime();
			gServerLogic.UpdateNetObjVisDataForVehicle(this, driver);*/
		}
	}

	return TRUE;
}

void obj_Vehicle::RelayPacket(const DefaultPacket* packetData, int packetSize, bool guaranteedAndOrdered)
{
	gServerLogic.RelayPacket(peerId_, this, packetData, packetSize, guaranteedAndOrdered);
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_MoveSetCell_s& n)
{
	r3dPoint3D p1 = netMover.SrvGetCell();
	r3dPoint3D p2 = n.pos;
	p1.y = 0;
	p2.y = 0;
	float dist = (p1 - p2).Length();

	netMover.SetCell(n);

	RelayPacket(&n, sizeof(n), true);
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_AutoUpdate_s& n)
{
	RelayPacket(&n, sizeof(n), true);
}

void obj_Vehicle::OnNetPacket(const PKT_C2C_MoveRel_s& n)
{
	const CNetCellMover::moveData_s& md = netMover.DecodeMove(n);

	r3dPoint3D p1 = GetPosition();
	r3dPoint3D p2 = md.pos;
	p1.y = 0;
	p2.y = 0;
	float dist = (p1 - p2).Length();

	SetPosition(md.pos);
	RelayPacket(&n, sizeof(n), false);
}

DefaultPacket* obj_Vehicle::NetGetCreatePacket(int* out_size)
{
	static PKT_S2C_CreateVehicle_s n;
	n.controlled = controlled;
	n.spawnID    = toP2pNetId(GetNetworkID());
	n.spawnPos   = GetPosition();
	n.spawnDir   = GetRotationVector().x;
	n.rotM		 = GetRotationMatrix();
	r3dscpy(n.model, vehicle_Model);
	
	*out_size = sizeof(n);
	return &n;
}

#undef DEFINE_GAMEOBJ_PACKET_HANDLER
#define DEFINE_GAMEOBJ_PACKET_HANDLER(xxx) \
	case xxx: { \
	const xxx##_s&n = *(xxx##_s*)packetData; \
	if(packetSize != sizeof(n)) { \
	r3dOutToLog("!!!!errror!!!! %s packetSize %d != %d\n", #xxx, packetSize, sizeof(n)); \
	return TRUE; \
	} \
	OnNetPacket(n); \
	return TRUE; \
}

BOOL obj_Vehicle::OnNetReceive(DWORD EventID, const void* packetData, int packetSize)
{
	switch(EventID)
	{
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_MoveSetCell);
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_MoveRel);
		DEFINE_GAMEOBJ_PACKET_HANDLER(PKT_C2C_AutoUpdate);
	}


	return FALSE;
}
#undef DEFINE_GAMEOBJ_PACKET_HANDLER

