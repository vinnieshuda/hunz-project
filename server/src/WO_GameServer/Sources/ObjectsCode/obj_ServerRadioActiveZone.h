#pragma once

#include "GameCommon.h"

class obj_rZone : public MeshGameObject
{
	DECLARE_CLASS(obj_rZone, MeshGameObject)
	
public:
	float		useRadius;
		
	static std::vector<obj_rZone*> LoadedrZonees;
public:
	obj_rZone();
	virtual ~obj_rZone();

	virtual	BOOL		OnCreate();
	virtual	BOOL		OnDestroy();

	virtual	BOOL		Update();
	virtual void		WriteSerializedData(pugi::xml_node& node);
	virtual	void		ReadSerializedData(pugi::xml_node& node);

};
