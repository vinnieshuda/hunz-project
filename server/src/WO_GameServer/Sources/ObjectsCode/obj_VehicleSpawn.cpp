#include "r3dPCH.h"
#include "r3d.h"

#include "Gameplay_Params.h"
#include "GameCommon.h"
#include "XMLHelpers.h"
#include "ServerGameLogic.h"

#include "sobj_Vehicle.h"
#include "obj_VehicleSpawn.h"

IMPLEMENT_CLASS(obj_VehicleSpawn, "obj_VehicleSpawn", "Object");
AUTOREGISTER_CLASS(obj_VehicleSpawn);

obj_VehicleSpawn::obj_VehicleSpawn()
{
	isSpawned = false;
}

obj_VehicleSpawn::~obj_VehicleSpawn()
{

}

BOOL obj_VehicleSpawn::OnCreate()
{

	return TRUE;
}

BOOL obj_VehicleSpawn::Update()
{
	if(gServerLogic.net_mapLoaded_LastNetID == 0)
		return TRUE;

	parent::Update();

	if(!isSpawned)
	{
		spawnedVehicle = (obj_Vehicle*)srv_CreateGameObject("obj_Vehicle", "Vehicle", GetPosition());
		spawnedVehicle->SetNetworkID(gServerLogic.GetFreeNetId());
		spawnedVehicle->NetworkLocal = true;
		spawnedVehicle->spawnObject = this;
		r3dscpy(spawnedVehicle->vehicle_Model, vehicle_Model);
		spawnedVehicle->SetRotationVector(r3dVector(u_GetRandom(0, 360), 0, 0));
		isSpawned = true;
		r3dOutToLog("HunZ - auto spawned\n");
	}
	return TRUE;
}

void obj_VehicleSpawn::ReadSerializedData(pugi::xml_node& node)
{
	GameObject::ReadSerializedData(node);
	pugi::xml_node myNode = node.child("VehicleSpawn");
	_snprintf_s( vehicle_Model, 64, "%s", myNode.attribute("VehicleModel").value());
}