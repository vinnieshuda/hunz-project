#pragma once

#include "GameCommon.h"

class obj_Vehicle;
// class to drop permanent weapon drops on server
// client will ignore this object, it is only loaded on server!
class obj_VehicleSpawn : public GameObject
{
	DECLARE_CLASS(obj_VehicleSpawn, GameObject)

public:
	obj_VehicleSpawn();
	~obj_VehicleSpawn();

	virtual	BOOL		OnCreate();

	virtual	void		ReadSerializedData(pugi::xml_node& node);

	virtual BOOL		Update();

	bool isSpawned;

	obj_Vehicle*		spawnedVehicle;

private:
	char				vehicle_Model[64];

};