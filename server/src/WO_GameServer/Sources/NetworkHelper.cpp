#include "r3dPCH.h"
#include "r3d.h"

#include "NetworkHelper.h"

INetworkHelper::INetworkHelper()
{
	memset(PeerVisStatus, 0, sizeof(PeerVisStatus));
	memset(PeerVisInCarCreated, 0, sizeof(PeerVisInCarCreated));

#if 0
	distToCreateSq = 10 * 10;
	distToDeleteSq = 30 * 30;
#else
	distToCreateSq = 600 * 600; // HunZ - object create tavolsag
	distToDeleteSq = 610 * 610; // HunZ - object eltuntetese tavolsag
#endif
}
