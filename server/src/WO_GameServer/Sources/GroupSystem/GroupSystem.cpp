#include "r3dPCH.h"
#include "r3d.h"
#include "shellapi.h"

#include "GroupSystem.h"

#include "../ServerGameLogic.h"

extern ServerGameLogic gServerLogic;

GroupSystem::GroupSystem()
{
	groupid = 0;
	mapUpdateTime = 0.0f;
}

GroupSystem::~GroupSystem()
{

}

BOOL GroupSystem::Update()
{
	if(gServerLogic.net_mapLoaded_LastNetID == 0)
		return TRUE;

	float curTime = r3dGetTime();

	if(curTime > mapUpdateTime)
	{
		mapUpdateTime = r3dGetTime() + 15.0f;
		for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
		{
			ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
			if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
			{
				if(pr.player->group.groupid < 0)
					continue;
				SendPosInSameGroup(pr.player);
			}

		}
	}
	return TRUE;
}

void GroupSystem::SendPosInSameGroup(obj_ServerPlayer* groupPlr)
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(groupPlr->group.groupid < 0 || pr.player == groupPlr)
				continue;

			if(groupPlr->group.groupid >= 0 && pr.player->group.groupid == groupPlr->group.groupid)
			{
				PKT_C2C_GroupPacket_s n;
				n.state = 5;
				n.peerId = groupPlr->peerId_;
				n.pos = groupPlr->GetPosition();
				gServerLogic.p2pSendToPeer(i, pr.player, &n, sizeof(n));
			}
		}
	}
}

obj_ServerPlayer* GroupSystem::GetPlayerByUserName(const char playername[128])
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(strcmpi(pr.player->userName, playername) == 0) // HunZ - player found
			{
				return pr.player;
			}
		}
	}
	return NULL;
}

void GroupSystem::SendInvitePending(obj_ServerPlayer* toPlr, obj_ServerPlayer* fromPlr, bool isPending)
{
	PKT_C2C_GroupPacket_s n;
	n.state = 1;
	n.peerId = fromPlr->peerId_;
	n.isPending = isPending;
	r3dscpy(n.PlayerName, fromPlr->userName);
	gServerLogic.p2pSendToPeer(toPlr->peerId_, toPlr, &n, sizeof(n));
}

void GroupSystem::SetInvitedState(obj_ServerPlayer* invitedPlr, bool isInvited)
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			PKT_C2C_GroupPacket_s n;
			n.state = 2;
			n.peerId = invitedPlr->peerId_;
			n.isInv = isInvited;
			gServerLogic.p2pSendToPeer(i, pr.player, &n, sizeof(n));
		}
	}
}

obj_ServerPlayer* GroupSystem::GetPlayerByPeerId(DWORD peerId)
{
	ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(peerId);
	return pr.player;
}

int GroupSystem::GetGroupIdandSetLeader(obj_ServerPlayer* leader)
{
	int tempId = -1;
	if(leader->group.groupid < 0)
	{
		tempId = groupid++;
		leader->group.groupid = tempId;
		leader->group.isLeader = true;

		PKT_C2C_GroupPacket_s n;
		n.plrNetID = leader->GetNetworkID();
		n.state = 3;
		n.isleader = true;
		n.peerId = leader->peerId_;
		n.groupid = tempId;
		r3dscpy(n.PlayerName, leader->userName);
		gServerLogic.p2pSendToPeer(leader->peerId_, leader, &n, sizeof(n));
		SetInvitedState(leader, true);

		return tempId;
	}
	else
	{
		tempId = leader->group.groupid;
		return tempId;
	}
}

void GroupSystem::RemovePlayerFromGroup(obj_ServerPlayer* removedPlr)
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player->group.groupid == removedPlr->group.groupid)
			{
				PKT_C2C_GroupPacket_s n;
				n.state = 4;
				n.peerId = removedPlr->peerId_;
				n.isleader = removedPlr->group.isLeader;
				n.plrNetID = removedPlr->GetNetworkID();
				r3dscpy(n.PlayerName, removedPlr->userName);
				gServerLogic.p2pSendToPeer(i, pr.player, &n, sizeof(n));
			}
		}
	}

	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player->group.groupid == removedPlr->group.groupid)
			{
				PKT_C2C_GroupPacket_s n;
				n.state = 4;
				n.peerId = pr.player->peerId_;
				n.isleader = pr.player->group.isLeader;
				n.plrNetID = pr.player->GetNetworkID();
				r3dscpy(n.PlayerName, pr.player->userName);
				gServerLogic.p2pSendToPeer(removedPlr->peerId_, removedPlr, &n, sizeof(n));
			}
		}
	}
	removedPlr->group.groupid = -1;
	removedPlr->group.isLeader = false;
}

bool GroupSystem::GetInvitedState(obj_ServerPlayer* plr)
{
	return plr->group.isInved;
}

void GroupSystem::SendErrorMessage(obj_ServerPlayer* toPlr, const char msg[128])
{
	PKT_C2C_GroupPacket_s n;
	n.state = 0;
	r3dscpy(n.msg, msg);
	gServerLogic.p2pSendToPeer(toPlr->peerId_, toPlr, &n, sizeof(n));
}

bool GroupSystem::isGroupFull(obj_ServerPlayer* leader)
{
	int groupsize = 0;
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player->group.groupid == leader->group.groupid)
			{
				groupsize++;
			}
		}
	}
	if(groupsize > 9)
		return true;
	else return false;
}

void GroupSystem::DiscardGroupById(int groupid)
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player->group.groupid == groupid)
			{
				SetInvitedState(pr.player, false);
				RemovePlayerFromGroup(pr.player);
			}
		}
	}
}

bool GroupSystem::isGroupEmpty(int groupid)
{
	int groupsize = 0;
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player->group.groupid == groupid)
			{
				groupsize++;
			}
		}
	}
	if(groupsize <= 1)
	{
/*
		for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
		{
			ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
			if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
			{
				if(pr.player->group.groupid == groupid)
				{
					SetInvitedState(pr.player, false);
					RemovePlayerFromGroup(pr.player);*/
					return true;
/*
				}
			}
		}*/
	}

	return false;
}

void GroupSystem::SetGroupId(obj_ServerPlayer* acceptedPlr, int groupId, bool isLeader)
{
	acceptedPlr->group.groupid = groupId;
	acceptedPlr->group.isLeader = isLeader;
}

void GroupSystem::SendGroupToNewJoiner(obj_ServerPlayer* joinedPlr, bool forceLeaderFirst)
{
	if(forceLeaderFirst)
	{
		for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
		{
			ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
			if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
			{
				if(pr.player->group.groupid == joinedPlr->group.groupid && pr.player->group.isLeader)
				{
					PKT_C2C_GroupPacket_s n;
					n.state = 3;
					n.peerId = i;
					n.groupid = pr.player->group.groupid;
					n.isleader = pr.player->group.isLeader;
					n.plrNetID = pr.player->GetNetworkID();
					r3dscpy(n.PlayerName, pr.player->userName);
					gServerLogic.p2pSendToPeer(joinedPlr->peerId_, joinedPlr, &n, sizeof(n));
				}
			}
		}
		for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
		{
			ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
			if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
			{
				if(pr.player->group.groupid == joinedPlr->group.groupid && !pr.player->group.isLeader)
				{
					PKT_C2C_GroupPacket_s n;
					n.state = 3;
					n.peerId = i;
					n.groupid = pr.player->group.groupid;
					n.isleader = pr.player->group.isLeader;
					n.plrNetID = pr.player->GetNetworkID();
					r3dscpy(n.PlayerName, pr.player->userName);
					gServerLogic.p2pSendToPeer(joinedPlr->peerId_, joinedPlr, &n, sizeof(n));
				}
			}
		}

	}
}

void GroupSystem::SendNewJoinerToGroup(obj_ServerPlayer* joinedPlr)
{
	for(int i=0; i< gServerLogic.MAX_PEERS_COUNT; ++i)
	{
		ServerGameLogic::peerInfo_s& pr = gServerLogic.GetPeer(i);
		if(pr.status_ == gServerLogic.PEER_PLAYING && pr.player)
		{
			if(pr.player == joinedPlr) // HunZ - magunkat ne kuldjuk le magunkak.
				continue;
			if(pr.player->group.groupid == joinedPlr->group.groupid)
			{
				PKT_C2C_GroupPacket_s n;
				n.state = 3;
				n.peerId = joinedPlr->peerId_;
				n.groupid = pr.player->group.groupid;
				n.isleader = false;
				n.plrNetID = joinedPlr->GetNetworkID();
				r3dscpy(n.PlayerName, joinedPlr->userName);
				gServerLogic.p2pSendToPeer(i, pr.player, &n, sizeof(n));
			}
		}
	}
}