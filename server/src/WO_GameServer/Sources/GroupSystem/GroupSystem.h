#pragma once

#include "../ObjectsCode/obj_ServerPlayer.h"

class obj_ServerPlayer;

class GroupSystem
{

  private:
    int groupid;

  public:
	GroupSystem();
	~GroupSystem();

	float mapUpdateTime;
	
	BOOL Update();

	obj_ServerPlayer* GetPlayerByUserName(const char playername[128]);
	obj_ServerPlayer* GetPlayerByPeerId(DWORD peerId);

	void SendPosInSameGroup(obj_ServerPlayer* groupPlr);
	void SendInvitePending(obj_ServerPlayer* toPlr, obj_ServerPlayer* fromPlr, bool isPending);
	void SetInvitedState(obj_ServerPlayer* invitedPlr, bool isInvited);
	void SetGroupId(obj_ServerPlayer* acceptedPlr, int groupId, bool isLeader);
	void SendGroupToNewJoiner(obj_ServerPlayer* joinedPlr, bool forceLeaderFirst);
	void SendNewJoinerToGroup(obj_ServerPlayer* joinedPlr);
	void RemovePlayerFromGroup(obj_ServerPlayer* removedPlr);
	void DiscardGroupById(int groupid);
	
	void SendErrorMessage(obj_ServerPlayer* toPlr, const char msg[128]);

	bool GetInvitedState(obj_ServerPlayer* plr);
	bool isGroupFull(obj_ServerPlayer* leader);
	bool isGroupEmpty(int groupid);

 
	int GetGroupIdandSetLeader(obj_ServerPlayer* leader);


};