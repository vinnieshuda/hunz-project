#pragma once

#include "public_definitions.h"
#include <stdio.h>

class Teamspeak
{
	public:
		Teamspeak();
		~Teamspeak();
		
	public:

		uint64 serverID;

		void Init();
		void Shutdown();
		void CreateChannel(/* HunZ - at kell lesni az adatokat*/);
		int readKeyPairFromFile(const char *fileName, char *keyPair);
		int writeKeyPairToFile(const char *fileName, const char* keyPair);

		int CreateVirtualServer();

};