#pragma once

#include "r3dPCH.h"
#include "r3d.h"
#include <stdio.h>
#include <Windows.h>

#include "teamspeak.h"

#include <public_definitions.h>
#include <public_errors.h>
#include <serverlib_publicdefinitions.h>
#include <serverlib.h>


Teamspeak::Teamspeak()
{

}

Teamspeak::~Teamspeak()
{

}

/*typedef void (__cdecl Teamspeak::*FuckYou) (uint64 serverID, unsigned int errorCode);*/
/*
typedef void (__cdecl *FuckYou) (uint64 serverID, unsigned int errorCode)
{

}*/
void onClientConnected(uint64 serverID, anyID clientID, uint64 channelID, unsigned int* removeClientError) {
	printf("Client %u joined channel %llu on virtual server %llu\n", clientID, (unsigned long long)channelID, (unsigned long long)serverID);
}

/*
 * Callback when client has disconnected.
 *
 * Parameter:
 *   serverID  - Virtual server ID
 *   clientID  - ID of disconnected client
 *   channelID - ID of channel the client left
 */
void onClientDisconnected(uint64 serverID, anyID clientID, uint64 channelID) {
	printf("Client %u left channel %llu on virtual server %llu\n", clientID, (unsigned long long)channelID, (unsigned long long)serverID);
}

/*
 * Callback when client has moved.
 *
 * Parameter:
 *   serverID     - Virtual server ID
 *   clientID     - ID of moved client
 *   oldChannelID - ID of old channel the client left
 *   newChannelID - ID of new channel the client joined
 */
void onClientMoved(uint64 serverID, anyID clientID, uint64 oldChannelID, uint64 newChannelID) {
	printf("Client %u moved from channel %llu to channel %llu on virtual server %llu\n", clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, (unsigned long long)serverID);
}
/*
 * Callback triggered when the specified client starts talking.
 *
 * Parameters:
 *   serverID - ID of the virtual server sending the callback
 *   clientID - ID of the client which started talking
 */
void onClientStartTalkingEvent(uint64 serverID, anyID clientID) {
	printf("onClientStartTalkingEvent serverID=%llu, clientID=%u\n", (unsigned long long)serverID, clientID);
}

/*
 * Callback triggered when the specified client stops talking.
 *
 * Parameters:
 *   serverID - ID of the virtual server sending the callback
 *   clientID - ID of the client which stopped talking
 */
void onClientStopTalkingEvent(uint64 serverID, anyID clientID) {
	printf("onClientStopTalkingEvent serverID=%llu, clientID=%u\n", (unsigned long long)serverID, clientID);
}
void onAccountingErrorEvent(uint64 serverID, unsigned int errorCode)
{
	char* errorMessage;
	if(ts3server_getGlobalErrorMessage(errorCode, &errorMessage) == ERROR_ok) {
		printf("onAccountingErrorEvent serverID=%llu, errorCode=%u: %s\n", (unsigned long long)serverID, errorCode, errorMessage);
		ts3server_freeMemory(errorMessage);
	}

	/* Your custom handling here. In a real application, you wouldn't stop the whole process because the TS3 server part went down.
	 * The whole idea of this callback is to let you gracefully handle the TS3 server failing to start and to continue your application. */
	exit(1);
}

void Teamspeak::Init()
{
	unsigned int error;
	char *version;
	int usedLogTypes = LogType_CONSOLE;
	const char* logFileFolder = "logss";
	struct ServerLibFunctions funcs;

	memset(&funcs, 0, sizeof(struct ServerLibFunctions));

	funcs.onAccountingErrorEvent	= onAccountingErrorEvent;
	funcs.onClientConnected			= onClientConnected;
	funcs.onClientDisconnected		= onClientDisconnected;
	funcs.onClientMoved				= onClientMoved;
	funcs.onClientStartTalkingEvent = onClientStartTalkingEvent;
	funcs.onClientStopTalkingEvent  = onClientStopTalkingEvent;

	if((error = ts3server_initServerLib(&funcs, LogType_FILE | LogType_CONSOLE | LogType_USERLOGGING, NULL)) != ERROR_ok) {
		char* errormsg;
		if(ts3server_getGlobalErrorMessage(error, &errormsg) == ERROR_ok) {
			printf("Error initialzing serverlib: %s\n", errormsg);
			ts3server_freeMemory(errormsg);
		}
	}
	if((error = ts3server_getServerLibVersion(&version)) != ERROR_ok) {
		printf("Error querying server lib version: %d\n", error);
		r3dOutToLog("HunZ - sdk init failed!\n");
	}
/*	error = ts3server_initServerLib(&funcs, usedLogTypes, logFileFolder);*/

	if(error != ERROR_ok) 
	{
		r3dOutToLog("HunZ - error on init serverlib: %d\n", error);
	}else r3dOutToLog("HunZ - serverlib initialized!\n");
}

void Teamspeak::Shutdown()
{
	unsigned int error;
	error = ts3server_destroyServerLib();
	if(error != ERROR_ok)
	  r3dOutToLog("HunZ - something Bad happend on shutdown: %d\n");
	else r3dOutToLog("HunZ - shutting down serverlib\n");

}
int Teamspeak::readKeyPairFromFile(const char *fileName, char *keyPair) {
	FILE *file;

	file = fopen(fileName, "r");
	if(file == NULL) {
		printf("Could not open file '%s' for reading keypair\n", fileName);
		return -1;
	}

	fgets(keyPair, BUFSIZ, file);
	if(ferror(file) != 0) {
		fclose (file);
		printf("Error reading keypair from file '%s'.\n", fileName);
		return -1;
	}
	fclose (file);

	printf("Read keypair '%s' from file '%s'.\n", keyPair, fileName);
	return 0;
}
int Teamspeak::writeKeyPairToFile(const char *fileName, const char* keyPair) {
	FILE *file;

	file = fopen(fileName, "w");
	if(file == NULL) {
		printf("Could not open file '%s' for writing keypair\n", fileName);
		return -1;
	}

	fputs(keyPair, file);
	if(ferror(file) != 0) {
		fclose (file);
		printf("Error writing keypair to file '%s'.\n", fileName);
		return -1;
	}
	fclose (file);

	printf("Wrote keypair '%s' to file '%s'.\n", keyPair, fileName);
	return 0;
}

int Teamspeak::CreateVirtualServer()
{
	// HunZ - ki kell tolteni az adatokat
	unsigned int error;
	unsigned int serverPort = 9987;
	const char* serverIp = "your ip";
	const char* serverName = "After-Life";
	unsigned int serverMaxClients = 80;
	uint64* result = NULL;
	/*error = ts3server_getVirtualServerKeyPair(serverID, keyPair);*/
	char buffer[BUFSIZ] = { 0 };
	char filename[BUFSIZ];
	char *keyPair;
	char port_str[20];

	strcpy(filename, "keypair_");
	sprintf(port_str, "%d", 9987);  // Default port
	strcat(filename, port_str);
	strcat(filename, ".txt");

	/* Try reading keyPair from file */
	if(readKeyPairFromFile(filename, buffer) == 0) {
		keyPair = buffer;  /* Id read from file */
	} else {
		keyPair = "";  /* No Id saved, start virtual server with empty keyPair string */
	}

	/* Create virtual server using default port 9987 with max 10 slots */

	/* Create the virtual server with specified port, name, keyPair and max clients */
	printf("Create virtual server using keypair '%s'\n", keyPair);
	if((error = ts3server_createVirtualServer(9987, "your ip", "After-Life", keyPair, 10, &serverID)) != ERROR_ok) {
		char* errormsg;
		if(ts3server_getGlobalErrorMessage(error, &errormsg) == ERROR_ok) {
			printf("Error creating virtual server: %s (%d)\n", errormsg, error);
			ts3server_freeMemory(errormsg);
		}
		return 1;
	}
	/* If we didn't load the keyPair before, query it from virtual server and save to file */
	if(!*buffer) {
		if((error = ts3server_getVirtualServerKeyPair(serverID, &keyPair)) != ERROR_ok) {
			char* errormsg;
			if(ts3server_getGlobalErrorMessage(error, &errormsg) == ERROR_ok) {
				printf("Error querying keyPair: %s\n\n", errormsg);
				ts3server_freeMemory(errormsg);
			}
			return 0;
		}

		/* Save keyPair to file "keypair_<port>.txt"*/
		if(writeKeyPairToFile(filename, keyPair) != 0) {
			ts3server_freeMemory(keyPair);
			return 0;
		}
		ts3server_freeMemory(keyPair);
	}
/*
	error2 = ts3server_getVirtualServerKeyPair(serverID, &keyPair);
	if(error2 != ERROR_ok)
		r3dOutToLog("HunZ - teamspeak error code: %d\n", error);
	if(writeKeyPairToFile(filename, keyPair) != 0) {
		ts3server_freeMemory(keyPair);
		return 0;
	}*/

	return 0;
}

void Teamspeak::CreateChannel()
{
	uint64* channels;

	if(ts3server_getChannelList(serverID, &channels) == ERROR_ok) 
	{
	   for(int i=0; channels[i] != NULL; i++)
	   printf("Channel ID: %u\n", channels[i]);
	}

	ts3server_freeMemory(channels);
}
